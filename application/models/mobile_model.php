<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * This model contains all db functions related to mobile Json management
 * @author Teamtweaks
 *
 */
class Mobile_model extends My_Model
{
	public function __construct(){
		parent::__construct();
	}
	/*
	* $table name of the table
	* $condition array of where conditions
	* $columnlist name of the coloums separated by comma(,) and enclosed with ` symbol.
	*/
	public function get_column_details($table='',$condition='',$columnlist=''){
		$this->db->select($columnlist);
		$this->db->from($table);
		$this->db->where($condition);
		return $this->db->get();
	}
	public function insertUserQuick($firstname='',$lastname='',$email='',$pwd='',$expireddate,$key=''){
		$dataArr = array(
			'firstname'	=>	$firstname,
			'lastname'	=>	$lastname,
			'user_name'	=>	$firstname,
			'group'		=>	'User',
			'email'		=>	$email,
			'password'	=>	md5($pwd),
			'status'	=>	'Active',
			'expired_date'	=>	$expireddate,
			'is_verified'=>	'No',
			'created'	=>	mdate($this->data['datestring'],time()),
			'through'	=> 'mobile',
			'mobile_key'=> $key
		);
		$this->simple_insert(USERS,$dataArr);
	}
	public function add_wishlist_category($dataArr=''){
			$this->db->insert(LISTS_DETAILS,$dataArr);
			return $this->db->insert_id();
	}
	
	public function get_dashboard_list($condition = '',$Cont2 = ''){
	
			$this->db->select('p.id,p.product_title,pp.product_image,pa.lat as latitude');
			$this->db->from(PRODUCT.' as p');
			$this->db->join(PRODUCT_ADDRESS_NEW.' as pa',"pa.productId=p.id","LEFT");
			$this->db->join(PRODUCT_PHOTOS.' as pp',"pp.product_id=p.id","LEFT");
			$this->db->join(HOSTPAYMENT.' as hs',"hs.product_id=p.id","LEFT");
			$this->db->where_in('p.user_id',$condition);
			if($Cont2!=''){
				$this->db->where('p.status',$Cont2);
			}
			$this->db->group_by('p.id');
			$this->db->order_by('p.id','desc');
			
			return $query = $this->db->get();
			
		}
		
		function get_productreview_aboutyou($user_id='')
		{
			$this->db->select('r.total_review,p.product_title,u.image');
			$this->db->from(REVIEW.' as r');
			$this->db->where('r.user_id', $user_id);
			$this->db->join(PRODUCT.' as p',"r.product_id=p.id");
			$this->db->join(USERS.' as u',"u.id=r.reviewer_id", "LEFT");
			return $query = $this->db->get_where();
		}
		function get_phone_number_verified($user_id='')
		{	
	
			$this->db->select('*');
			$this->db->from(REQUIREMENTS);
			$this->db->where('id',$user_id);
			
			return $query = $this->db->get_where();
		
			
		}
	public function get_product_details_wishlist($condition = ''){
		$this->db->select('p.id, p.product_title, pp.product_image, pa.city as CityName, pa.state as State_name, pa.country as Country_name,');
		$this->db->from(PRODUCT.' as p');
		$this->db->join(PRODUCT_ADDRESS_NEW.' as pa',"pa.productId=p.id","LEFT");
		$this->db->join(PRODUCT_PHOTOS.' as pp',"pp.product_id=p.id","LEFT");
		$this->db->where('p.id',$condition);
		$this->db->where('p.status','Publish');
		$this->db->order_by('pp.imgPriority','asc');
		$this->db->group_by('pp.product_id');
		return $query = $this->db->get();
			
	}
	
	public function get_list_details_wishlist($condition = ''){
		$select_qry = "select id,name,product_id from ".LISTS_DETAILS." where user_id=".$condition." or user_id=0";
		$productList = $this->ExecuteQuery($select_qry);
		return $productList;
	}
	
	public function get_product_image($condition = ''){
		$productIds = explode(',', $condition);
		$productsNotEmy=array_filter($productIds);
		shuffle($productsNotEmy);
		if(!empty($productsNotEmy))
		{
			$select_qry = "select product_image from ".PRODUCT_PHOTOS." where product_id=".$productsNotEmy[0];
			$productList = $this->ExecuteQuery($select_qry);
			return $productList->row()->product_image;
		}else{
			return 'dummyProductImage.jpg';
		}
	}
	
	public function get_notes_added($Rental_id,$user_id){
		$this->db->select('id,notes');
		$this->db->from(NOTES);
		$this->db->where('product_id',$Rental_id);
		$this->db->where('user_id',$user_id);
		return $query = $this->db->get();
	}
	
	public function update_wishlist($dataStr='',$condition1='', $userid)
	{
		
		$sel_qry = "select product_id,id from ".LISTS_DETAILS." where user_id=".$userid."";
		$ListVal = $this->ExecuteQuery($sel_qry);
		if($ListVal->num_rows() > 0){
			foreach($ListVal->result() as $wlist){
			$productArr=explode(',',$wlist->product_id);
				if(!empty($productArr)){
					if(in_array($dataStr,$productArr)){ 
						$conditi = array('id' => $wlist->id);
						$my_array = array_filter($productArr);
						$to_remove =(array)$dataStr;
						$result = array_diff($my_array, $to_remove);
						$resultStr =implode(',',$result);
						$this->updateWishlistRentals(array('product_id' =>$resultStr),$conditi);
					}
				}
			}
		}
		
		if(!empty($condition1)){
			foreach($condition1 as $wcont){
				$select_qry = "select product_id from ".LISTS_DETAILS." where id=".$wcont."";
				$productList = $this->ExecuteQuery($select_qry);
				$productIdArr=explode(',',$productList->row()->product_id);
		
				if(!empty($productIdArr)){
					if(!in_array($dataStr,$productIdArr)){
						$select_qry = "update ".LISTS_DETAILS." set product_id=concat(product_id,',".$dataStr."') where id=".$wcont."";
						$productList = $this->ExecuteQuery($select_qry);
					}
				}
			}
		}
	}	
	
	public function updateWishlistRentals($dataArr='',$condition=''){
			$this->db->where($condition);
			$this->db->update(LISTS_DETAILS,$dataArr);
	}
	
	public function get_review_data($bookingNo){
		$this->db->select('P.id, P.product_title, PP.product_image, R.review, R.total_review');
		$this->db->from(RENTALENQUIRY.' as RQ');
		$this->db->join(PRODUCT.' as P',"P.id=RQ.prd_id","LEFT");
		$this->db->join(REVIEW.' as R',"R.product_id=RQ.prd_id","LEFT");
		$this->db->join(PRODUCT_PHOTOS.' as PP',"PP.product_id=RQ.prd_id","LEFT");
		$this->db->where('RQ.Bookingno', $bookingNo);
		return $result = $this->db->get();
	}
	
	public function get_review_product_data($bookingNo){
		$this->db->select('P.id, P.product_title, PP.product_image');
		$this->db->from(RENTALENQUIRY.' as RQ');
		$this->db->join(PRODUCT.' as P',"P.id=RQ.prd_id","LEFT");
		$this->db->join(PRODUCT_PHOTOS.' as PP',"PP.product_id=RQ.prd_id","LEFT");
		$this->db->where('RQ.Bookingno', $bookingNo);
		return $result = $this->db->get();
	}
	
	
	public function get_exp_image($condition = ''){
		$this->db->select('product_image,product_id');
		$this->db->from(PRODUCT_PHOTOS);
		$this->db->where('product_id',$condition);
		$this->db->group_by('product_id');
		return $query = $this->db->get();
			
	}
	
	public function get_wishlistphoto($condition = '')
	{
		$this->db->select('product_image,product_id');
		$this->db->from(PRODUCT_PHOTOS);
		$this->db->where('product_id',$condition);
		return $query = $this->db->get();
	}
	
	public function get_featured_lists()
	{
		$this->db->select('p.*,u.id as user_id, u.firstname, u.image as user_image,AVG(r.total_review ) as rate,rp.product_image as product_image, pa.lat, pa.lang');
		$this->db->from(PRODUCT.' as p');
		$this->db->join(USERS.' as u',"u.id=p.user_id");
		$this->db->join(REVIEW.' as r',"r.product_id=p.id","LEFT");
		$this->db->join(PRODUCT_PHOTOS.' as rp',"rp.product_id=p.id","LEFT");
		$this->db->join(PRODUCT_ADDRESS_NEW.' as pa',"pa.productId=p.id","LEFT");
		$this->db->order_by('rate','desc'); 
		$this->db->group_by('p.id'); 
		$this->db->where('p.status','Publish'); 
		$this->db->where('p.featured','Featured'); 
		return $query = $this->db->get();
	}
	
}
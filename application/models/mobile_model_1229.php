<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * This model contains all db functions related to mobile Json management
 * @author Teamtweaks
 *
 */
class Mobile_model extends My_Model
{
	public function __construct(){
		parent::__construct();
	}
	/*
	* $table name of the table
	* $condition array of where conditions
	* $columnlist name of the coloums separated by comma(,) and enclosed with ` symbol.
	*/
	public function get_column_details($table='',$condition='',$columnlist=''){
		$this->db->select($columnlist);
		$this->db->from($table);
		$this->db->where($condition);
		return $this->db->get();
	}
	public function insertUserQuick($firstname='',$lastname='',$email='',$pwd='',$expireddate,$key=''){
		$dataArr = array(
			'firstname'	=>	$firstname,
			'lastname'	=>	$lastname,
			'user_name'	=>	$firstname,
			'group'		=>	'User',
			'email'		=>	$email,
			'password'	=>	md5($pwd),
			'status'	=>	'Active',
			'expired_date'	=>	$expireddate,
			'is_verified'=>	'No',
			'created'	=>	mdate($this->data['datestring'],time()),
			'through'	=> 'mobile',
			'mobile_key'=> $key
		);
		$this->simple_insert(USERS,$dataArr);
	}
	public function add_wishlist_category($dataArr=''){
			$this->db->insert(LISTS_DETAILS,$dataArr);
			return $this->db->insert_id();
	}
}
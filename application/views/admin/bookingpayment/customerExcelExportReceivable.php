<?php
header("Content-type: application/octet-stream");
//header("Content-Type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=Green-House-Receivable-".date('Y-m-d').".xls");
header("Pragma: no-cache");
header("Expires: 0");
$this->load->model('order_model');




?>
<?php
$XML .= '<table>
	<tr>
    	<td colspan="2" align="right">Date :</td>
		<td colspan="2" align="left">'. date('Y-m-d'). '</td>
		<td colspan="3" align="right">Title :</td>
        <td colspan="3" align="left">'.$this->config->item('site_title').' Account Receivable List</td>
        
	</tr>
	<tr>
    	<td colspan="11">&nbsp;</td>
	</tr>
	</table>';	
		
$XML .= '<table border="1">
	<tr>
    	<th>S No</th>
		<th>Date</th>
		<th>Booking No</th>
		<th>Host Email</th>
		<th>Total Amount</th>
		<th>Guest Service Fee</th>
		<th>Host Service Fee</th>
		<th>Net Profit</th>
		<th>Amount to Host</th>
    </tr>';
$sno = 1;
foreach ($commissionTracking->result() as $row){
    
	$XML .='<tr>
    	<td style="text-align:left">'.$sno.'</td>
		<td style="text-align:left">'.date('d-m-Y',strtotime($row->dateAdded)).'</td>
        <td style="text-align:left">'.$row->booking_no.'</td>
        <td style="text-align:left">'.$row->host_email.'</td>
        <td style="text-align:left">'.$row->totalAmt.'</td>
        <td style="text-align:left">'.$row->guest_fee.'</td>
        <td style="text-align:left">'.$row->host_fee.'</td>
        <td style="text-align:left">'.($row->host_fee+$row->guest_fee).'</td>
		<td style="text-align:left">'.($row->payable_amount).'</td>
    </tr>';
	$sno=$sno+1;

}
	$XML .='</table>';	
echo $XML;


?>
                                                               
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title></title>
	</head>
	<body style="margin:0px;"><table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#4BBEFF" data-bgcolor="body-bg-dark" data-module="1" class="ui-sortable-handle currentTable">  
	<tbody><tr>
	 <td>
	 <table width="600" border="0" cellspacing="0" cellpadding="0" align="center" class="devicewidth" style="background-color:#ffffff;" data-bgcolor="light-gray-bg"> 
	 <tbody><tr>
	 <td height="30" bgcolor="#4BBEFF" >&nbsp;</td> 
	 </tr>  
	 <tr>  <?php// echo "<pre>";print_r($pageDetails);  ?>
	<td align="center">          
	 <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">              
	 <tbody><tr style="padding: 10px 10px 0px 10px; float: left">          
			   
	 <td align="center" valign="top">
					<table width="650" border="0" cellpadding="5" cellspacing="1" >
							<tbody style="font-family:Open Sans, Arial, Helvetica, sans-serif; font-size:13px;">
							<tr>
							  
							  <th width="70" bgcolor="#4BBEFF"style="color:#fff; font-size:15px;">Receipt</th>
							  <th width="75" ></th>
							  <th width="75"></th>
							  <th width="75"></th>
							  <th align="right" width="75" style="color:#f3402e; text-align:right"><a onClick="window.print()" TARGET="_blank" style="cursor: pointer; cursor: hand;text-decoration:underline;">Print Page</a></th>
							</tr>
							
						 
							
				</tbody></table>
				</td>       
	 </tr>          
	 </tr>
	 <tr><td align="left" style="color:#4c4c4c;font-size:13px;font-family:Open Sans, Arial, Helvetica, sans-serif;margin:10px; padding: 10px">Booking No : <?php echo $pageDetails[0]['Booking_no']; ?></td></tr>
	 <tr><td align="left" style="color:#4c4c4c;font-size:13px;font-family:Open Sans, Arial, Helvetica, sans-serif;margin:10px; padding: 10px">Experience Name : <?php echo $pageDetails[0]['experience_name']; ?></td></tr>
	 <tr><td align="left" style="color:#4c4c4c;font-size:13px;font-family:Open Sans, Arial, Helvetica, sans-serif;margin:10px; padding: 10px">Address : <?php echo $pageDetails[0]['experience_address']; ?></td></tr>
	 <tr>
	 <td style="border-top:1px solid #808080" bgcolor="#fff">&nbsp;</td>       
	 </tr>        
	 <tr>         
	 <td>
	 <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">              
	 <tbody><tr style="padding: 10px; float: left">          
			   
	 <td align="center" valign="top">
					<table width="650" border="0" cellpadding="5" cellspacing="1" >
							<tbody style="font-family:Open Sans, Arial, Helvetica, sans-serif; font-size:13px;" ><tr>
							  <th width="75" bgcolor="#EFEFEF">Booking Date</th>
							 <th width="5"></th>
							  <th width="75" bgcolor="#EFEFEF">Booking Time</th>
							  <th width="75" ></th>
							 
							  <th width="75" bgcolor="#EFEFEF">No of Guest</th>
							 
							</tr>
							<tr align="center">
								<td ><?php echo $pageDetails[0]['Booking_date']; ?></td>
								<td ></td>
								<td ><?php echo $pageDetails[0]['Booking_time']; ?></td>
								<td ></td>
							
								<td ><?php echo $pageDetails[0]['Number_guest']; ?></td>

							  </tr>
						
							
						 
							
				</tbody></table>
				</td>       
	 </tr>          
	 </tbody>
	 </table>  
	 </td>
	 </tr>      
	 
	<tr style="pointer-events:none;">
	 <td align="center" valign="top" style="color:#000; font-weight: 700; font-family:Open Sans, Arial, Helvetica, sans-serif; font-size:13px;" data-size="body-text" data-min="10" data-max="25" data-color="footer-text">
<img id="map-image" border="0" alt="<?php echo $pageDetails[0]['experience_address']; ?>" src="https://maps.googleapis.com/maps/api/staticmap?center=<?php echo $pageDetails[0]['experience_address']; ?>&zoom=13&size=600x300&maptype=roadmap&sensor=false&format=png&visual_refresh=true&markers=size:mid%7Ccolor:red%7C<?php echo $pageDetails[0]['experience_address']; ?>">	
	 </td> 
	</tr> 
		   
	 <tr>      
	 <td>&nbsp;</td>      
	 </tr>       
	      
	 <tr>   
	  <tr>         
 <td align="center" >          
 <table width="100%" border="0" cellspacing="1" cellpadding="0" align="center" style="padding:0px 10px;">              
 <tbody><tr>          
            
 <td align="left" width="300px" valign="top" style="color:#4f595b; font-family:Open Sans, Arial, Helvetica, sans-serif; font-size:13px; line-height:20px;" data-size="body-text" data-min="10" data-max="25" data-color="footer-text">
<h4 style="float: left; width:100%;"> Cancellation Policy  - <?php echo $pageDetails->Number_guest; ?> </h4> Refer Link <a href="'.base_url().'pages/cancellation-policy" target="_blank">Cancellation Policy</a>.
 <td>

 </tr>      


<tr>          
      
 <td align="left" width="300px" valign="top" style="color:#4f595b; font-family:Open Sans, Arial, Helvetica, sans-serif; font-size:13px; line-height:20px;" data-size="body-text" data-min="10" data-max="25" data-color="footer-text">
<h4 style="float: left; width:100%; margin: 10px 0px;">Billing</h4>
<table>
  <tr>
    <td style="border-bottom: 1px solid #bbb;">Booked for <?php echo $pageDetails[0]['Number_guest']; ?>  &nbsp person</td>
    <td style="border-bottom: 1px solid #bbb;"></td>		
    <td style="border-bottom: 1px solid #bbb; padding: 5px 12px;"><?php echo $pageDetails[0]['user_currency']; ?><?php echo $pageDetails[0]['Price']; ?></td>
  </tr>
  <tr>
<td style="border-bottom: 1px solid #bbb;">Service Fee</td>
    <td style="border-bottom: 1px solid #bbb;"></td>		
    <td style="border-bottom: 1px solid #bbb; padding: 5px 12px;"><?php echo $pageDetails[0]['user_currency']; ?><?php echo $pageDetails[0]['Service_amount']; ?></td>
  </tr>
  <tr>
<td style="border-bottom: 1px solid #bbb;  padding: 10px 0px;">Total Amount</td>
    <td style="border-bottom: 1px solid #bbb;padding: 10px 0px;"></td>		
    <td style="border-bottom: 1px solid #bbb;padding: 10px 12px;"><?php echo $pageDetails[0]['user_currency']; ?><?php echo $pageDetails[0]['Sub_Total']; ?></td>
	
</tr>
  
  <tr>
<td style="border-bottom: 1px solid #bbb;  padding: 10px 0px;">Discount Amount</td>
    <td style="border-bottom: 1px solid #bbb;padding: 10px 0px;"></td>		
    <td style="border-bottom: 1px solid #bbb;padding: 10px 12px;"><?php echo $pageDetails[0]['user_currency']; ?><?php echo $pageDetails[0]['Coupon_Discount']; ?> </td>
	
	
	
  </tr>
  
  <tr>
<td style="border-bottom: 1px solid #bbb;  padding: 10px 0px;">Total Amount</td>
    <td style="border-bottom: 1px solid #bbb;padding: 10px 0px;"></td>		
    <td style="border-bottom: 1px solid #bbb;padding: 10px 12px;"><?php echo $pageDetails[0]['user_currency']; ?><?php echo $pageDetails[0]['Coupon_Total']; ?></td>
  </tr>
 
</table>

<td>
 </tr> 
 
 </tbody>
 </table>      
 </td>        
 </tr> 
	 </tr>
	        
<tr>      
	 <td>&nbsp;</td>     
	 </tr>       
	 <tr>    
	 <td align="center" valign="middle" style="color:#444444; font-family:Open Sans, Arial, Helvetica, sans-serif; font-size:13px;"><a href="javascript:void(0);" style="color:#0094aa; text-decoration:none;" data-size="body-text" data-min="10" data-max="25" data-link-color="plain-url-color" data-link-size="plain-url-text">(Fun Diary releases the money to Travel Buddy after the experience completed.)</a></td>       
	 </tr>        
	 <tr>        
	 <td>&nbsp;</td>   
	 </tr>              
	 <tr>               
	 <td align="center" valign="middle" style="color:#444444; font-family:Open Sans, Arial, Helvetica, sans-serif; font-size:13px; padding:0 20px;" data-size="body-text" data-min="10" data-max="25" data-color="body-text">If you need help or have any questions, please visit<a href="mailto:'.$admin_email.'" style="color:#0094aa;" data-link-color="plain-url-color">info@myfundiary.com</a></td>     
	 </tr>       
	 <tr>       
	 <td height="50">&nbsp;</td>      
	 </tr>         
	 <tr>       
	 <td height="30" bgcolor="#fff">&nbsp;</td>     
	 </tr>      
	 <tr>         
	 <td align="center" bgcolor="#fff">          
	 <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="padding:0px 10px;">              
	 <tbody>
	 
	 </tbody>
	 </table>      
	 </td>        
	 </tr>         
	 <tr>
	 <td height="30" bgcolor="#4BBEFF" >&nbsp;</td> 
	 </tr> 
	 </tbody></table> 
	 </td>      </tr>  
	 </tbody></table>
	 </body>
</html>

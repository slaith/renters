<?php $this->load->view('site/templates/header'); ?>

<style>
table.table-condensed {
    background-color: transparent;
    max-width: 100%;
    float: left;
    width: 100%;
}
table.table-condensed td {
	border-top:1px solid #ccc; 
	border-right: 1px solid #ccc;
}
div.datepicker.datepicker-dropdown.dropdown-menu {
	padding: 0
}
.datepicker-days {
	height: 227px;
}
.datepicker.datepicker-dropdown.dropdown-menu {
    min-height: 179px;
    overflow: hidden !important;
    padding: 4px 0 0 8px;
    width: 220px !important;
    border-radius: 0;
}
.datepicker th{font-weight: normal; color: #aaa;}
.datepicker td, .datepicker th{width: 30px; height: 30px;}
.datepicker.datepicker-dropdown.dropdown-menu{min-height: 225px;}
.datepicker tr:first-child th{height: 20px;}
.datepicker td, .datepicker th {
    height: 30px !important;
    width: 33px !important;
}
div.datepicker thead tr:first-child th, div.datepicker tfoot tr:first-child th {
    border-radius: 0;
    cursor: pointer;
    height: 24px !important;
}
.table-condensed > thead > tr > th, .table-condensed > tbody > tr > th, .table-condensed > tfoot > tr > th, .table-condensed > thead > tr > td, .table-condensed > tbody > tr > td, .table-condensed > tfoot > tr > td {
    padding: 8.3px 5px !important;
}
@media screen (max-width: 567px){
	.datepicker.datepicker-dropdown.dropdown-menu {
		min-height: 200px;
		overflow: hidden !important;
		padding: 4px 0 0 8px;
		width: 226px !important;
		left: 25% !important;
		right: 25% !important;
	}
}
@media screen (max-width: 375px){
	.datepicker.datepicker-dropdown.dropdown-menu {
		left: auto !important;

		right: 10px !important;
	}
}
header {
    position: absolute;
    z-index: 999;
}
.header {
    padding: 7px 0 0 !important;
    transition-duration: 0.3s;
    z-index: 999;
	background:none;
	position: relative;
	 border: none;
}
.navbar .nav > li > a{	
	color: #fff;
}
.browse{
	color: #fff;
}
.caret{
	border-top: 4px solid #fff;
}
.text-center{
	z-index: 99;
}
.logo a img {
    float: left;
    margin: -21px 6px 0 0;
    width: 100%;
}
.brows-loop{
	margin: 0 0 0 100px;
}
.user-name{
	 color: #fff;
}
</style>
<link rel="stylesheet" href="css/site/datepicker.css" type="text/css">
<script type="text/javascript">
	var tag = document.createElement('script');
	tag.src = "//www.youtube.com/iframe_api";
	var firstScriptTag = document.getElementsByTagName('script')[0];
	firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
	var player;
	function onYouTubeIframeAPIReady() {
		player = new YT.Player('ytplayer', {
			events: {
				'onReady': onPlayerReady
			}
		});
	}
	function onPlayerReady() {
		player.playVideo();
		// Mute!
		player.mute();
	}
</script>
<script typ="text/javascript">
$.getScript("<?php echo base_url();?>js/site/bootstrap-datepicker.js", function(){
	var startDate = '<?php echo date('m/d/Y');?>';
	var FromEndDate = new Date();
	var ToEndDate = new Date();
	ToEndDate.setDate(ToEndDate.getDate()+365);
	$('.from_date').datepicker({
		weekStart: 1,
		startDate: '<?php echo date('m/d/Y');?>',
		autoclose: true
	}).on('changeDate', function(selected){
		startDate = new Date(selected.date.valueOf());
		startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())) + 1);
		$('.to_date').datepicker('setStartDate', startDate);
	}); 
	
	$('.to_date').datepicker({
		weekStart: 1,
		startDate: startDate,
		endDate: ToEndDate,
		autoclose: true
	}).on('changeDate', function(selected){
		FromEndDate = new Date(selected.date.valueOf());
		FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())) + 1);
		$('.from_date').datepicker('setEndDate', FromEndDate);
	});
});
</script>

<section>
	<div class="banner-container">
		<div class="banner-container-bg"></div>
		<div class="row">
			<div class="col-md-12">
			<?php if($adminList->slider == "on") {?>
				<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
					<ul class="carousel-inner">
						<?php $slider_loop=1;
						foreach ($sliderList->result() as $Slider)
						{ 
							if($Slider->image !='')
							{
								$ImgSrc=$Slider->image;
							}else{
								$ImgSrc='dummyProductImage.jpg';
							} ?>
							<li class="item <?php if($slider_loop==1){ $slider_loop=2;?>active<?php }?>">
								<img src="images/slider/thumb/<?php echo $ImgSrc;?>" alt="<?php echo $Slider->slider_title; ?>">
							</li>
						<?php }?> 
					</ul>
					<!--<a class="left carousel-control" href="#carousel-example-generic" data-slide="prev"><span class="fa fa-chevron-left"></span></a>
					<a class="right carousel-control" href="#carousel-example-generic" data-slide="next"><span class="fa fa-chevron-right"></span></a>-->
				</div>
				<?php } else { ?>
				<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
					<ul class="carousel-inner">
						<li class="item active" style="height: 600px;">
							<section class="content-section video-section" style="position: relative; height: 600px;">
								<div class="pattern-overlay">
									<iframe width="100%" height="130%" id="ytplayer" type="text/html" src="<?php echo $adminList->videoUrl;?>?playlist=DjxwLr6TjHs&start=60&rel=0&autoplay=1&controls=0&showinfo=0&loop=1&iv_load_policy=3&enablejsapi=1" frameborder="0" allowfullscreen></iframe>
								</div>
							</section>
						</li>
					</ul>
				</div>
				<?php }?>
				<div class="main-text hidden-xs">
					<div class="col-md-12 text-center">
						<div class="container">
							<h1><?php if($adminList->home_title_1 != ''){
								echo $adminList->home_title_1;
							}else{
								if($this->lang->line('WELCOMEHOME') != '') { echo stripslashes($this->lang->line('WELCOMEHOME')); } else echo "WELCOME HOME";
							} ?></h1>
							<h3><?php if($adminList->home_title_2 != ''){
								echo $adminList->home_title_2;
							}else{
								if($this->lang->line('Rentuniqueplacestostay') != '') { echo stripslashes($this->lang->line('Rentuniqueplacestostay')); } else echo "Rent unique places to stay"; 
							} ?></h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="push"></div>
</section>

<section>
	<div class="relative-holder">
		<div class="searching-section">
			<div class="container">
				<!--This is for mobile view  doesnot disply in normal view only for mobil -->
				<!-- WILL DISPLAY ONLY ON MODILE FOE RESPONSIVE STATIC START-->
				<div class="searct-sechs mobile-display ">
					<a class="mobile-selecbox" href="#success" data-toggle="modal">
						<input class="search-text" placeholder="Search for a property" type="text">
						<input class="sbt-btn" value="Submit" type="submit">
					</a>
					<!-- Modal -->
					<div class="modal fade banerpop" id="success" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header modal-header-success">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
								</div>
								<div class="modal-body">
									<form  class="mobilform" method="get" action="property" id="property_search_form_mobile" >
										<input name="city" id="autocompleteNewMoblie" class="where" placeholder="<?php if($this->lang->line('search_where') != '') { echo stripslashes($this->lang->line('search_where')); } else echo "Where do you want to go?"; ?>"  type="text"  >
										<!--<input  name="datefrom" class="chek from_date" placeholder="<?php if($this->lang->line('check_in') != '') { echo stripslashes($this->lang->line('check_in')); } else echo "Check in"; ?>" type="text" contenteditable="false">-->`
										<?php if($this->config->item("enable_checkin")):?>
										<input  name="dateto" class="chek-in to_date" placeholder="<?php if($this->lang->line('check_out') != '') { echo stripslashes($this->lang->line('check_out')); } else echo "Check out"; ?>" type="text" contenteditable="false">
										<?php endif;?>
										<?php if($accommodates !='' && count($accommodates)){ ?>
										<select name="guests" class="home_select" id="guest">
											<option value=""><?php if($this->lang->line('guest') != '') { echo stripslashes($this->lang->line('guest')); } else echo "Guest";?></option>
											<?php foreach($accommodates as $accommodate) {
												if($accommodate==1){?>
												<option value="<?php echo $accommodate;?>"><?php echo $accommodate.' Guest'?></option>
												<?php } else { ?>
												<option value="<?php echo $accommodate;?>">
												<?php echo $accommodate.' Guests';?></option>
												<?php } }?>
										</select>
										<?php } ?>
										<input class="fom-subm" value="<?php if($this->lang->line('Submit') != '') { echo stripslashes($this->lang->line('Submit')); } else echo "Submit"; ?>" type="submit" >
									</form>
								</div>
							</div><!-- /.modal-content -->
						</div><!-- /.modal-dialog -->
					</div><!-- /.modal -->
				</div>
				<!-- WILL DISPLAY ONLY ON MODILE FOE RESPONSIVE END-->
				<!--This is for mobile view  END -->
				<form  class="webform" method="get" action="property" id= "property_search_form">
					<datalist id="locations">
						<option value="Vancouver,BC,Canada">Vancouver</option>
						<option value="Richmond,BC,Canada">Richmond</option>

					</datalist>
					<input name="city"  id="autocompleteNew1" class="where" placeholder="<?php if($this->lang->line('search_where') != '') { echo stripslashes($this->lang->line('search_where')); } else echo "Where do you want to go?"; ?>"  type="text"  >

					<script type="text/javascript">
					var sources=<?php echo json_encode($this->config->item('cities'))?>;
    				var sourceObject=sources.map(function(el){
                            return {city: el}
                        })
                        console.log (sourceObject)
                        $('#autocompleteNew1').selectize({
                            options:sourceObject,
                            valueField: 'city',
                            labelField: 'city',
                            searchField: 'city',
                            create:false,
                            maxItems:1

                     


                        })
					/*
						$('#autocompleteNew1').typeahead({
						      source: sources,
						      minLength:0,
						         
                              showHintOnFocus:true

						});

						
						$('#autocompleteNew1').keyup(function(){
							if($(this).val()=="")
								$(this).html("");

						})*/
						
					</script>
				<!--	<input  name="datefrom" class="chek from_date" placeholder="<?php if($this->lang->line('check_in') != '') { echo stripslashes($this->lang->line('check_in')); } else echo "Check in"; ?>" id="check_in"type="text" contenteditable="false">-->
					<?php if($this->config->item("enable_checkin")):?>

					<input  name="dateto" class="chek-in to_date" placeholder="<?php if($this->lang->line('check_out') != '') { echo stripslashes($this->lang->line('check_out')); } else echo "Check out"; ?>" type="text" contenteditable="false" id="check_out">
					<?php endif;?>
					<?php if($accommodates !='' && count($accommodates) &&0){ ?>
					<select name="guests" class="home_select" id="guest">
						<option value=""><?php if($this->lang->line('guest') != '') { echo stripslashes($this->lang->line('guest')); } else echo "Guest";?></option>
						<?php foreach($accommodates as $accommodate) {
							if($accommodate==1){?>
							<option value="<?php echo $accommodate;?>"><?php echo $accommodate.' Guest'?></option>
							<?php } else { ?>
							<option value="<?php echo $accommodate;?>">
							<?php echo $accommodate.' Guests';?></option>
						<?php } }?>
					</select>
					<?php } ?>
					<input class="fom-subm" value="<?php if($this->lang->line('Submit') != '') { echo stripslashes($this->lang->line('Submit')); } else echo "Submit"; ?>" type="submit" >
				</form>
				<span id="city_warn"></span>
			</div>
		</div>
	</div>
</section>

<section>
<div class="fraet-exp-section">
<div class="container">
<?php /* <div class="top-title-structure">
<h2 class="find-a-place">Featured Property</h2>
<span class="discover-place">Choose your favorite property</span>
</div>


<ul id="myList">
    <?php //echo "<pre>"; print_r($featuredLists->result_array());die;
        foreach($featuredLists->result_array() as $exp){ 
        $review_count = $this->landing_model->get_all_details(REVIEW,array('product_id'=>$exp['id']));
		
		//echo "<pre>"; print_r($review_count->result());die;
        $count_rating = 0;
        foreach($review_count->result() as $c_t){
        $count_rating = $count_rating + $c_t->total_review;
        }
        $image_count = round($count_rating/$review_count->num_rows());
    ?>

<li class="col-sm-4 col-xs-6">
<div class="country-product-container">
    <figure><a href="<?php echo base_url() ?>rental/<?php echo $exp['id']; ?>">
    
<img src="<?php if(strpos($exp['product_image'], 's3.amazonaws.com') > 1)echo $exp['product_image'];else echo base_url()."server/php/rental/".$exp['product_image']; ?>">
        
    </a>

        <div class="start-side">
            <span class="start"><img src="images/review/star_rating_<?php echo $image_count; ?>.png"></span>
<?php ?>
<span class="feture-name">(<?php echo $review_count->num_rows(); ?>)</span>
</div>

<div class="price-tags">
<span class="price-numr"><?php echo  $this->session->userdata('currency_s').' '.CurrencyValue($exp['id'] ,stripslashes($exp['price'])); ?></span>

</div>

</figure>
<div class="botm-full-contnr">
    <div class="pricing-contnr">
<div class="price-loops">
<a href="<?php echo base_url(); ?>users/show/<?php echo $exp['property_owner']?>" class="aurtrs-img">
<?php  if($exp['image'] !=''){ ?>
<img  src="images/users/<?php echo $exp['image'];?>"/>
<?php  } else { ?>
<img  src="images/site/safe_image.png"/>
<?php  } ?>
</a>


</div>


    </div>


    <div class="detail-container-text">
        <div class="detail-container-text-top">
  <span class="contrys-name"><?php echo $exp['country']; ?></span>
  <span class="feture-name"><?php echo $exp['room_type']; ?></span>

	
    
        </div>

            


<div class="detail-container-text-bottom">
<p><?php echo $exp['product_title']; ?></p>
        </div>

</div>
    </div>

</div>
</li>

        <?php } ?>



</ul> */ ?>

</div>
</div>

</section>

<section>
	<div class="center-page">
		<div class="container">
			<div class="top-title-structure">
				<h2 class="find-a-place">
				<?php if($adminList->home_title_3 != ''){
					echo $adminList->home_title_3;
				}else{
					if($this->lang->line('ExploretheWorld') != '') { echo stripslashes($this->lang->line('ExploretheWorld')); } else echo "Explore the World"; } ?>
				</h2>
				<span class="discover-place">
				<?php if($adminList->home_title_4 != ''){
					echo $adminList->home_title_4;
				}else{
					if($this->lang->line('Seewherepeoplearetraveling') != '') { echo stripslashes($this->lang->line('Seewherepeoplearetraveling')); } else echo "See where people are traveling, all around the world."; } ?>
				</span>
			</div>
			<ul class="hme-container">
			<?php if($CityDetails->result() > 0){ 
				$i = 1;
				foreach($CityDetails->result() as $CityRows){
				$Cityname=str_replace(' ','+',$CityRows->name);?>
				<li class="col-md-<?php if ($i%10 == 1 || $i%10 == 7)echo "8"; else echo "4"; $i++;?>">
					<a href="property?city=<?php echo $Cityname; ?>">
						<div class="image-container">
							<img src="images/city/<?php echo trim(stripslashes($CityRows->citythumb)); ?>">
						</div>
						<div class="overlay-text">
							<span><?php echo trim(ucfirst(stripslashes($CityRows->name))); ?></span>
						</div>
					</a>
				</li>
			<?php } }?>
			</ul>
		</div>
	</div>
</section>

<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script type="text/javascript">
	$("#property_search_form").validate({
		// Specify the validation rules
		rules: {
			city: "required",
			agree: "required",
			datefrom: "required",
			dateto:"required",
			//guests:"required"
		},
		// Specify the validation error messages
		messages: {
			city: '',
			agree: "Please accept our policy",
			datefrom:'',
			dateto:'',
			//guests:''
		},
		submitHandler: function(form) {
			form.submit();
		}
	});
	$("#property_search_form_mobile").validate({
		// Specify the validation rules
		rules: {
			city: "required",
			agree: "required",
			datefrom: "required",
			dateto:"required",
			//guests:"required"
		},
		// Specify the validation error messages
		messages: {
			city: '',
			agree: "Please accept our policy",
			datefrom:'',
			dateto:'',
			//guests:''
		},
		submitHandler: function(form) {
			form.submit();
		}
	});
</script>
<!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&signed_in=true"></script>-->
<!--<script type="text/javascript" src="js/markerwithlabel.js"></script>
<script type="text/javascript" src="js/markerwithlabel_packed.js"></script>-->
<script>
// This example displays an address form, using the autocomplete feature
// of the Google Places API to help users fill in the information.

var placeSearch, autocomplete;
var componentForm = {
  street_number: 'short_name',
  route: 'long_name',
  locality: 'long_name',
  administrative_area_level_1: 'short_name',
  country: 'long_name',
  postal_code: 'short_name'
};
function initializeMap() {
  // Create the autocomplete object, restricting the search
  // to geographical location types.
 
//Commenting out to remove autocomplete
/*
  autocomplete = new google.maps.places.Autocomplete(
    (document.getElementById('autocompleteNew1')),
      { types: ['geocode'] });
  // When the user selects an address from the dropdown,
  // populate the address fields in the form.
  google.maps.event.addListener(autocomplete, 'place_changed', function() {
    var data = $("#autocompleteNew1").serialize();
	//findLocationAuto(data);
	return false;
  });*/
}





// [START region_fillform]
function fillInAddress() {

  // Get the place details from the autocomplete object.
  var place = autocomplete.getPlace();

  for (var component in componentForm) {
    document.getElementById(component).value = '';
    document.getElementById(component).disabled = false;
  }

  // Get each component of the address from the place details
  // and fill the corresponding field on the form.
  for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];
    if (componentForm[addressType]) {
      var val = place.address_components[i][componentForm[addressType]];
      document.getElementById(addressType).value = val;
    }
  }
}
// [END region_fillform]

// [START region_geolocation]
// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
}

function findLocation1(event)
{

	var x = event.which || event.keyCode;
	if(x == 13)window.location='<?php echo base_url()?>property?city='+$('#autocompleteNew1').val();
}

function findLocationAuto1(loc)
{
	window.location='<?php echo base_url()?>property?'+loc;
}
// [END region_geolocation]
</script>
<script>
function Form_Validation(){
var autocompletenew1=$("autocompleteNew1").val();
var check_in = $("#check_in").val();
var check_out = $("#check_out").val();
if(check_in==""){
$("#check_in").focus();
return false;
}
 if(check_out==""){
$("#check_out").focus();
return false;
}
}
</script>
<style>
.buttonBar{opacity:0};
.main-text {
    color: #fff;
    position: absolute;
    top: 60%;
    width: 99.667%;
}
.modal-backdrop.fade.in{z-index: 99}
</style>
<?php 
$this->load->view('site/templates/content_above_footer');
$this->load->view('site/templates/footer');
?>
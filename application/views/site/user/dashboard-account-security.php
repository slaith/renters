<?php 
$this->load->view('site/templates/header');
?>
<!---DASHBOARD-->
<div class="dashboard  yourlisting bgcolor account acc-security">

  <div class="top-listing-head">
 <div class="main">   
            <ul id="nav">
                <li><a href="<?php echo base_url();?>dashboard"><?php if($this->lang->line('Dashboard') != '') { echo stripslashes($this->lang->line('Dashboard')); } else echo "Dashboard";?></a></li>
                <li><a href="<?php echo base_url();?>inbox"><?php if($this->lang->line('Inbox') != '') { echo stripslashes($this->lang->line('Inbox')); } else echo "Inbox";?></a></li>
                <li><a href="<?php echo base_url();?>listing/all"><?php if($this->lang->line('YourListing') != '') { echo stripslashes($this->lang->line('YourListing')); } else echo "Your Listing";?></a></li>
                <li class='hidden'><a href="<?php echo base_url();?>trips/upcoming"><?php if($this->lang->line('YourTrips') != '') { echo stripslashes($this->lang->line('YourTrips')); } else echo "Your Trips";?></a></li>
                <li><a href="<?php echo base_url();?>settings"><?php if($this->lang->line('Profile') != '') { echo stripslashes($this->lang->line('Profile')); } else echo "Profile";?></a></li>
                <li class="active"><a href="<?php echo base_url();?>account"><?php if($this->lang->line('Account') != '') { echo stripslashes($this->lang->line('Account')); } else echo "Account";?></a></li>
                <li><a href="<?php echo base_url();?>plan"><?php if($this->lang->line('Plan') != '') { echo stripslashes($this->lang->line('Plan')); } else echo "Plan";?></a></li>
            </ul> </div></div>
	<div class="main">
    	<div id="command_center">
    
              <ul id="nav">
                <li><a href="<?php echo base_url();?>dashboard"><?php if($this->lang->line('Dashboard') != '') { echo stripslashes($this->lang->line('Dashboard')); } else echo "Dashboard";?></a></li>
                <li><a href="<?php echo base_url();?>inbox"><?php if($this->lang->line('Inbox') != '') { echo stripslashes($this->lang->line('Inbox')); } else echo "Inbox";?></a></li>
                <li><a href="<?php echo base_url();?>listing/all"><?php if($this->lang->line('YourListing') != '') { echo stripslashes($this->lang->line('YourListing')); } else echo "Your Listing";?></a></li>
                <li class='hidden'><a href="<?php echo base_url();?>trips/upcoming"><?php if($this->lang->line('YourTrips') != '') { echo stripslashes($this->lang->line('YourTrips')); } else echo "Your Trips";?></a></li>
                <li><a href="<?php echo base_url();?>settings"><?php if($this->lang->line('Profile') != '') { echo stripslashes($this->lang->line('Profile')); } else echo "Profile";?></a></li>
                <li class="active"><a href="<?php echo base_url();?>account"><?php if($this->lang->line('Account') != '') { echo stripslashes($this->lang->line('Account')); } else echo "Account";?></a></li>
                <li><a href="<?php echo base_url();?>plan"><?php if($this->lang->line('Plan') != '') { echo stripslashes($this->lang->line('Plan')); } else echo "Plan";?></a></li>
            </ul>  
            <ul class="subnav">
                <li><a href="<?php echo base_url();?>account"><?php if($this->lang->line('Notifications') != '') { echo stripslashes($this->lang->line('Notifications')); } else echo "Notifications";?></a></li>
                 <li><a href="<?php echo base_url();?>account-payout"><?php if($this->lang->line('PayoutPreferences') != '') { echo stripslashes($this->lang->line('PayoutPreferences')); } else echo "Payout Preferences";?></a></li>
                <li><a href="<?php echo base_url();?>account-trans"><?php if($this->lang->line('TransactionHistory') != '') { echo stripslashes($this->lang->line('TransactionHistory')); } else echo "Transaction History";?></a></li>
                <!-- <li><a href="<?php echo base_url();?>referrals">Referrals</a></li>-->
                <!--<li><a href="<?php echo base_url();?>account-privacy"><?php if($this->lang->line('Privacy') != '') { echo stripslashes($this->lang->line('Privacy')); } else echo "Privacy";?></a></li>-->
                <li class="active"><a href="<?php echo base_url();?>account-security"><?php if($this->lang->line('Security') != '') { echo stripslashes($this->lang->line('Security')); } else echo "Security";?></a></li>
                <li><a href="<?php echo base_url();?>account-setting"><?php if($this->lang->line('Settings') != '') { echo stripslashes($this->lang->line('Settings')); } else echo "Settings";?></a></li>            
            

            <a class="invitefrds" href="#" style="display:none"><?php if($this->lang->line('InviteFriendspage') != '') { echo stripslashes($this->lang->line('InviteFriendspage')); } else echo "Invite Friends page";?></a>

            
          </ul>
            	<div id="account">
    <div class="box">
      <div class="middle">
        <form method="post" action="site/user/change_password1" accept-charset="UTF-8" id="changePassword1"><div style="margin:0;padding:0;display:inline"></div>
          <h2><?php if($this->lang->line('ChangeYourPassword') != '') { echo stripslashes($this->lang->line('ChangeYourPassword')); } else echo "Change Your Password";?></h2>
          <div class="section notification_section">
            <div class="notification_area">
            
              <div class="notification_action">


                <input type="hidden" value="<?php echo $userDetails->row()->email;?>" name="id" id="id">
               
                <table class="password-fields">
                    <tbody><tr><td width=200><?php if($this->lang->line('OldPassword') != '') { echo stripslashes($this->lang->line('OldPassword')); } else echo "Old Password";?>:<span style="color:#FF0000">*</span></td><td><input autocomplete="off" type="password" style="width:150px;margin:3px;" name="old_password" id="old_password" class="space_restric">
                    <div id="old_password_warn"  style="float:right;margin-left:5px; color:#FF0000;"></div></td></tr>
                  <tr><td><?php if($this->lang->line('NewPassword') != '') { echo stripslashes($this->lang->line('NewPassword')); } else echo "New Password";?>:<span style="color:#FF0000">*</span></td><td><input type="password" style="width:150px;margin:3px;" size="30" name="new_password" id="new_password" class="space_restric">
                   <div id="new_password_warn"  style="float:right;margin-left:5px; color:#FF0000;"></div></td></tr>
                  <tr><td><?php if($this->lang->line('change_conf_pwd') != '') { echo stripslashes($this->lang->line('change_conf_pwd')); } else echo "Confirm Password";?>:<span style="color:#FF0000">*</span></td><td><input type="password" style="width:150px;margin:3px;" size="30" name="confirm_password" id="confirm_password" class="space_restric">
                   <div id="confirm_password_warn"  style="float:right;margin-left:5px; color:#FF0000;"></div></td></tr>
                </tbody></table>
              </div>
            </div>
            <div class="buttons">
              <input type="submit" value="<?php if($this->lang->line('UpdatePassword') != '') { echo stripslashes($this->lang->line('UpdatePassword')); } else echo "Update Password";?>" onclick="return ChangePassword();" name="commit" class="updatepaswd">
            </div>
          </div>
</form>
       

        <div class="clearfix"></div>
      </div>
    </div>
  </div>
         
  </div>
    </div>
</div>
<script type="text/javascript">
$(function() {
			$("#changePassword").submit(function(){
				alert('');
				//alert('password');
				 $("#old_password_warn").html('');
				 $("#new_password_warn").html('');
				 $("#confirm_password_warn").html('');
				
				
					
					if(jQuery.trim($("#old_password").val()) == ''){
					  
						$("#old_password_warn").html('Please Enter your Current password');
						$("#old_password").focus();
						return false;
						
					}else if(jQuery.trim($("#new_password").val()) == ''){
						
						$("#new_password_warn").html('Please Enter your new passowrd');
						$("#new_password").focus();
						return false;
						
					}else if(jQuery.trim($("#confirm_password").val()) == ''){
					  
						$("#confirm_password_warn").html('Re-enter the new password');
						$("#confirm_password").focus();
						return false;
					}else if(jQuery.trim($("#confirm_password").val()) != (jQuery.trim($("#new_password").val()))){
					  
						$("#confirm_password_warn").html('Passwords do not match');
						$("#confirm_password").focus();
						return false;
						
					}else
					{	
					      	$("#changePassword").submit();
					}
					
					return false;	
				});
		});
		
	 
function removeError(idval){
	$("#"+idval+"_warn").html('');}
</script>
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="http://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
<script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>
<script>
// just for the demos, avoids form submit
jQuery.validator.setDefaults({
  debug: true,
  success: "valid"
});
$( "#changePassword" ).validate({
  rules: {
    old_password: {
      required: true,
      minlength: 6
    },
	new_password: {
      required: true,
      minlength: 6
    },
	confirm_password: {
      required: true,
      minlength: 6
    },
  }
});
</script>

<script type="text/javascript">
     $(document).on('keydown', '.space_restric', function(e) {
            if (e.keyCode == 32) return false;
                });								          
</script>
<!---DASHBOARD-->
<!---FOOTER-->
<?php 

$this->load->view('site/templates/footer');
?>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * User related functions
 * @author Teamtweaks
 *
 */

class Mobilecart extends MY_Controller { 
	public $mobdata = array();
	function __construct(){
        parent::__construct();
		$this->load->helper(array('cookie','date','form','email'));
		$this->load->library(array('encrypt','form_validation'));		
		$this->load->model('mobile_model');		
		define("API_LOGINID",$this->config->item('payment_2'));
		
    }
  
	/** 
	 * 
	 * Loading index page
	 */
	
	public function index(){
		$this->load->view('mobile/home.php',$this->mobdata);
	} 
	
	
	/** 
	 * 
	 * Payment Process using credit card
	 */
	
	public function userPaymentCard(){
		
	
		$enqury_id=$this->input->post('enqury_id'); 
		
		$enquryDetails = $this->mobile_model->get_all_details(RENTALENQUIRY,array('id'=>$enqury_id));
		
		
		$condition =array('id' => $enquryDetails->row()->user_id);
		$userDetails = $this->mobile_model->get_all_details(USERS,$condition);
		$loginUserId = $enquryDetails->row()->user_id;
		$lastFeatureInsertId = mt_rand();
		if($this->input->post('creditvalue')=='authorize') 
		{	
			$Auth_Details=unserialize(API_LOGINID); 
			$Auth_Setting_Details=unserialize($Auth_Details['settings']);	
			//echo '<pre>';print_r($Auth_Setting_Details);die;
			error_reporting(-1);
			define("AUTHORIZENET_API_LOGIN_ID",$Auth_Setting_Details['merchantcode']);    // Add your API LOGIN ID
			define("AUTHORIZENET_TRANSACTION_KEY",$Auth_Setting_Details['merchantkey']); // Add your API transaction key
			define("API_MODE",$Auth_Setting_Details['mode']);
			if(API_MODE	=='sandbox')
			{ 
				define("AUTHORIZENET_SANDBOX",true);// Set to false to test against production
			}
			else
			{
				define("AUTHORIZENET_SANDBOX",false);
			}       
			define("TEST_REQUEST", "FALSE"); 
			require_once './authorize/autoload.php';
			$transaction = new AuthorizeNetAIM;
			$transaction->setSandbox(AUTHORIZENET_SANDBOX);
			$transaction->setFields(array('amount' =>   $enquryDetails->row()->totalAmt, 
				'card_num' =>  $this->input->post('cardNumber'), 
				'exp_date' => $this->input->post('CCExpDay').'/'.$this->input->post('CCExpMnth'),
				'first_name' =>$userDetails->row()->firstname,
				'last_name' =>$userDetails->row()->lastname,
				'address' => $userDetails->row()->address,
				'city' => $userDetails->row()->city,
				'state' => $userDetails->row()->state,
				'country' => $userDetails->row()->country,
				'phone' => $userDetails->row()->phone_no,
				'email' =>  $userDetails->row()->email,
				'card_code' => $this->input->post('creditCardIdentifier'),
				));
			$response = $transaction->authorizeAndCapture();
			if($response->approved != '')
			{
				$product_id =$enquryDetails->row()->prd_id;
				$product = $this->mobile_model->get_all_details(PRODUCT,array('id' => $product_id));
				$seller = $this->mobile_model->get_all_details(USERS,array('id' => $product->row()->user_id));
				$totalAmount = $enquryDetails->row()->totalAmt;
				$enquiryid = $enquryDetails->row()->id;
				$loginUserId = $enquryDetails->row()->user_id;
				if($lastFeatureInsertId != '') {
				$delete = 'delete from '.PAYMENT.' where dealCodeNumber = "'.$lastFeatureInsertId.'" and user_id = "'.$loginUserId.'" ';
				$this->mobile_model->ExecuteQuery($delete, 'delete');
				$dealCodeNumber = $lastFeatureInsertId;
				} else {
				$dealCodeNumber = mt_rand();
				}
				$insertIds = array();
				$now = date("Y-m-d H:i:s");
				$paymentArr=array(
					'product_id'=>$product_id,
					'sell_id'=>$product->row()->user_id,
					'price'=>$totalAmount,
					'indtotal'=>$product->row()->price,
					'sumtotal'=>$totalAmount,
					'user_id'=>$loginUserId,
					'created' => $now, 
					'dealCodeNumber' => $dealCodeNumber,
					'status' => 'Paid',
					'shipping_status' => 'Pending',
					'total'  => $totalAmount,
					'EnquiryId'=>$enquiryid,
					'inserttime' => NOW());
					
				$this->mobile_model->simple_insert(PAYMENT,$paymentArr);
				$insertIds[]=$this->db->insert_id();
				$paymtdata = array(
					'randomNo' => $dealCodeNumber,
					'randomIds' => $insertIds
					);
				
				$this->db->where(array('id'=>$enquiryid));
				$this->db->update(RENTALENQUIRY,array('booking_status' => 'Booked'));
				
				$SelBookingQty =$this->mobile_model->get_all_details(RENTALENQUIRY,array( 'id' => $enquiryid));
			
				//booking update
				$productId = $SelBookingQty->row()->prd_id;
				$arrival = $SelBookingQty->row()->checkin;
				$depature = $SelBookingQty->row()->checkout;
				$dates = $this->getDatesFromRange($arrival, $depature);
				$i=1;
				$dateMinus1= count($dates)-1; 
				
				foreach($dates as $date){
					if($i <= $dateMinus1){
						$BookingArr=$this->mobile_model->get_all_details(CALENDARBOOKING,array('PropId' => $productId,'id_state' => 1,'id_item' => 1,'the_date' => $date));
						if($BookingArr->num_rows() > 0){
						
						}else{
							$dataArr = array('PropId' => $productId,
											 'id_state' => 1,
											 'id_item' => 1,
											 'the_date' => $date
											);
							$this->mobile_model->simple_insert(CALENDARBOOKING,$dataArr);
						}
				   }
				   $i++;
				}
												
				//SCHEDULE calendar
				$DateArr=$this->mobile_model->get_all_details(CALENDARBOOKING,array('PropId'=>$productId));
				$dateDispalyRowCount=0;
				
				if($DateArr->num_rows > 0){
					$dateArrVAl .='{';
					foreach($DateArr->result() as $dateDispalyRow){
											
						if($dateDispalyRowCount==0){
							$dateArrVAl .='"'.$dateDispalyRow->the_date.'":{"available":"1","bind":0,"info":"","notes":"","price":"'.$price.'","promo":"","status":"booked"}';
						}else{
							$dateArrVAl .=',"'.$dateDispalyRow->the_date.'":{"available":"1","bind":0,"info":"","notes":"","price":"'.$price.'","promo":"","status":"booked"}';
						}
						$dateDispalyRowCount=$dateDispalyRowCount+1;
					}
					$dateArrVAl .='}';
				}
											
				$inputArr4=array();
				$inputArr4 = array('id' =>$productId,'data' => trim($dateArrVAl));
											
				$this->mobile_model->update_details(SCHEDULE,$inputArr4,array('id' =>$productId));
				
				$condition = array('status' => 'Active', 'seo_tag'=>'host-accept');
				$service_tax_host=$this->mobile_model->get_all_details(COMMISSION, $condition);
				$this->data['host_tax_type'] = $service_tax_host->row()->promotion_type;
				$this->data['host_tax_value'] = $service_tax_host->row()->commission_percentage;
				$condition = array('status' => 'Active', 'seo_tag'=>'guest-booking');
				$service_tax_guest=$this->mobile_model->get_all_details(COMMISSION, $condition);
				$this->data['guest_tax_type'] = $service_tax_guest->row()->promotion_type;
				$this->data['guest_tax_value'] = $service_tax_guest->row()->commission_percentage;
				
				
				$orderDetails = $this->mobile_model->get_all_details(RENTALENQUIRY,array( 'id' => $enquiryid));
				//echo $this->db->last_query();die;
				$userDetails = $this->mobile_model->get_all_details(USERS,array( 'id' => $orderDetails->row()->renter_id));
				$guest_fee = $orderDetails->row()->serviceFee;
				$netAmount = $orderDetails->row()->totalAmt-$orderDetails->row()->serviceFee;
				if($this->data['host_tax_type'] == 'flat')
				{
					$host_fee = $this->data['host_tax_value'];
				}
				else 
				{
					$host_fee = ($netAmount * $this->data['host_tax_value'])/100;
				}
				$payable_amount = $netAmount - $host_fee;
				$data1 = array('host_email'=>$userDetails->row()->email,
					'booking_no'=>$orderDetails->row()->Bookingno,
					'total_amount'=>$orderDetails->row()->totalAmt,
					'guest_fee'=>$guest_fee,
					'host_fee'=>$host_fee,
					'payable_amount'=>$payable_amount
					);
				$chkQry = $this->mobile_model->get_all_details(COMMISSION_TRACKING,array( 'booking_no' => $orderDetails->row()->Bookingno));
				if($chkQry->num_rows() == 0)
				$this->mobile_model->simple_insert(COMMISSION_TRACKING, $data1);
				else
				$this->mobile_model->update_details(COMMISSION_TRACKING,$data1,array('booking_no'=>$orderDetails->row()->Bookingno));
				
				redirect('mobile/success/'.$loginUserId.'/'.$lastFeatureInsertId.'/'.$response->transaction_id);
			}
			else{		
				redirect('mobile/failed/'.$response->response_reason_text); 
			}
		
	
	}
	}
	
	/** 
	 * 
	 * Loading success payment
	 */
	
	public function pay_success(){
		if($this->uri->segment(5)==''){
			$transId = $_REQUEST['txn_id'];
			$Pray_Email = $_REQUEST['payer_email'];
		}else{
			$transId = $this->uri->segment(5);
			$Pray_Email = '';
		}

		$this->mobdata['Confirmation'] = "Success";	

		$this->load->view('mobile/success.php',$this->mobdata);
	}
	
	/** 
	 * 
	 * Loading failed payment
	 */
	
	public function pay_failed(){
		$this->mobdata['errors'] = $this->uri->segment(3);
		$this->load->view('mobile/failed.php',$this->mobdata);
	}
	/** 
	 * 
	 * Connecting back to mobile application
	 */
	
	public function payment_return(){
		$this->mobdata['msg'] = $this->uri->segment(3);	
		$this->load->view('mobile/payment_return.php',$this->mobdata);
	}
	
	
	
	/* selvakumar mobile payment function */
	
	/* */
	/* credit form function */
	
	public function credit_card_form(){
		$enqury_id = $_GET['enqury-id'];
		$this->mobdata['enqury_id'] = $enqury_id;
		
		$this->mobdata['enqury_detail'] = $this->mobile_model->get_all_details(RENTALENQUIRY,array('id'=>$this->mobdata['enqury_id']));
		
		$this->mobdata['product_detail'] = $this->mobile_model->get_all_details(PRODUCT,array('id'=>$this->mobdata['enqury_detail']->row()->prd_id));
		
		$this->mobdata['product_image'] = $this->mobile_model->get_all_details(PRODUCT_PHOTOS,array('product_id'=>$this->mobdata['enqury_detail']->row()->prd_id));
		
		$this->load->view('mobile/credit_card_payment.php',$this->mobdata);
	}
	
	
	public function getDatesFromRange($start, $end){
    	$dates = array($start);
    	while(end($dates) < $end){
        	$dates[] = date('Y-m-d', strtotime(end($dates).' +1 day'));
    	}
		return $dates;
	}	
	
	/* credit form function */
	
	
	
	
	/* selvakumar mobile payment function */
	
	
	

}

/* End of file user.php */
/* Location: ./application/controllers/site/mobilecart.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * mobile related functions
 * @author Teamtweaks
 *
 */
 
class Mobile extends MY_Controller {  


	function __construct(){
        parent::__construct();
		$this->load->helper(array('cookie','date','form','email'));
		$this->load->library(array('encrypt','form_validation','resizeimage'));		
		$this->load->model('mobile_model');
	}	

	/** 
	 * 
	 * Loading Category Json Page
	 */
	
	public function index(){
		
	} 
	
	public function discoverfeatured(){

		$perpage = 10; 
		$page=intval($_GET['page_Id']);
		$email = $_GET['email_id'];
		$userSection = array();
		if($email != '')
		{
			$userDetails = $this->mobile_model->get_all_details(USERS, array('email'=>$email));
			$userSection['user_name'] = $userDetails->row()->user_name;
			$userSection['firstname'] = $userDetails->row()->firstname;
			$userSection['lastname'] = $userDetails->row()->lastname;
			$userSection['is_verified'] = $userDetails->row()->is_verified;
			$userSection['id_verified'] = $userDetails->row()->id_verified;
			$userSection['ph_verified'] = $userDetails->row()->ph_verified;
			$userSection['phone_no'] = $userDetails->row()->phone_no;
			$userSection['since'] = date('Y-m-d', strtotime($userDetails->row()->created));
			$userSection['gender'] = $userDetails->row()->gender;
			$userSection['description'] = $userDetails->row()->description;
			$userSection['language'] = $userDetails->row()->languages_known;
			$userSection['location'] = $userDetails->row()->address;
			if($userDetails->row()->dob_date != 0) {
			$date = $userDetails->row()->dob_date."-";
			}else{
			$date = '';
			}
			if($userDetails->row()->dob_month != 0) {
			$month = $userDetails->row()->dob_month."-";
			}else{
			$month = '';
			}
			if($userDetails->row()->dob_year != 0) {
			$year = $userDetails->row()->dob_year;
			}else{
			$year = '';
			}
			$userSection['dob'] = $date.$month.$year;
			if($userDetails->row()->image != '') {
			$userSection['image'] = $userDetails->row()->image;
			}else{
			$userSection['image'] = 'profile.jpg';
			}
		}
        //$this->db->select('u.state_code,u.name,u.id,c.name as countryname,u.citythumb as citylogo');
        $this->db->select('*');
		$this->db->from(CITY_NEW.' as u');
		$this->db->where('u.status','Active');
		$this->db->where('u.featured','1');
		$this->db->order_by('u.name');
		
		if($page>0){
			$this->db->limit($perpage,($page*$perpage)-$perpage);	
		}else{
			$page=1;
			$this->db->limit($perpage,0);
		}
		$city = $this->db->get();
		$CatArr = array();
		foreach($city->result() as $catVal){
			
			$googleAddress = $catVal->name.", ".$catVal->state_name.", ".$catVal->country_name;
			$googleAddress = str_replace(" ", "+", $googleAddress);
			$json = file_get_contents("https://maps.google.com/maps/api/geocode/json?address=$googleAddress&sensor=false&key=$google_map_api");
		
			$city = '';
			$country = '';
			$json = json_decode($json);
			if($json->{'error_message'} == ''){
				$newAddress = $json->{'results'}[0]->{'address_components'};
				$this->data ['lat'] = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
				$this->data ['long'] = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
				foreach($newAddress as $nA)
				{
					if($nA->{'types'}[0] == 'locality')$location = $nA->{'long_name'};
					if($nA->{'types'}[0] == 'administrative_area_level_2')$city = $nA->{'long_name'};
					if($nA->{'types'}[0] == 'country')$country = $nA->{'long_name'};
				}
				if($city == '')
				$city = $location;
				
				
				$this->db->from(PRODUCT.' as p');
				$this->db->join(PRODUCT_ADDRESS_NEW.' as pa',"pa.productId=p.id","LEFT");
				$this->db->where('p.status', 'Publish');
				$this->db->where('pa.city', $city);
				$this->db->where('pa.country', $country);
				$product_count=$this->db->count_all_results();
				
				if($catVal->citylogo != ''){
					$renameArr = explode('.', $catVal->citylogo);
					$catImage = $renameArr[0].'.jpg';;
				}else{
					$catImage = 'no_image.jpg';	
				}
				if($product_count != 0){ 
				$CatArr[] = array("city_id" => $catVal->name, "city_name" => $catVal->name,"city_image" =>$catImage,"product_count"=>$product_count);
				}
			}
		}

		$json_encode = json_encode(array("featuredCities" => $CatArr,"pageId"=>(string)$page, "userDetails" => $userSection));
		echo $json_encode;
		
	}

	public function citysearch() {
		$cityname = $_GET['cityname'];
	}
	
	public function rental_list(){
		$responseArr = array();
		$perpage =5;
		$page=intval($_GET['page_Id']);
		$email=$_GET['email_id'];
		$fav=0;
		$responseArr['city_Id'] = $cityId=$_GET['city_Id'];
		$responseArr['mcity_Id'] = $mcityId=$_GET['mcity_Id'];
		$responseArr['f_date_arrive'] = $arive = $_POST['f_date_arrive'];
		$responseArr['f_date_depart'] = $depart = $_POST['f_date_depart'];
		$responseArr['f_guest'] = $noofguest = $_POST['f_guest'];
		$responseArr['f_private_room'] = $privateroom = $_POST['f_private_room'];
		$responseArr['f_shared_room'] = $sharedroom = $_POST['f_shared_room'];
		$responseArr['f_entire_home'] = $entireroom = $_POST['f_entire_home'];
		$responseArr['f_p_min'] = $pricemin = $_POST['f_p_min'];
		$responseArr['f_p_max'] = $pricemax = $_POST['f_p_max'];
		$responseArr['f_bath_room'] = $bathroom = $_POST['f_bath_room'];
		$responseArr['f_bed'] = $bed = $_POST['f_bed'];
		$responseArr['f_bed_room'] = $bedroom = $_POST['f_bed_room'];
		
		
		if($noofguest != '' && $noofguest != '0'){
		$search .= " and p.accommodates >=".$noofguest;
		}
		if($pricemax != '' && $pricemin != ''){
		$search .= " and (p.price BETWEEN ".$pricemin." and ".$pricemax.')';
		}
		if($bed != '' && $bed != '0'){
		$search .= " and p.listings LIKE '%\"Beds\":\"".$bed."\"%'";
		}
		if($bedroom != '' && $bedroom != '0'){
		$search .= " and p.listings LIKE '%\"Bedrooms\":\"".$bedroom."\"%'";
		}
		if($bathroom != '' && $bathroom != '0'){
		$search .= " and p.listings LIKE '%\"Bathrooms\":\"".$bathroom."\"%'";
		}
		
		//{"Bedrooms":"1","Beds":"1","Bathrooms":"Private","minimum_stay":"3","accommodates":"","SPACE_SIZE":""}

		if($page>0){
		$pageLimitStart=($page*$perpage)-$perpage;
         $this->db->limit($perpage,($page*$perpage)-$perpage);	
		}else{
			$page=1;
			$pageLimitStart=0;
			$this->db->limit($perpage,0);
		}
		$searchPerPage=$perpage;
		$condition .= '( 1=1';
		
		if($cityId !='')
		{
			$googleAddress = str_replace(" ", "+", $cityId);
			$json = file_get_contents("https://maps.google.com/maps/api/geocode/json?address=$googleAddress&sensor=false&key=$google_map_api");
			$json = json_decode($json);
			//echo '<pre>';print_r($json);die;
			$newAddress = $json->{'results'}[0]->{'address_components'};
			$this->data ['lat'] = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
			$this->data ['long'] = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
			foreach($newAddress as $nA)
			{
				if($nA->{'types'}[0] == 'locality')$location = $nA->{'long_name'};
				if($nA->{'types'}[0] == 'administrative_area_level_2')$city = $nA->{'long_name'};
				if($nA->{'types'}[0] == 'country')$country = $nA->{'long_name'};
			}
			$minLat = $json->{'results'}[0]->{'geometry'}->{'bounds'}->{'southwest'}->{'lat'};
			$minLong = $json->{'results'}[0]->{'geometry'}->{'bounds'}->{'southwest'}->{'lng'};
			$maxLat = $json->{'results'}[0]->{'geometry'}->{'bounds'}->{'northeast'}->{'lat'};
			$maxLong = $json->{'results'}[0]->{'geometry'}->{'bounds'}->{'northeast'}->{'lng'};
			
			$condition .= ' AND (pa.lat BETWEEN "'.$minLat.'" AND "'.$maxLat.'" ) AND (pa.lang BETWEEN "'.$minLong.'" AND "'.$maxLong.'" )';
		}
		if($mcityId !='')
		{
			$googleAddress = str_replace(" ", "+", $mcityId);
			$json = file_get_contents("https://maps.google.com/maps/api/geocode/json?address=$googleAddress&sensor=false&key=$google_map_api");
			$json = json_decode($json);
			//echo '<pre>';print_r($json);die;
			$newAddress = $json->{'results'}[0]->{'address_components'};
			$this->data ['lat'] = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
			$this->data ['long'] = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
			foreach($newAddress as $nA)
			{
				if($nA->{'types'}[0] == 'locality')$location = $nA->{'long_name'};
				if($nA->{'types'}[0] == 'administrative_area_level_2')$city = $nA->{'long_name'};
				if($nA->{'types'}[0] == 'country')$country = $nA->{'long_name'};
			}
			$minLat = $json->{'results'}[0]->{'geometry'}->{'bounds'}->{'southwest'}->{'lat'};
			$minLong = $json->{'results'}[0]->{'geometry'}->{'bounds'}->{'southwest'}->{'lng'};
			$maxLat = $json->{'results'}[0]->{'geometry'}->{'bounds'}->{'northeast'}->{'lat'};
			$maxLong = $json->{'results'}[0]->{'geometry'}->{'bounds'}->{'northeast'}->{'lng'};
			
			$condition .= ' AND (pa.lat BETWEEN "'.$minLat.'" AND "'.$maxLat.'" ) AND (pa.lang BETWEEN "'.$minLong.'" AND "'.$maxLong.'" )';
		}
		if($mrentalId !='')
		{
			$condition .= ' and p.id = "' .$_GET['mrental_Id']. '" ';
		}
		$condition .= ' ) ';
		$condition=' where '.$condition.$search.' and p.status="Publish" group by p.id order by p.created desc limit '. $pageLimitStart . ',' . $searchPerPage;
       
		$select_qry = "select  p.product_title,p.id,p.price,p.currency,p.home_type,u.image as userphoto,u.email,u.user_name,pa.lat as latitude,pa.lang as longitude, pp.product_image from ".PRODUCT." p LEFT JOIN ".PRODUCT_ADDRESS_NEW." pa on pa.productId=p.id LEFT JOIN ".PRODUCT_PHOTOS." pp on pp.product_id=p.id LEFT JOIN ".USERS." u on (u.id=p.user_id) ".$condition;
		
		$rentalList = $this->mobile_model->ExecuteQuery($select_qry);
		
		$searchcount = $rentalList->num_rows();
		
		$priceQuery = $this->mobile_model->ExecuteQuery("SELECT MAX(`price`) AS MAXIMUN, MIN(`price`) AS MINIMUM FROM `fc_product`");
		
		$priceArr = array('siteMin'=>$priceQuery->row()->MINIMUM,'siteMax'=>$priceQuery->row()->MAXIMUN);
        //echo $this->db->last_query();die;
		$mapcount=0;
		$condition = array('email'=>$email);
		$userDetails = $this->mobile_model->get_all_details ( USERS, $condition );
		$userId = $userDetails->row()->id;
		$favs = array();
		$checkFavorite = $this->mobile_model->get_all_details(LISTS_DETAILS, array('user_id'=>$userId));
		foreach($checkFavorite->result() as $result)
		$favs[] = $result->product_id;
		
		foreach($rentalList->result() as $rental){
			if (in_array($rental->id,$favs))$fav = 1;
			$price_details .= $rental->price.','; 
			if($rental->product_image != ''){
			$p_img = explode('.',$rental->product_image);	
			$pro_img = $p_img[0].'.jpg';
			$proImage = $pro_img;
			}else{
				$proImage = 'no_image.jpg';
			}
			if($_GET['code'] != ''){
			$condition = array('currency_type'=>$_GET['code']);
			$currency_rate = $this->mobile_model->get_all_details ( CURRENCY, $condition );
			$currency_code = $currency_rate->row()->currency_symbols;
			}else{
			$currency_code = '$';
			}
			if($_GET['code'] != ''){
			$condition = array('currency_type'=>$_GET['code']);
			$currency_rate = $this->mobile_model->get_all_details ( CURRENCY, $condition );
			$currency_price =$rental->price * $currency_rate->row()->currency_rate; 
			}else{
			$currency_price = $rental->price;
			}
			if($rental->userphoto != '') {
			$userphoto = $rental->userphoto;
			}else{
			$userphoto = 'profile.jpg';
			}
			//$this->dyn_price($rental->email);	
			$latandlog[] = array("latitude" => $rental->latitude,"longitude" => $rental->longitude,"rental_id" => $rental->id,"hostname"=>$rental->user_name,"rental_price" =>$currency_price,"rental_title" => $rental->product_title,"rental_image" =>$proImage,"userphoto" =>$userphoto,"user_currency"=>$currency_code);	
		
			$rentalListArr[] = array("rental_id" => $rental->id, "rental_title" => $rental->product_title,"rental_image" =>$proImage,"latitude" => $rental->latitude,"longitude" => $rental->longitude,"userphoto" =>$userphoto,"fav"=>$fav,"rental_price" =>$currency_price,"user_currency"=>$currency_code,"home_type"=>$rental->home_type);
			$mapcount++;
			$fav = 0;
			
		}
		$all_price =  rtrim($price_details,',');
		
		$all_price_detail  =  explode(',',$all_price);
		
		

		if($mcityId == ''){
		if($searchcount == 0){
		$rentalListArr = array();
		$json_encode = json_encode(array("rentalList" =>$rentalListArr,"pageId"=>(string)$page,"count"=>$searchcount));
		}else{
		if(min($all_price_detail) == max($all_price_detail))
			$json_encode = json_encode(array("rentalList" => $rentalListArr,"pageId"=>(string)$page,"count"=>$searchcount,'minprice'=>'0.00','maxprice'=>max($all_price_detail), "responseArr" => $responseArr, "priceArr"=>$priceArr));
		else
			$json_encode = json_encode(array("rentalList" => $rentalListArr,"pageId"=>(string)$page,"count"=>$searchcount,'minprice'=>min($all_price_detail),'maxprice'=>max($all_price_detail), "responseArr" => $responseArr, "priceArr"=>$priceArr));
		}
		}else{
		$json_encode = json_encode(array("latandlog"=>$latandlog, "responseArr" => $responseArr));
		}
		echo $json_encode;
		
	} 
	
	public function rental_detail() {
	$email = $_GET['email'];
	$prodID = $_GET['prod_id'];
	$prodPrice = $_GET['prodPrice']; 
	//$prodID =198 ;
		
	$userDetails = $this->mobile_model->get_all_details(USERS, array('email'=>$email));
	$userId = $userDetails->row()->id;
	$checkFavorite = $this->mobile_model->get_all_details(LISTS_DETAILS, array('user_id'=>$userId, 'product_id'=>$prodID));
	if($checkFavorite->num_rows() > 0) $fav = 1;
	else $fav = 0;
	
		$where1 = array('p.id'=>$prodID);	
		$where2 = array('product_id'=>$prodID);	
		
		$this->db->select('p.product_title,p.listings, p.home_type, p.datefrom, p.dateto, p.price_perweek, p.price_permonth, p.id, p.user_id, p.description,p.price, p.image, p.accommodates, p.bedrooms, p.beds, p.noofbathrooms, p.minimum_stay, p.cancellation_policy, p.house_rules, p.list_name,pa.lat as latitude,pa.lang as longitude, u.description as description1, u.phone_no, rq.id, rq.checkin, rq.checkout, u.user_name, u.group, u.s_phone_no, u.about, u.loginUserType, u.email as RenterEmail, u.image as thumbnail,
		pa.country, pa.state, pa.city, pa.zip, pa.address, u.is_verified, u.id_verified, u.ph_verified, u.created, u.facebook, u.google, u.address, u.about');
		$this->db->from(PRODUCT.' as p');
		$this->db->join(PRODUCT_ADDRESS_NEW.' as pa',"pa.productId=p.id","LEFT");
		$this->db->join(USERS.' as u',"u.id=p.user_id","LEFT");
		$this->db->join(RENTALENQUIRY.' as rq',"rq.prd_id=p.id","LEFT");
		$this->db->where($where1);
		$this->db->group_by('rq.id');
		$rental_details = $this->db->get();
		//echo $this->db->last_query(); die;
		//echo '<pre>';print_r($rental_details->result());die;
		$this->db->select('product_image');
		$this->db->from(PRODUCT_PHOTOS);
		$this->db->where($where2);
		$photoDetails = $this->db->get();
		//echo $this->db->last_query(); die;
		$prodimgArr = array();
		$proddetailArr = array();
		$check = array();
		$producttitle="";
		$productdesc="";
		$productprice="";
		$userimg="";
		$loginUserType="";
		$hostname="";
		$accommodates="";
		$bedroom="";
		$beds="";
		$bathrooms="";
		$country="";
		$state="";
		$city="";
		$post_code="";
		$address="";
		$latitude="";
		$longitude="";
		$minimum_stay="";
		$cancellation="";
		$house_rules="";
		$list_name="";
		$rental_id="";
		$host_id="";
		$host_email="";
		$user_currency="";
		$user_about="";
		$price_perweek="";
		$price_permonth="";
		$email_verified="";
		$ph_verified="";
		$id_verified="";
		$listarr = array();
		$datefrom = "";
		$dateto = "";
		$home_type = "";
		$member_since = "";
		$facebook = "";
		$google = "";
		$userAddress = "";
		$hostabout = "";
		foreach($photoDetails->result() as $rental_detail){ 
			if($rental_detail->product_image != ''){
			$p_img = explode('.',$rental_detail->product_image);	
			$pro_img = $p_img[0].'.jpg';
			$proImage = $pro_img;
			}else{
				$proImage = 'no_image.jpg';
			}
			$prodimgArr[] = array("product_image" =>$proImage);
		}
		//print_r($rental_details->result());die;
		foreach($rental_details->result() as $rental_detail){ 
			if($rental_detail->checkin != ''){
				$checkin = $rental_detail->checkin;
				$checkout = $rental_detail->checkout;
			}else{
				$checkin = '';
				$checkout = '';
			}
			$producttitle = $rental_detail->product_title;
			$productdesc = $rental_detail->description;
			
			if($_GET['code'] != ''){
			$condition = array('currency_type'=>$_GET['code']);
			$checkUser = $this->mobile_model->get_all_details (CURRENCY, $condition );
			$productprice = $rental_detail->price * $checkUser->row()->currency_rate;
			}else{
			$productprice = $rental_detail->price;
			}
			
			
			$check[] = array("checkin" =>$checkin,'checkout'=>$checkout);
			$hostname = $rental_detail->user_name;
			if($rental_detail->thumbnail != ''){
				$userimg = $rental_detail->thumbnail;
			}else{
				$userimg = 'user_thumb.jpg';
			}
			if($rental_detail->loginUserType != ''){
				$loginUserType = $rental_detail->loginUserType;
			}else{
				$loginUserType = '';
			}
			//$userimg = $rental_detail->thumbnail;
			$listing_json = $rental_detail->listings;
			$listing_decode = json_decode($listing_json);
			
			$accommodates = $rental_detail->accommodates;
			$bedroom = $listing_decode->Bedrooms;
			$beds = $listing_decode->Beds;
			if($listing_decode->Bathrooms == ''){
			$bathrooms = ''; 
			}else{ 
			$bathrooms = $listing_decode->Bathrooms;
			}
			if($bedroom== '')$bedroom = $rental_detail->bedrooms;
			if($beds == '')$beds = $rental_detail->beds;
			if($bathrooms == '')$bathrooms = $rental_detail->noofbathrooms;
			if($rental_detail->country != ''){
			$country = $rental_detail->country;
			}else{
			$country = '';
			}
			if($rental_detail->state != ''){
			$state = $rental_detail->state;
			}else{
			$state = '';
			}
			if($rental_detail->city != ''){
			$city = $rental_detail->city;
			}else{
			$city = '';
			}
			
			if($rental_detail->zip != '') {
			$post_code = $rental_detail->zip;
			}else{
			$post_code ='';
			}
			
			if($rental_detail->address !='') {
			$address = $rental_detail->address;
			}else{
			$address ='';
			}
			
			if($rental_detail->latitude !='') {
			$latitude = $rental_detail->latitude;
			}else{
			$latitude = '';
			}
			
			if($rental_detail->longitude !='') {
			$longitude = $rental_detail->longitude;
			}else{
			$longitude ='';
			}
			
			if($rental_detail->minimum_stay !='') {
			$minimum_stay = $rental_detail->minimum_stay;
			}else{
			$minimum_stay ='';
			}
			
			if($rental_detail->cancellation_policy !='') {
			$cancellation = $rental_detail->cancellation_policy;
			}else{
			$cancellation = '';
			}
			
			
			$rental_id = $_GET['prod_id'];
			$host_id = $rental_detail->user_id;
			
			
			if($rental_detail->about != '') {
			$user_about = $rental_detail->about;
			}else{
			$user_about ='';
			}
			
			if($rental_detail->price_perweek != '') {
			$price_perweek = $rental_detail->price_perweek;
			}else{
			$price_perweek = '';
			}
			
			if($rental_detail->price_permonth != '') {
			$price_permonth = $rental_detail->price_permonth;
			}else{
			$price_permonth = '';
			}
			
			if($_GET['code'] != '') {
			$user_currency = $checkUser->row()->currency_symbols;
			}else{
			$user_currency = '$';
			}
			$hostemail = $this->mobile_model->get_all_details ( USERS, array('id'=>$host_id) );
			$host_email = $hostemail->row()->email;
			if($rental_detail->house_rules == ''){
			$house_rules = '';
			}else{
			$house_rules = $rental_detail->house_rules;
			}
			$list_name = $rental_detail->list_name;
			
			$datefrom = $rental_detail->datefrom;
			$dateto = $rental_detail->dateto;
			$home_type = $rental_detail->home_type;
			$email_verified=$rental_detail->is_verified;
			$ph_verified=$rental_detail->ph_verified;
			$id_verified=$rental_detail->id_verified;
			$member_since=$rental_detail->created;
			$facebook=$rental_detail->facebook;
			$google=$rental_detail->google;
			$userAddress=$rental_detail->address;
		}
		$list = $rental_details->row()->list_name;
		$list_value = explode(',',$list);
		for($i=0;$i<count($list_value);$i++) {
			$list_detail = $this->mobile_model->get_all_details (LIST_VALUES, array('id'=>$list_value[$i]) );
			if($list_name != '')$list_name = $list_detail->row()->list_value;
			else $list_name="";
			if($list_img != '')$list_img = $list_detail->row()->image;
			else $list_img="";
			$listarr[] = array('list_name'=>$list_name,'list_image'=>$list_img);
		}
		$json_encode = json_encode(array("product_image" => $prodimgArr,"check"=>$check,"defaultproducttitle"=>(string)$producttitle,"productdesc"=>$productdesc,"productprice"=>$productprice,"hostimg"=>$userimg,"loginUserType"=>$loginUserType,"hostname"=>$hostname,"accommodates"=>$accommodates,"bedrooms"=>$bedroom,"beds"=>$beds,"fav"=>$fav,"bathrooms"=>$bathrooms,"country"=>$country,"state"=>$state,"city"=>$city,"post_code"=>$post_code,"address"=>$address,"latitude"=>$latitude,"longitude"=>$longitude,"minimum_stay"=>$minimum_stay,"cancellation"=>$cancellation,"house_rules"=>$house_rules,"list_name"=>$list_name,'rental_id'=>$rental_id,'host_id'=>$host_id,'host_email'=>$host_email,'user_currency'=>$user_currency,'user_about'=>$user_about,'week_price'=>$price_perweek,'month_price'=>$price_permonth,'url'=>'http://airbnb.zoplay.com/rental/','datefrom'=>$datefrom,'dateto'=>$dateto,'home_type'=>$home_type,'list_details'=>$listarr, 'email_verified'=>$email_verified, 'ph_verified'=>$ph_verified, 'id_verified'=>$id_verified, 'member_since'=>$member_since, 'facebook'=>$facebook, 'google'=>$google, 'userAddress'=>$userAddress, 'hostabout'=>$hostabout));
		echo $json_encode;
		}
		
		public function search_detail() {
		
			$q = $_GET['query'];
			$this->db->select('c.name,c.latitude,c.longitude,c.id,states_list.name as State,country_list.name as country_name');
			$this->db->from(CITY.' as c');
			$this->db->join(STATE_TAX.' as states_list',"states_list.id=c.stateid","LEFT");
			$this->db->join(COUNTRY_LIST.' as country_list',"country_list.id=states_list.countryid","LEFT");
			$this->db->like('states_list.name', $q);
			$this->db->or_like('c.name', $q);
			$this->db->limit(20);
			$this->db->order_by('c.name',asc);
			$this->db->order_by('states_list.name',asc);
			$query = $this->db->get();
			$search_res = $query->result_array();
			//echo $this->db->last_query();  die;
			$row_set = array();
			$state_arr = array();
			foreach ($search_res as $result){
				if (!in_array($result['State'],$state_arr)){
					$row_set[] = array(
						'search_result' => $result['State'].','.$result['country_name'],
						'id'=>$result['id'],'lat'=>$result['latitude'],'long'=>$result['longitude']
						
					);
					$state_arr[] = $result['State'];
				}
				$row_set[] = array(
					   'search_result' => $result['name'].','.$result['State'].','.$result['country_name'],
					   'id'=>$result['id'],'lat'=>$result['latitude'],'long'=>$result['longitude']
					   
				);
			//$row_set[] .= array('lat'=>$result['latitude'],'long'=>$result['longitude']);
      		}
			$json_encode = json_encode(array("search_list"=>$row_set));
     			 echo $json_encode;
			
		
		
		}
	public function mobile_bkguide() {
		$prd_id = $_GET['pro_id'];
		$this->db->select('pb.*,pa.post_code,pa.address,pa.apt,
								c.name as country_name,
								s.name as state_name,
								ci.name as city_name,
								p.product_name,p.product_title,p.price,p.currency,
								u.firstname,u.image,
								rq.id as EnqId,rq.booking_status,rq.checkin,rq.checkout,rq.dateAdded,rq.user_id as GestId,rq.numofdates as noofdates,rq.approval as approval,rq.serviceFee,rq.totalAmt,rq.Bookingno as Bookingno');
			$this->db->from(PRODUCT_BOOKING.' as pb');
			//$this->db->from(PAYMENT.' as py');
			$this->db->join(PRODUCT_ADDRESS.' as pa' , 'pa.product_id = pb.product_id','left');
			$this->db->join(LOCATIONS.' as c' , 'c.id = pa.country','left');
			$this->db->join(STATE_TAX.' as s' , 's.id = pa.state','left');
			$this->db->join(CITY.' as ci' , 'ci.id = pa.city','left');
			$this->db->join(PRODUCT.' as p' , 'p.id = pb.product_id');
			$this->db->join(RENTALENQUIRY.' as rq' , 'p.id = rq.prd_id');
			$this->db->join(USERS.' as u' , 'u.id = rq.user_id');
			$this->db->where('p.user_id = '.$prd_id);
			$this->db->where('rq.renter_id = '.$prd_id);
			$this->db->where('rq.booking_status != "Enquiry"');
			$this->db->group_by('rq.id');
			$this->db->order_by('rq.dateAdded','desc');
			return $this->db->get();
			echo $this->db->last_query();die;
	}
		
		
	public function mobile_login(){
	
		$email = $_POST['u_email'];
		$user_password = $_POST['u_psd'];
		$pwd = md5($user_password);
		if($_POST['device_type'] == 'android') {
		if($_POST['u_key'] != '') {
			$data = array(
					'mobile_key'=>''
					);
			$condition = array(
					'mobile_key'=>$_POST['u_key']
					);
			$this->mobile_model->update_details(USERS ,$data ,$condition);
			$data = array(
					'mobile_key'=>$_POST['u_key']
					);
			$condition = array(
					'email'=>$_POST['u_email']	
					);
			$this->mobile_model->update_details(USERS ,$data ,$condition);
		}
		}
		
		if($_POST['device_type'] == 'ios') {
		if($_POST['u_key'] != '') {
			$data = array(
					'ios_key'=>''
					);
			$condition = array(
					'ios_key'=>$_POST['u_key']
					);
			$this->mobile_model->update_details(USERS ,$data ,$condition);
			$data = array(
					'ios_key'=>$_POST['u_key']
					);
			$condition = array(
					'email'=>$_POST['u_email']	
					);
			$this->mobile_model->update_details(USERS ,$data ,$condition);
		}
		}
		if (valid_email ( $email )) {
			$condition = array (
					'email' => $email,
					'password' => $pwd,
					'status' => 'Active' 
			);	
		$checkUser = $this->mobile_model->get_all_details (USERS, $condition );
		if ($checkUser->num_rows () == '1') {
		$status = 'You are Logged In successfully';
		$firstname = $checkUser->row()->firstname;
		$lastname = $checkUser->row()->lastname;
		$email = $checkUser->row()->email;
		$key = $checkUser->row()->mobile_key;
		if($checkUser->row()->image != '') {
		$img = $checkUser->row()->image;
		}else{
		$img = 'profile.jpg';
		}
		$json_encode = json_encode(array('message'=>$status,'firstname'=>$firstname,'lastname'=>$lastname,'email'=>$email,'userimage'=>$img,'key'=>$key));
		}else{
		$status = 'Invalid login details';
		$json_encode = json_encode(array('message'=>$status));
		}
		}
		echo $json_encode;
	}
	public function mobile_signup(){
	
		$firstname = $_POST['u_fn'];
		$lastname = $_POST['u_ln'];
		$email = $_POST['u_email'];
		$pwd = $_POST['u_psd'];
		$key = $_POST['u_key'];

		if($_POST['login_type'] == 'facebook' || $_POST['login_type'] == 'google' ) {
			$this->db->select('id');
			$this->db->from(USERS);
			$this->db->where('email',$email);
			$this->db->where('status','Active');
			$facebookQuery = $this->db->get();
			
		if($facebookQuery->num_rows() > 0 ){
			if($_POST['device_type'] == 'android' ) {
				if($_POST['u_key'] != '') {
					$data = array(
							'mobile_key'=>''
							);
					$condition = array(
							'mobile_key'=>$_POST['u_key']
							);
					$this->mobile_model->update_details(USERS ,$data ,$condition);
					$data = array(
							'mobile_key'=>$_POST['u_key']
							);
					$condition = array(
							'email'=>$_POST['u_email']	
							);
					$this->mobile_model->update_details(USERS ,$data ,$condition);
				}
			}
			
			if($_POST['device_type'] == 'ios' ) {
				if($_POST['u_key'] != '') {
					$data = array(
							'ios_key'=>''
							);
					$condition = array(
							'ios_key'=>$_POST['u_key']
							);
					$this->mobile_model->update_details(USERS ,$data ,$condition);
					$data = array(
							'ios_key'=>$_POST['u_key']
							);
					$condition = array(
							'email'=>$_POST['u_email']	
							);
					$this->mobile_model->update_details(USERS ,$data ,$condition);
				}
			}
			
			
				$returnStr .= 'You are Logged In successfully';
				$json_encode = json_encode(array('message'=>$returnStr,'firstname'=>$firstname,'lastname'=>$lastname,'email'=>$email,'key'=>$key));
				
			}else{
				if($_FILES['photo1'] != ''){
					
					$uploaddir = "images/users/";
					$data = file_get_contents($_FILES['photo1']['tmp_name']);
					$image = imagecreatefromstring( $data );
					$imgname=time().".jpg";
					imagejpeg($image,$uploaddir.$imgname, 99);
				
				}else{
					$imgname = '';
				}
				
				$orgPass = time();
				$pwd = md5($orgPass);
				$dataArr = array('firstname'=>$firstname,'lastname'=>$lastname,'user_name'=>$firstname,'image'=>$imgname,'group'=>'User','email'=>$email,'password'=>$pwd,'status'=>'Active','loginUserType'=>$_POST['login_type'],'is_verified'=>'No','created'=>date('Y-m-d H:i:s'),'mobile_key'=>$key);
				
				$this->mobile_model->simple_insert(USERS,$dataArr);
				
				$returnStr .= 'You are Registered successfully';
				$json_encode = json_encode(array('message'=>$returnStr,'firstname'=>$firstname,'lastname'=>$lastname,'email'=>$email,'key'=>$key));
			}		
	 	}else{
			if (valid_email ( $email )) {
				$condition = array (
						'email' => $email 
				);
				$duplicateMail = $this->mobile_model->get_all_details( USERS, $condition );
				if ($duplicateMail->num_rows () > 0) {
					$returnStr .= 'Email id already exists'; 
				} else {
	
		            $expireddate = date ( 'Y-m-d', strtotime ( '+15 days' ) );
					$this->mobile_model->insertUserQuick ( $firstname, $lastname, $email, $pwd, $expireddate,$key);
					$usrDetails = $this->mobile_model->get_all_details ( USERS, $condition );
					$this->send_confirm_mail ( $usrDetails );
					$returnStr .= 'Successfully registered';
				}
				}
				if($returnStr == 'Email id already exists'){
					$json_encode = json_encode(array('message'=>$returnStr));
				}else{
					$json_encode = json_encode(array('message'=>$returnStr,'firstname'=>$firstname,'lastname'=>$lastname,'email'=>$email,'key'=>$key));
				}
		}
	     			 echo $json_encode;
	}
	public function mobile_forgotpsd(){
		$email=$_POST['u_email'];
		//$email='selvakumarvbtech@gmail.com';
		if (valid_email ( $email )) {
				$condition = array (
						'email' => $email 
				);
				$checkUser = $this->mobile_model->get_all_details ( USERS, $condition );
				if ($checkUser->num_rows () == '1') {
					$pwd = $this->get_rand_str ( '6' );
					$newdata = array (
							'password' => md5 ( $pwd ) 
					);
					$condition = array (
							'email' => $email 
					);
					$this->mobile_model->update_details ( USERS, $newdata, $condition );
					$this->send_user_password ( $pwd, $checkUser );
					$returnStr .= 'New password sent to your email';
				} else {
					$returnStr .= 'email id not matched';
				}
			} 
		$json_encode = json_encode(array('message'=>$returnStr));
		echo $json_encode;
	}
	
	public function send_user_password($pwd = '', $query) {
		$newsid = '5';
		$template_values = $this->mobile_model->get_newsletter_template_details ( $newsid );
		$adminnewstemplateArr = array (
				'email_title' => $this->config->item ( 'email_title' ),
				'logo' => $this->data ['logo'] 
		);
		extract ( $adminnewstemplateArr );
		$subject = 'From: ' . $this->config->item ( 'email_title' ) . ' - ' . $template_values ['news_subject'];
		$message .= '<!DOCTYPE HTML>
			<html>
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
			<meta name="viewport" content="width=device-width"/>
			<title>' . $template_values ['news_subject'] . '</title>
			<body>';
		include ('./newsletter/registeration' . $newsid . '.php');
		
		$message .= '</body>
			</html>';
		
		if ($template_values ['sender_name'] == '' && $template_values ['sender_email'] == '') {
			$sender_email = $this->config->item ( 'site_contact_mail' );
			$sender_name = $this->config->item ( 'email_title' );
		} else {
			$sender_name = $template_values ['sender_name'];
			$sender_email = $template_values ['sender_email'];
		}
		
		$email_values = array (
				'mail_type' => 'html',
				'from_mail_id' => $sender_email,
				'mail_name' => $sender_name,
				'to_mail_id' => $query->row ()->email,
				'subject_message' => 'Password Reset',
				'body_messages' => $message 
		);

		$email_send_to_common = $this->mobile_model->common_email_send ( $email_values );
		
	}
	
	public function send_confirm_mail($userDetails = '') {
	
		$uid = $userDetails->row ()->id;
		$email = $userDetails->row ()->email;
		$name = $userDetails->row ()->firstname.$userDetails->row ()->lastname;
		
		$randStr = $this->get_rand_str ( '10' );
		$condition = array (
				'id' => $uid 
		);
		$dataArr = array (
				'verify_code' => $randStr 
		);
		$this->mobile_model->update_details ( USERS, $dataArr, $condition );
		$newsid = '3';
		$template_values = $this->mobile_model->get_newsletter_template_details( $newsid );
		
		$user=$userDetails->row ()->firstname.' '.$userDetails->row ()->lastname;
		$cfmurl = base_url () . 'site/user/confirm_register/' . $uid . "/" . $randStr . "/confirmation";
		$subject = 'From: ' . $this->config->item ( 'email_title' ) . ' - ' . $template_values ['news_subject'];
		$adminnewstemplateArr = array (
				'email_title' => $this->config->item ( 'email_title' ),
				'logo' => $this->data ['logo'],
				'username'=>$name
		);
		extract ( $adminnewstemplateArr );
		$header .= "Content-Type: text/plain; charset=ISO-8859-1\r\n";
		
		$message .= '<body>';
		include ('./newsletter/registeration' . $newsid . '.php');
		
		$message .= '</body>
			';
		
		if ($template_values ['sender_name'] == '' && $template_values ['sender_email'] == '') {
			$sender_email = $this->data ['siteContactMail'];
			$sender_name = $this->data ['siteTitle'];
		} else {
			$sender_name = $template_values ['sender_name'];
			$sender_email = $template_values ['sender_email'];
		}
		$email_values = array (
				'mail_type' => 'html',
				'from_mail_id' => $sender_email,
				'mail_name' => $sender_name,
				'to_mail_id' => $email,
				'subject_message' => $template_values ['news_subject'],
				'body_messages' => trim($message)
		);
		$email_send_to_common = $this->mobile_model->common_email_send ( $email_values );
	}
	
	public function list_values() {
		$select_qry = "select id,list_value,image from fc_list_values where status='Active'";
		$list_values = $this->mobile_model->ExecuteQuery($select_qry);
		$listvalueArr = array();
		foreach($list_values->result() as $list_value) {
		$listvalueArr[] = array("id" =>$list_value->id,"listvalues"=>$list_value->list_value,"image"=>$list_value->image);
		}
		$json_encode = json_encode(array("list_values"=>$listvalueArr));
		echo $json_encode;
	}
	
	public function currency_values() {
	
			$select_qry = "select id,currency_symbols,currency_type from fc_currency";
			
			$currency_values = $this->mobile_model->ExecuteQuery($select_qry);
			
			$listvalueArr = array();
			foreach($currency_values->result() as $list_value) {
			$listvalueArr[] = array("id" =>$list_value->id,"country_symbols"=>$list_value->currency_symbols,"currency_type"=>$list_value->currency_type);
			}
			$json_encode = json_encode(array("list_values"=>$listvalueArr));
			echo $json_encode;
	}
	
	public function mobile_edit_list_value() {
	
	}
	
	public function mobile_delete_image() {
		if($_POST['imageIds'] != ''){
			$imgIds = explode(',', $_POST['imageIds']);
			foreach($imgIds as $imgId)
			{
				$this->db->where('id', $imgId);
				$this->db->delete(PRODUCT_PHOTOS);
			}
			$res = 'Success';
			$json_encode = json_encode(array("status"=>$res));
			echo $json_encode;
		}
		else{
			$res = 'Failed';
			$json_encode = json_encode(array("status"=>$res));
			echo $json_encode;
		}
	}
	public function mobile_list_value() {
		
		$email = $_POST['Email'];
		$p_address = $_POST['p_location'];
		$property_type = $_POST['property_type'];
		$t_guest= $_POST['t_guest'];
		$t_bed= $_POST['t_bed'];
		$t_bedroom= $_POST['t_bedroom'];
		
		$bathrooms = $_POST['t_bathroom'];
		
		if($_POST['list_space'] == 1){
		$list_space = 'Entire home/apt';
		}elseif($_POST['list_space'] == 2){
		$list_space = 'Private room';
		}else{
		$list_space = 'Shared room';
		}
		$currency = $_POST['P_Currency'];
		if($currency == '')$currency = 'USD';
		$condition = array('email'=>$email,'status'=>'Active');
		$this->data['checkUser'] = $this->mobile_model->get_all_details(USERS,$condition);
		$id = $this->data['checkUser']->row()->id;
		
		$data = array('room_type'=>$this->input->post('room_type1'),
				'room_type'=>$list_space,
				'home_type'=>$property_type,
				'accommodates'=>$t_guest,
				'bedrooms'=>$t_bedroom,
				'beds'=>$t_bed,
				'noofbathrooms'=>$bathrooms,
				'user_id'=>$id,
				'currency'=>$currency,
				'status'=>'UnPublish',
				'through'=>'mobile'
			);

		$this->mobile_model->simple_insert(PRODUCT,$data);
		//echo $this->db->last_query();die;
		$getInsertId=$this->mobile_model->get_last_insert_id();
		
		$street = '';
		$street1 = '';
		$area = '';
		$location = '';
		$city = '';
		$state = '';
		$country = '';
		$lat = '';
		$long = '';
		$zip = '';
		$address = str_replace(" ", "+", $p_address);
		$json = file_get_contents("https://maps.google.com/maps/api/geocode/json?address=$address&sensor=false&key=$google_map_api");
		$json = json_decode($json);
		
		$newAddress = $json->{'results'}[0]->{'address_components'};
		foreach($newAddress as $nA)
		{
			if($nA->{'types'}[0] == 'route')$street = $nA->{'long_name'};
			if($nA->{'types'}[0] == 'sublocality_level_2')$street1 = $nA->{'long_name'};
			if($nA->{'types'}[0] == 'sublocality_level_1')$area = $nA->{'long_name'};
			if($nA->{'types'}[0] == 'locality')$location = $nA->{'long_name'};
			if($nA->{'types'}[0] == 'administrative_area_level_2')$city = $nA->{'long_name'};
			if($nA->{'types'}[0] == 'administrative_area_level_1')$state = $nA->{'long_name'};
			if($nA->{'types'}[0] == 'country')$country = $nA->{'long_name'};
			if($nA->{'types'}[0] == 'postal_code')$zip = $nA->{'long_name'};
		}
		if($city == '')
		$city = $location;
		
		$lat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
		$lang = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
		
		$data = array('address' => $p_address,'productId' => $getInsertId,'street' => $street,'area' => $area,'location' => $location,'city' => $city,'state' => $state,'country' => $country,'lang' => $lang,'lat' => $lat,'zip' => $zip);
		
		$this->mobile_model->simple_insert(PRODUCT_ADDRESS_NEW,$data);
		
		$inputArr = array('product_id' =>$getInsertId);
		
		$this->mobile_model->simple_insert(PRODUCT_BOOKING,$inputArr);

		$inputArr = array('id' =>$getInsertId);
		
		$this->mobile_model->simple_insert(SCHEDULE,$inputArr);
		
		$this->mobile_model->update_details(USERS,array('group'=>'Seller'),array('id'=>$id));
		
		$res = 'successfully added';
		
		$condition1 = array('id'=>$getInsertId);
		
		$pDetails = $this->mobile_model->get_all_details(PRODUCT,$condition1);
		
		$currency = $pDetails->row()->currency;
		
		if($currency == '')$currency = 'USD';
		
		$json_encode = json_encode(array("status"=>$res,'p_id'=>$getInsertId,'currency'=>$currency,'location'=>$p_address, "lat"=>$lat,"long"=>$lang));
		
		echo $json_encode;
	}
	
	public function mobile_update_list() {
		if($_POST['P_Title'] !='' && $_POST['P_Id'] != '') {
				$data = array(
				'product_title'=>$_POST['P_Title']
					);
			
			$condition = array(
				'id'=>$_POST['P_Id']	
					);
			$this->mobile_model->update_details(PRODUCT ,$data ,$condition);
		}
		if($_POST['P_Price'] !='' && $_POST['P_Id'] != '') {
			$data = array(
				'price'=>$_POST['P_Price']
					);
			
			$condition = array(
				'id'=>$_POST['P_Id']	
					);
			$this->mobile_model->update_details(PRODUCT ,$data ,$condition);
		}
		if($_POST['P_Summary'] !='' && $_POST['P_Id'] != '') {
			$data = array(
				'description'=>$_POST['P_Summary']
					);
			
			$condition = array(
				'id'=>$_POST['P_Id']	
					);
			$this->mobile_model->update_details(PRODUCT ,$data ,$condition);
		}
		if($_POST['P_Currency'] != '' && $_POST['P_Id'] != '' )
		{
			if($_POST['P_Currency'] == '')$c_value = 'USD';
			else $c_value = $_POST['P_Currency'];
			$data = array(
				'currency'=>$c_value
				);
			
			$condition = array(
				'id'=>$_POST['P_Id']	
				);
			$this->mobile_model->update_details(PRODUCT ,$data ,$condition);
		}
		
		if($_POST['p_address'] != '' && $_POST['P_Id'] != '' )
		{
			$p_address = $_POST['p_address'];
			$street = '';
			$street1 = '';
			$area = '';
			$location = '';
			$city = '';
			$state = '';
			$country = '';
			$lat = '';
			$long = '';
			$zip = '';
			$address = str_replace(" ", "+", $p_address);
			$json = file_get_contents("https://maps.google.com/maps/api/geocode/json?address=$address&sensor=false&key=$google_map_api");
			$json = json_decode($json);
			
			$newAddress = $json->{'results'}[0]->{'address_components'};
			foreach($newAddress as $nA)
			{
				if($nA->{'types'}[0] == 'route')$street = $nA->{'long_name'};
				if($nA->{'types'}[0] == 'sublocality_level_2')$street1 = $nA->{'long_name'};
				if($nA->{'types'}[0] == 'sublocality_level_1')$area = $nA->{'long_name'};
				if($nA->{'types'}[0] == 'locality')$location = $nA->{'long_name'};
				if($nA->{'types'}[0] == 'administrative_area_level_2')$city = $nA->{'long_name'};
				if($nA->{'types'}[0] == 'administrative_area_level_1')$state = $nA->{'long_name'};
				if($nA->{'types'}[0] == 'country')$country = $nA->{'long_name'};
				if($nA->{'types'}[0] == 'postal_code')$zip = $nA->{'long_name'};
			}
			if($city == '')
			$city = $location;
			
			$lat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
			$lang = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
			
			$data = array('address' => $p_address,'street' => $street,'area' => $area,'location' => $location,'city' => $city,'state' => $state,'country' => $country,'lang' => $lang,'lat' => $lat,'zip' => $zip);
			
			$condition = array(
				'productId'=>$_POST['P_Id']	
				);
			$this->mobile_model->update_details(PRODUCT_ADDRESS_NEW ,$data ,$condition);
		}
		if($_FILES['photo'] != '' && $_POST['P_Id'] != '') { 
			$uploaddir = "server/php/rental/";
			//$data = file_get_contents($_FILES['photo']['tmp_name']);
			//$image = imagecreatefromstring( $data );
			//echo 'Hi';die;
			$imgname=time().".jpg";
			//imagejpeg($image,$uploaddir.$imgname, 99);
			//echo 'Hi';die;
			move_uploaded_file($_FILES['photo']['tmp_name'], $uploaddir.$imgname);
			@copy($uploaddir.$imgname, './server/php/rental/mobile/'.$imgname);
			$imageName=$imgname;
			/* $image_name=$imgname;
			$newname=$uploaddir.$image_name;
			
			
			$timeImg=time();
			@copy($imgname, './server/php/rental/mobile/'.$imgname);
			$target_file=$uploaddir.$image_name;
			$imageName=$imgname; 
			$option=$this->getImageShape(800,750,$target_file);
			
			$resizeObj = new Resizeimage($target_file);	
			$resizeObj -> resizeImage(800, 750, $option);
			$resizeObj -> saveImage($uploaddir.'mobile/'.$imageName, 100);
			$this->ImageCompress($uploaddir.'mobile/'.$imageName);
			@copy($uploaddir.'mobile/'.$imageName, $uploaddir.'mobile/'.$imageName); */

			$inputArr3 = array(
				'product_id' =>$_POST['P_Id'],
				'product_image'=>$imgname,
				'mproduct_image'=>$imageName
					); 
			$this->mobile_model->simple_insert(PRODUCT_PHOTOS,$inputArr3);
			$getPhotoInsertId=$this->mobile_model->get_last_insert_id();
			$latLong = $this->mobile_model->get_all_details(PRODUCT_ADDRESS_NEW, array('productId'=>$_POST['P_Id']));
			$res = 'successfully added';	
			$json_encode = json_encode(array("status"=>$res, "image"=>$imgname,"imgId"=>$getPhotoInsertId, "lat"=>$latLong->row()->lat,"long"=>$latLong->row()->lang));
			echo $json_encode;die;
		}
		if($_FILES['photo1'] != '' && $_POST['P_Id'] != '') {
			$uploaddir = "server/php/rental/";
			$data = file_get_contents($_FILES['photo1']['tmp_name']);
			$image = imagecreatefromstring( $data );
			$imgname=time().".jpg";
			imagejpeg($image,$uploaddir.$imgname, 99);


			$image_name=$imgname;
			$newname=$uploaddir.$image_name;
			
			
			$timeImg=time();
			@copy($imgname, './server/php/rental/mobile/'.$imgname);
			$target_file=$uploaddir.$image_name;
			$imageName=$imgname; 
			$option=$this->getImageShape(800,750,$target_file);
			
			$resizeObj = new Resizeimage($target_file);	
			$resizeObj -> resizeImage(800, 750, $option);
			$resizeObj -> saveImage($uploaddir.'mobile/'.$imageName, 100);
			$this->ImageCompress($uploaddir.'mobile/'.$imageName);
			@copy($uploaddir.'mobile/'.$imageName, $uploaddir.'mobile/'.$imageName);

			$inputArr3 = array(
				'product_id' =>$_POST['P_Id'],
				'product_image'=>$imgname,
				'mproduct_image'=>$imageName
					);
			$this->mobile_model->simple_insert(PRODUCT_PHOTOS,$inputArr3);
		}
		if($_FILES['photo2'] != '' && $_POST['P_Id'] != '') {
			$uploaddir = "server/php/rental/";
			$data = file_get_contents($_FILES['photo2']['tmp_name']);
			$image = imagecreatefromstring( $data );
			$imgname=time().".jpg";
			imagejpeg($image,$uploaddir.$imgname, 99);


			$image_name=$imgname;
			$newname=$uploaddir.$image_name;
			
			
			$timeImg=time();
			@copy($imgname, './server/php/rental/mobile/'.$imgname);
			$target_file=$uploaddir.$image_name;
			$imageName=$imgname; 
			$option=$this->getImageShape(500,350,$target_file);
			
			$resizeObj = new Resizeimage($target_file);	
			$resizeObj -> resizeImage(500, 350, $option);
			$resizeObj -> saveImage($uploaddir.'mobile/'.$imageName, 100);
			$this->ImageCompress($uploaddir.'mobile/'.$imageName);
			@copy($uploaddir.'mobile/'.$imageName, $uploaddir.'mobile/'.$imageName);

			$inputArr3 = array(
				'product_id' =>$_POST['P_Id'],
				'product_image'=>$imgname,
				'mproduct_image'=>$imageName
					);
			$this->mobile_model->simple_insert(PRODUCT_PHOTOS,$inputArr3);
		}
		if($_FILES['photo3'] != '' && $_POST['P_Id'] != '') {
			$uploaddir = "server/php/rental/";
			$data = file_get_contents($_FILES['photo3']['tmp_name']);
			$image = imagecreatefromstring( $data );
			$imgname=time().".jpg";
			imagejpeg($image,$uploaddir.$imgname, 99);


			$image_name=$imgname;
			$newname=$uploaddir.$image_name;
			
			
			$timeImg=time();
			@copy($imgname, './server/php/rental/mobile/'.$imgname);
			$target_file=$uploaddir.$image_name;
			$imageName=$imgname; 
			$option=$this->getImageShape(500,350,$target_file);
			
			$resizeObj = new Resizeimage($target_file);	
			$resizeObj -> resizeImage(500, 350, $option);
			$resizeObj -> saveImage($uploaddir.'mobile/'.$imageName, 100);
			$this->ImageCompress($uploaddir.'mobile/'.$imageName);
			@copy($uploaddir.'mobile/'.$imageName, $uploaddir.'mobile/'.$imageName);

			$inputArr3 = array(
				'product_id' =>$_POST['P_Id'],
				'product_image'=>$imgname,
				'mproduct_image'=>$imageName
					);
			$this->mobile_model->simple_insert(PRODUCT_PHOTOS,$inputArr3);
		}
		if($_FILES['photo4'] != '' && $_POST['P_Id'] != '') {
			$uploaddir = "server/php/rental/";
			$data = file_get_contents($_FILES['photo4']['tmp_name']);
			$image = imagecreatefromstring( $data );
			$imgname=time().".jpg";
			imagejpeg($image,$uploaddir.$imgname, 99);


			$image_name=$imgname;
			$newname=$uploaddir.$image_name;
			
			
			$timeImg=time();
			@copy($imgname, './server/php/rental/mobile/'.$imgname);
			$target_file=$uploaddir.$image_name;
			$imageName=$imgname; 
			$option=$this->getImageShape(500,350,$target_file);
			
			$resizeObj = new Resizeimage($target_file);	
			$resizeObj -> resizeImage(500, 350, $option);
			$resizeObj -> saveImage($uploaddir.'mobile/'.$imageName, 100);
			$this->ImageCompress($uploaddir.'mobile/'.$imageName);
			@copy($uploaddir.'mobile/'.$imageName, $uploaddir.'mobile/'.$imageName);

			$inputArr3 = array(
				'product_id' =>$_POST['P_Id'],
				'product_image'=>$imgname,
				'mproduct_image'=>$imageName
					);
			$this->mobile_model->simple_insert(PRODUCT_PHOTOS,$inputArr3);
		}
		if($_FILES['photo5'] != '' && $_POST['P_Id'] != '') {
			$uploaddir = "server/php/rental/";
			$data = file_get_contents($_FILES['photo5']['tmp_name']);
			$image = imagecreatefromstring( $data );
			$imgname=time().".jpg";
			imagejpeg($image,$uploaddir.$imgname, 99);


			$image_name=$imgname;
			$newname=$uploaddir.$image_name;
			
			
			$timeImg=time();
			@copy($imgname, './server/php/rental/mobile/'.$imgname);
			$target_file=$uploaddir.$image_name;
			$imageName=$imgname; 
			$option=$this->getImageShape(500,350,$target_file);
			
			$resizeObj = new Resizeimage($target_file);	
			$resizeObj -> resizeImage(500, 350, $option);
			$resizeObj -> saveImage($uploaddir.'mobile/'.$imageName, 100);
			$this->ImageCompress($uploaddir.'mobile/'.$imageName);
			@copy($uploaddir.'mobile/'.$imageName, $uploaddir.'mobile/'.$imageName);

			$inputArr3 = array(
				'product_id' =>$_POST['P_Id'],
				'product_image'=>$imgname,
				'mproduct_image'=>$imageName
			);
			$this->mobile_model->simple_insert(PRODUCT_PHOTOS,$inputArr3);
		}
		if($_POST['P_Id'] != '' && $_POST['list_space'] !='')
		{
			$t_guest= $_POST['t_guest'];
			$t_bed= $_POST['t_bed'];
			$t_bedroom= $_POST['t_bedroom'];
			$property_type= $_POST['property_type'];

			if($_POST['t_bathroom'] == 1){
				$bathrooms = 'Private';
			}elseif($_POST['t_bathroom'] == 2){
				$bathrooms = 'Both';
			}else{
				$bathrooms = 'Shared';
			}

			if($_POST['P_Id'] != '' && $_POST['list_space'] == 1){
				$list_space = 'entire home/apt';
			}elseif($_POST['list_space'] == 2){
				$list_space = 'private room';
			}else{
				$list_space = 'Shared room';
			}
			
			$data = array(
				'room_type'=>$list_space,
				'home_type'=>$property_type,
				'accommodates'=>$t_guest,
				'bedrooms'=>$t_bedroom,
				'beds'=>$t_bed,
				'bathrooms'=>$bathrooms
			);

			$condition = array(
				'id'=>$_POST['P_Id']	
			);
			$this->mobile_model->update_details(PRODUCT ,$data ,$condition);
		}
		if($_POST['P_Id'] != '' && $_POST['d_space'] !='')
		{
			$data = array(
				'space'=>$_POST['d_space']
			);

			$condition = array(
				'id'=>$_POST['P_Id']	
			);
			$this->mobile_model->update_details(PRODUCT ,$data ,$condition);
		}
		if($_POST['P_Id'] != '' && $_POST['P_Title'] !='')
		{
			$data = array(
				'product_title'=>$_POST['P_Title']
			);

			$condition = array(
				'id'=>$_POST['P_Id']	
			);
			$this->mobile_model->update_details(PRODUCT ,$data ,$condition);
		}
		if($_POST['P_Id'] != '' && $_POST['P_Summary'] !='')
		{
			$data = array(
				'description'=>$_POST['P_Summary']
			);

			$condition = array(
				'id'=>$_POST['P_Id']	
			);
			$this->mobile_model->update_details(PRODUCT ,$data ,$condition);
		}
		if($_POST['P_Id'] != '' && $_POST['d_other'] !='')
		{
			$condition = array('email'=>$_POST['Email'],'status'=>'Active');

			$data = array(
				'other_thingnote'=>$_POST['d_other']
			);

			$condition = array(
				'id'=>$_POST['P_Id']	
			);
			$this->mobile_model->update_details(PRODUCT ,$data ,$condition);
		}
		if($_POST['P_Id'] != '' && $_POST['d_house_rules'] !='')
		{
			$data = array(
				'house_rules'=>$_POST['d_house_rules']
			);

			$condition = array(
				'id'=>$_POST['P_Id']	
			);
			$this->mobile_model->update_details(PRODUCT ,$data ,$condition);
		}
		if($_POST['P_Id'] != '' && $_POST['d_listvalue'] !='')
		{
			$data = array(
				'list_name'=>$_POST['d_listvalue']
			);

			$condition = array(
				'id'=>$_POST['P_Id']	
			);
			$this->mobile_model->update_details(PRODUCT ,$data ,$condition);
		}
		if($_POST['P_Id'] != '' && $_POST['l_week'] !='')
		{
			$data = array(
			'price_perweek'=>$_POST['l_week'],
			'price_permonth'=>$_POST['l_month']
			);

			$condition = array(
			'id'=>$_POST['P_Id']	
			);
			$this->mobile_model->update_details(PRODUCT ,$data ,$condition);
		}
		if($_POST['P_Id'] != '' && $_POST['P_Currency'] !='')
		{
			if($_POST['P_Currency'] == '')$c_value = 'USD';
			else $c_value = $_POST['P_Currency'];
			$data = array(
				'currency'=>$c_value
			);

			$condition = array(
				'id'=>$_POST['P_Id']	
			);
			$this->mobile_model->update_details(PRODUCT ,$data ,$condition);
		}
		$res = 'successfully added';
		$latLong = $this->mobile_model->get_all_details(PRODUCT_ADDRESS_NEW, array('productId'=>$_POST['P_Id']));
		$json_encode = json_encode(array("status"=>$res, "lat"=>$latLong->row()->lat,"long"=>$latLong->row()->lang));
		echo $json_encode;
	}
	
	public function mobile_listvalue() {
		$email = $_POST['Email'];
		$p_address = $_POST['p_address'];
		$p_location = $_POST['p_location'];
		$fulladdress = explode(',',$p_address);
		$lat = $_POST['P_Address_LatLon'];
		$log = explode(',',$lat);
		$property_type = $_POST['property_type'];
		
		$t_guest= $_POST['t_guest'];
		$t_bed= $_POST['t_bed'];
		$t_bedroom= $_POST['t_bedroom'];
		
		if($_POST['t_bathroom'] == 1){
		$bathrooms = 'Private';
		}elseif($_POST['t_bathroom'] == 2){
		$bathrooms = 'Both';
		}else{
		$bathrooms = 'Shared';
		}
		
		if($_POST['list_space'] == 1){
		$list_space = 'entire home/apt';
		}elseif($_POST['list_space'] == 2){
		$list_space = 'private room';
		}else{
		$list_space = 'Shared room';
		}
		
		$condition = array('email'=>$email,'status'=>'Active');
		$this->data['checkUser'] = $this->mobile_model->get_all_details(USERS,$condition);
		$id = $this->data['checkUser']->row()->id;
		//echo $this->data['checkUser']->num_rows();die;
		if($_POST['Email'] != '' && $_POST['P_Id'] != ''){
		//post value start
			if($_FILES['photo1'] != '') {
						
					$uploaddir = "server/php/rental/";
					$data = file_get_contents($_FILES['photo1']['tmp_name']);
					$image = imagecreatefromstring( $data );
					$imgname=time().".jpg";
					imagejpeg($image,$uploaddir.$imgname, 99);


					$image_name=$imgname;
					$newname=$uploaddir.$image_name;
					
					
					$timeImg=time();
					@copy($imgname, './server/php/rental/mobile/'.$imgname);
					$target_file=$uploaddir.$image_name;
					$imageName=$imgname; 
					$option=$this->getImageShape(800,750,$target_file);
					
					$resizeObj = new Resizeimage($target_file);	
					$resizeObj -> resizeImage(800, 750, $option);
					$resizeObj -> saveImage($uploaddir.'mobile/'.$imageName, 100);
					$this->ImageCompress($uploaddir.'mobile/'.$imageName);
					@copy($uploaddir.'mobile/'.$imageName, $uploaddir.'mobile/'.$imageName);

					$inputArr3 = array(
						'product_id' =>$_POST['P_Id'],
						'product_image'=>$imgname,
						'mproduct_image'=>$imageName
							);
					$this->mobile_model->simple_insert(PRODUCT_PHOTOS,$inputArr3);
					
					
			}
			if($_FILES['photo2'] != '') {
					$uploaddir = "server/php/rental/";
					$data = file_get_contents($_FILES['photo2']['tmp_name']);
					$image = imagecreatefromstring( $data );
					$imgname=time().".jpg";
					imagejpeg($image,$uploaddir.$imgname, 99);


					$image_name=$imgname;
					$newname=$uploaddir.$image_name;
					
					
					$timeImg=time();
					@copy($imgname, './server/php/rental/mobile/'.$imgname);
					$target_file=$uploaddir.$image_name;
					$imageName=$imgname; 
					$option=$this->getImageShape(500,350,$target_file);
					
					$resizeObj = new Resizeimage($target_file);	
					$resizeObj -> resizeImage(500, 350, $option);
					$resizeObj -> saveImage($uploaddir.'mobile/'.$imageName, 100);
					$this->ImageCompress($uploaddir.'mobile/'.$imageName);
					@copy($uploaddir.'mobile/'.$imageName, $uploaddir.'mobile/'.$imageName);

					$inputArr3 = array(
						'product_id' =>$_POST['P_Id'],
						'product_image'=>$imgname,
						'mproduct_image'=>$imageName
							);
					$this->mobile_model->simple_insert(PRODUCT_PHOTOS,$inputArr3);
					
			}
			if($_FILES['photo3'] != '') {
				$uploaddir = "server/php/rental/";
					$data = file_get_contents($_FILES['photo3']['tmp_name']);
					$image = imagecreatefromstring( $data );
					$imgname=time().".jpg";
					imagejpeg($image,$uploaddir.$imgname, 99);


					$image_name=$imgname;
					$newname=$uploaddir.$image_name;
					
					
					$timeImg=time();
					@copy($imgname, './server/php/rental/mobile/'.$imgname);
					$target_file=$uploaddir.$image_name;
					$imageName=$imgname; 
					$option=$this->getImageShape(500,350,$target_file);
					
					$resizeObj = new Resizeimage($target_file);	
					$resizeObj -> resizeImage(500, 350, $option);
					$resizeObj -> saveImage($uploaddir.'mobile/'.$imageName, 100);
					$this->ImageCompress($uploaddir.'mobile/'.$imageName);
					@copy($uploaddir.'mobile/'.$imageName, $uploaddir.'mobile/'.$imageName);

					$inputArr3 = array(
						'product_id' =>$_POST['P_Id'],
						'product_image'=>$imgname,
						'mproduct_image'=>$imageName
							);
					$this->mobile_model->simple_insert(PRODUCT_PHOTOS,$inputArr3);
					
			}
			if($_FILES['photo4'] != '') {
					$uploaddir = "server/php/rental/";
					$data = file_get_contents($_FILES['photo4']['tmp_name']);
					$image = imagecreatefromstring( $data );
					$imgname=time().".jpg";
					imagejpeg($image,$uploaddir.$imgname, 99);


					$image_name=$imgname;
					$newname=$uploaddir.$image_name;
					
					
					$timeImg=time();
					@copy($imgname, './server/php/rental/mobile/'.$imgname);
					$target_file=$uploaddir.$image_name;
					$imageName=$imgname; 
					$option=$this->getImageShape(500,350,$target_file);
					
					$resizeObj = new Resizeimage($target_file);	
					$resizeObj -> resizeImage(500, 350, $option);
					$resizeObj -> saveImage($uploaddir.'mobile/'.$imageName, 100);
					$this->ImageCompress($uploaddir.'mobile/'.$imageName);
					@copy($uploaddir.'mobile/'.$imageName, $uploaddir.'mobile/'.$imageName);

					$inputArr3 = array(
						'product_id' =>$_POST['P_Id'],
						'product_image'=>$imgname,
						'mproduct_image'=>$imageName
							);
					$this->mobile_model->simple_insert(PRODUCT_PHOTOS,$inputArr3);
					
			}
			if($_FILES['photo5'] != '') {
					$uploaddir = "server/php/rental/";
					$data = file_get_contents($_FILES['photo5']['tmp_name']);
					$image = imagecreatefromstring( $data );
					$imgname=time().".jpg";
					imagejpeg($image,$uploaddir.$imgname, 99);


					$image_name=$imgname;
					$newname=$uploaddir.$image_name;
					
					
					$timeImg=time();
					@copy($imgname, './server/php/rental/mobile/'.$imgname);
					$target_file=$uploaddir.$image_name;
					$imageName=$imgname; 
					$option=$this->getImageShape(500,350,$target_file);
					
					$resizeObj = new Resizeimage($target_file);	
					$resizeObj -> resizeImage(500, 350, $option);
					$resizeObj -> saveImage($uploaddir.'mobile/'.$imageName, 100);
					$this->ImageCompress($uploaddir.'mobile/'.$imageName);
					@copy($uploaddir.'mobile/'.$imageName, $uploaddir.'mobile/'.$imageName);

					$inputArr3 = array(
						'product_id' =>$_POST['P_Id'],
						'product_image'=>$imgname,
						'mproduct_image'=>$imageName
							);
					$this->mobile_model->simple_insert(PRODUCT_PHOTOS,$inputArr3);
					
			}
			if($_POST['P_Id'] != '') {
				if($_POST['P_Title'] !='') {
				
					$data = array(
						'product_title'=>$_POST['P_Title']
							);
					
					$condition = array(
						'id'=>$_POST['P_Id']	
							);
					$this->mobile_model->update_details(PRODUCT ,$data ,$condition);
				}
				if($_POST['P_Price'] !='') {
					$data = array(
						'price'=>$_POST['P_Price']
							);
					
					$condition = array(
						'id'=>$_POST['P_Id']	
							);
					$this->mobile_model->update_details(PRODUCT ,$data ,$condition);
				}
				if($_POST['P_Summary'] !='') {
					$data = array(
						'description'=>$_POST['P_Summary']
							);
					
					$condition = array(
						'id'=>$_POST['P_Id']	
							);
					$this->mobile_model->update_details(PRODUCT ,$data ,$condition);
				}
				if(count($fulladdress) > 3 && $_POST['P_Id'] != '') {
				$data = array(
						'address'=>$fulladdress[4].$fulladdress[3],
						'latitude'=>$log[0],
						'longitude'=>$log[1]
						);
				$condition = array(
						'product_id'=>$_POST['P_Id']	
							);
				$this->mobile_model->update_details(PRODUCT_ADDRESS ,$data ,$condition);
				
				$condition = array('name'=>$fulladdress[0]);
				$this->data['city'] = $this->mobile_model->get_all_details(CITY,$condition);
				$city_id = $this->data['city']->row()->id;
				if($this->data['city']->num_rows() !=0 ) {
					$data = array(
						'city'=>$city_id
						);
					$condition = array(
						'product_id'=>$_POST['P_Id']	
							);
					$this->mobile_model->update_details(PRODUCT_ADDRESS ,$data ,$condition);
					//echo $this->db->last_query();
				}
				$condition1 = array('name'=>$fulladdress[1]);
				$this->data['state'] = $this->mobile_model->get_all_details(STATE_TAX,$condition1);
				$state = $city_id = $this->data['state']->row()->id;
				if($this->data['state']->num_rows() !=0 ) {
					$data = array(
						'state'=>$state
						);
					$condition = array(
						'product_id'=>$_POST['P_Id']	
							);
					$this->mobile_model->update_details(PRODUCT_ADDRESS ,$data ,$condition);
					//echo $this->db->last_query();
				}
				//echo "country".$fulladdress[2];
				$condition2 = array('name'=>$fulladdress[2]);
				$this->data['LOCATIONS'] = $this->mobile_model->get_all_details(COUNTRY_LIST,$condition2);
				$loc = $city_id = $this->data['LOCATIONS']->row()->id;
				if($this->data['LOCATIONS']->num_rows() !=0 ) {
					$data = array(
						'country'=>$loc
						);
					$condition = array(
						'product_id'=>$_POST['P_Id']	
							);
					$this->mobile_model->update_details(PRODUCT_ADDRESS ,$data ,$condition);
					//echo $this->db->last_query();
				}
				
				
			
			}
			
			if(count($fulladdress) == 3 && $_POST['P_Id'] != '') {
				
				$condition = array('name'=>$fulladdress[0]);
				$this->data['city'] = $this->mobile_model->get_all_details(CITY,$condition);
				$city_id = $this->data['city']->row()->id;
				if($this->data['city']->num_rows() !=0 ) {
					$data = array(
						'city'=>$city_id,
						'latitude'=>$log[0],
						'longitude'=>$log[1]
						);
					$condition = array(
						'product_id'=>$_POST['P_Id']	
							);
					$this->mobile_model->update_details(PRODUCT_ADDRESS ,$data ,$condition);
					//echo $this->db->last_query();
				}
				$condition1 = array('name'=>$fulladdress[1]);
				$this->data['state'] = $this->mobile_model->get_all_details(STATE_TAX,$condition1);
				$state = $city_id = $this->data['state']->row()->id;
				if($this->data['state']->num_rows() !=0 ) {
					$data = array(
						'state'=>$state
						);
					$condition = array(
						'product_id'=>$_POST['P_Id']	
							);
					$this->mobile_model->update_details(PRODUCT_ADDRESS ,$data ,$condition);
					//echo $this->db->last_query();
				}
				//echo "country".$fulladdress[2];
				$condition2 = array('name'=>$fulladdress[2]);
				$this->data['LOCATIONS'] = $this->mobile_model->get_all_details(COUNTRY_LIST,$condition2);
				$loc = $city_id = $this->data['LOCATIONS']->row()->id;
				if($this->data['LOCATIONS']->num_rows() !=0 ) {
					$data = array(
						'country'=>$loc
						);
					$condition = array(
						'product_id'=>$_POST['P_Id']	
							);
					$this->mobile_model->update_details(PRODUCT_ADDRESS ,$data ,$condition);
					//echo $this->db->last_query();
				}
				
				
			
			}
			if(count($fulladdress) == 2 && $_POST['P_Id'] != '') {
				
				
				$condition1 = array('name'=>$fulladdress[0]);
				$this->data['state'] = $this->mobile_model->get_all_details(STATE_TAX,$condition1);
				$state = $city_id = $this->data['state']->row()->id;
				if($this->data['state']->num_rows() !=0 ) {
					$data = array(
						'state'=>$state,
						'latitude'=>$log[0],
						'longitude'=>$log[1]
						);
					$condition = array(
						'product_id'=>$_POST['P_Id']	
							);
					$this->mobile_model->update_details(PRODUCT_ADDRESS ,$data ,$condition);
					//echo $this->db->last_query();
				}
				//echo "country".$fulladdress[2];
				$condition2 = array('name'=>$fulladdress[1]);
				$this->data['LOCATIONS'] = $this->mobile_model->get_all_details(COUNTRY_LIST,$condition2);
				$loc = $city_id = $this->data['LOCATIONS']->row()->id;
				if($this->data['LOCATIONS']->num_rows() !=0 ) {
					$data = array(
						'country'=>$loc
						);
					$condition = array(
						'product_id'=>$_POST['P_Id']	
							);
					$this->mobile_model->update_details(PRODUCT_ADDRESS ,$data ,$condition);
					//echo $this->db->last_query();
				}
				
				
			
			}
			
			if(count($fulladdress) == 1 && $_POST['P_Id'] != '') {
				$condition2 = array('name'=>$fulladdress[0]);
				$this->data['LOCATIONS'] = $this->mobile_model->get_all_details(COUNTRY_LIST,$condition2);
				$loc = $city_id = $this->data['LOCATIONS']->row()->id;
				if($this->data['LOCATIONS']->num_rows() !=0 ) {
					$data = array(
						'country'=>$loc
						);
					$condition = array(
						'product_id'=>$_POST['P_Id']	
							);
					$this->mobile_model->update_details(PRODUCT_ADDRESS ,$data ,$condition);
					//echo $this->db->last_query();
				}
				
				
			
			}
				
				
				
				//post value end
			}
			$res = 'successfully added';	
			$json_encode = json_encode(array("status"=>$res));
			}elseif($this->data['checkUser']->num_rows() == 1){
		
					$data = array('room_type'=>$this->input->post('room_type1'),
								 'room_type'=>$list_space,
								 'home_type'=>$property_type,
								 'accommodates'=>$t_guest,
								 'bedrooms'=>$t_bedroom,
								 'beds'=>$t_bed,
								 'bathrooms'=>$bathrooms,
								 'user_id'=>$id,
								 'status'=>'UnPublish',
								 'through'=>'mobile'
							);
							
			$this->mobile_model->simple_insert(PRODUCT,$data);
			//echo $this->db->last_query();die;
			$getInsertId=$this->mobile_model->get_last_insert_id();
			$inputArr3 = array(
						'product_id' =>$getInsertId,
						'address'=>$_POST['p_location']
							 );
			$this->mobile_model->simple_insert(PRODUCT_ADDRESS,$inputArr3);
			$addr_id=$this->mobile_model->get_last_insert_id();
			$location = $_POST['p_location'];
			$this->mobile_model->update_details(USERS,array('group'=>'Seller'),array('id'=>$id));
			$res = 'successfully added';
			$condition1 = array('id'=>$getInsertId);
			$pDetails = $this->mobile_model->get_all_details(PRODUCT,$condition1);
			$currency = $pDetails->row()->currency;
			if($currency = '')$currency = 'USD';
			$json_encode = json_encode(array("status"=>$res,'p_id'=>$getInsertId,'currency'=>$currency,'location'=>$location));
			}elseif($_POST['P_Id'] != '' && $_POST['list_space'] !=''){
			$t_guest= $_POST['t_guest'];
			$t_bed= $_POST['t_bed'];
			$t_bedroom= $_POST['t_bedroom'];
			$property_type= $_POST['property_type'];

			if($_POST['t_bathroom'] == 1){
			$bathrooms = 'Private';
			}elseif($_POST['t_bathroom'] == 2){
			$bathrooms = 'Both';
			}else{
			$bathrooms = 'Shared';
			}

			if($_POST['list_space'] == 1){
			$list_space = 'entire home/apt';
			}elseif($_POST['list_space'] == 2){
			$list_space = 'private room';
			}else{
			$list_space = 'Shared room';
			}
			$data = array(
						'room_type'=>$list_space,
						 'home_type'=>$property_type,
						 'accommodates'=>$t_guest,
						 'bedrooms'=>$t_bedroom,
						 'beds'=>$t_bed,
						 'bathrooms'=>$bathrooms
							);
					
			$condition = array(
				'id'=>$_POST['P_Id']	
					);
			$this->mobile_model->update_details(PRODUCT ,$data ,$condition);
			
			$res = 'successfully added';
			$json_encode = json_encode(array("status"=>$res));
			}elseif($_POST['P_Id'] != '' && $_POST['d_space'] !=''){
			//echo  $_GET['P_Id'];die;
			$data = array(
						'space'=>$_POST['d_space']
							);
					
					$condition = array(
						'id'=>$_POST['P_Id']	
							);
					$this->mobile_model->update_details(PRODUCT ,$data ,$condition);
			$res = 'successfully added';
			$json_encode = json_encode(array("status"=>$res));
			}elseif($_POST['P_Id'] != '' && $_POST['P_Title'] !=''){
			$data = array(
				'product_title'=>$_POST['P_Title']
					);
			
			$condition = array(
				'id'=>$_POST['P_Id']	
					);
			$this->mobile_model->update_details(PRODUCT ,$data ,$condition);
			$res = 'successfully added';
			$json_encode = json_encode(array("status"=>$res));
			}elseif($_POST['P_Id'] != '' && $_POST['P_Summary'] !=''){
			$data = array(
				'description'=>$_POST['P_Summary']
					);
			
			$condition = array(
				'id'=>$_POST['P_Id']	
					);
			$this->mobile_model->update_details(PRODUCT ,$data ,$condition);
			$res = 'successfully added';
			$json_encode = json_encode(array("status"=>$res));
			}elseif($_POST['P_Id'] != '' && $_POST['d_other'] !=''){
			$condition = array('email'=>$_POST['Email'],'status'=>'Active');
			
			$data = array(
						'other_thingnote'=>$_POST['d_other']
							);
					
					$condition = array(
						'id'=>$_POST['P_Id']	
							);
					$this->mobile_model->update_details(PRODUCT ,$data ,$condition);
			$res = 'successfully added';
			$json_encode = json_encode(array("status"=>$res));
			}elseif($_POST['P_Id'] != '' && $_POST['d_house_rules'] !=''){

			$data = array(
						'house_rules'=>$_POST['d_house_rules']
							);
					
					$condition = array(
						'id'=>$_POST['P_Id']	
							);
					$this->mobile_model->update_details(PRODUCT ,$data ,$condition);
			$res = 'successfully added';
			$json_encode = json_encode(array("status"=>$res));
			}elseif($_POST['P_Id'] != '' && $_POST['d_listvalue'] !=''){

			$data = array(
						'list_name'=>$_POST['d_listvalue']
					);
					
			$condition = array(
				'		id'=>$_POST['P_Id']	
					);
			$this->mobile_model->update_details(PRODUCT ,$data ,$condition);
			$res = 'successfully added';
			$json_encode = json_encode(array("status"=>$res));
			}elseif($_POST['P_Id'] != '' && $_POST['l_week'] !=''){

			$data = array(
						'price_perweek'=>$_POST['l_week'],
						'price_permonth'=>$_POST['l_month']
							);
					
					$condition = array(
						'id'=>$_POST['P_Id']	
							);
					$this->mobile_model->update_details(PRODUCT ,$data ,$condition);
			$res = 'successfully added';
			$json_encode = json_encode(array("status"=>$res));
			}elseif($_POST['P_Id'] != '' && $_POST['P_Price'] !=''){
			
					$data = array(
						'price'=>$_POST['P_Price']
							);
					
					$condition = array(
						'id'=>$_POST['P_Id']	
							);
					$this->mobile_model->update_details(PRODUCT ,$data ,$condition);
			$res = 'successfully added';
			$json_encode = json_encode(array("status"=>$res));
			
			}elseif($_POST['P_Id'] != '' && $_POST['P_Currency'] !=''){
			if($_POST['P_Currency'] == '')$c_value = 'USD';
			else $c_value = $_POST['P_Currency'];
			$data = array(
						'currency'=>$c_value
							);
					
					$condition = array(
						'id'=>$_POST['P_Id']	
							);
					$this->mobile_model->update_details(PRODUCT ,$data ,$condition);
			$res = 'successfully added';
			$json_encode = json_encode(array("status"=>$res));
			}else{
			$res = 'failed';
			$json_encode = json_encode(array("status"=>$res));
			}
			echo $json_encode;
	}
	public function mobile_listview(){
	
			$email = $_GET['email_Id'];
			$condition1 = array('email'=>$email);
			$userdetail = $this->mobile_model->get_all_details(USERS,$condition1);
			$id =$userdetail->row()->id;
			$condition = array('id'=>$id);		
			$this->db->select('p.product_title,p.description,p.price,p.id,p.user_status,p.status,p.room_type,pp.product_image,pa.address,pa.country, pa.lat, pa.lang, u.user_name as hostname');
			$this->db->from(PRODUCT.' as p');
			$this->db->join(PRODUCT_PHOTOS.' as pp',"pp.product_id=p.id","LEFT");
			$this->db->join(USERS.' as u',"u.id=p.user_id","LEFT");
			$this->db->join(PRODUCT_ADDRESS_NEW.' as pa',"pa.productId=p.id","LEFT");
			$this->db->where('p.user_id',$id);
			if($_GET['status'] == 'publish')
			$this->db->where('p.status','Publish');
			$this->db->order_by('p.id','desc');
			$this->db->group_by('p.id');
			$query = $this->db->get();
			$search_res1 = $query->result();
			if($query->num_rows() != 0) {
			
			foreach($search_res1 as $res1){ 
			//print_r($res1);
			if($res1->product_image != ''){
			$p_img = explode('.',$res1->product_image);	
			$pro_img = $p_img[0].'.jpg';
			$proImage = $pro_img;
			}else{
				$proImage = 'no_image.jpg';
			}
			if($res1->product_title == '' || $res1->price == '' ) {
			$status = 'Incomplete';
			}else{
			$status = $res1->status;
			}
			if($res1->lat != '' || $res1->lang != '' ) {
			$lat = $res1->lat;
			$lang = $res1->lang;
			}
			$product_title = $res1->product_title;
			$hostname = $res1->hostname;
			$price = $res1->price;
			$user_status = $res1->user_status;
			$city = explode(',',$res1->address);
			$condition1 = array("id"=>$res1->country);
			$view = $this->mobile_model->get_all_details(LOCATIONS,$condition1);
			$countryname = $view->row()->name;
			$condition = array("country_name"=>$countryname);
			$view1 = $this->mobile_model->get_all_details(CURRENCY,$condition);
			if($view1->row()->currency_symbols !=''){
			$currency = $view1->row()->currency_symbols;
			}else{
			$currency = '$';
			}
			$productarr[] = array('image'=>$proImage,'product_title'=>$product_title,'price'=>$price,'status'=>$status,'hostname'=>$hostname,'user_status'=>$user_status,'room_type'=>$res1->room_type,'p_id'=>$res1->id,'city'=>$city[0],'lat'=>$lat,'long'=>$lang,'currency'=>$currency);
			}
			}else{
			$productarr = array();
			}
			
			$json_encode = json_encode(array("product"=>$productarr));
			echo $json_encode;

	}
	
	public function product_edit() { 
			$p_id = $_GET['p_id'];
			$this->db->select('p.*,pp.product_image,pa.address,pa.country, pa.lat, pa.lang');
			$this->db->from(PRODUCT.' as p');
			$this->db->join(PRODUCT_PHOTOS.' as pp',"pp.product_id=p.id","LEFT");
			$this->db->join(PRODUCT_ADDRESS_NEW.' as pa',"pa.productId=p.id","LEFT");
			$this->db->where('p.id',$p_id);
			$this->db->group_by('p.id');
			$query = $this->db->get();
			$product_res = $query->result();
			// echo $product_res->price;die;
			// print_r($product_res);die;
			$this->db->select('id,product_image');
			$this->db->from(PRODUCT_PHOTOS);
			$this->db->where('product_id',$p_id);
			$query = $this->db->get();
			$product_photos = $query->result();
			if(!$product_photos)$prodimgArr = array();
			else{
				foreach($product_photos as $value) {
					if($value->product_image != ''){
						$p_img = explode('.',$value->product_image);	
						$pro_img = $p_img[0].'.jpg';
						$prodimgArr[] = array('productImage' => $pro_img,'imageId' => $value->id);
					}else{
						$prodimgArr[] = array('productImage' => 'no_image.jpg','imageId' => 0);
					}
				}
			}
					
			foreach($product_res as $value) {
				
			if($value->product_title == '') {
			$title='';
			}else{
			$title = $value->product_title;
			}
			if($value->minimum_stay == '') {
			$minimum_stay='';
			}else{
			$minimum_stay = $value->minimum_stay;
			}
			if($value->description == '') {
			$description='';
			}else{
			$description = strip_tags($value->description);
			}
			if($value->space == '') {
			$space='';
			}else{
			$space = strip_tags($value->space);
			}
			if($value->other_thingnote == '') {
			$other_thingnote='';
			}else{
			$other_thingnote = strip_tags($value->other_thingnote);
			}
			if($value->house_rules == '') {
			$house_rules='';
			}else{
			$house_rules = strip_tags($value->house_rules);
			}
			if($value->cancellation_policy == '') {
			$cancellation_policy='';
			}else{
			$cancellation_policy = strip_tags($value->cancellation_policy);
			}
			if($value->currency == '') {
			$currency='';
			}else{
			$currency = $value->currency;
			}
			if($value->price == '' || $value->price == '0.00') {
			$price='';
			}else{
			$price = $value->price;
			}
			if($value->address == '') {
			$address='';
			}else{
			$address = $value->address;
			}
			if($value->price_perweek == '' || $value->price_perweek == '0.00') {
			$perweek='';
			}else{
			$perweek = $value->price_perweek;
			}
			if($value->price_permonth == '' || $value->price_permonth == '0.00') {
			$permonth='';
			}else{
			$permonth = $value->price_permonth;
			}
			if($value->currency == '') {
			$currency='USD';
			}else{
			$currency = $value->currency;
			}
			if($value->list_name == '') {
			$list_name='';
			}else{
			$list_name = $value->list_name;
			}
			if($value->beds == '') {
			$bed='';
			}else{
			$bed = $value->beds;
			}
			if($value->bedrooms == '') {
			$bedrooms='';
			}else{
			$bedrooms = $value->bedrooms;
			}
			if($value->bathrooms == '') {
			$bathrooms='';
			}else{
			$bathrooms = $value->bathrooms;
			}
			if($value->accommodates == '') {
			$accommodates='';
			}else{
			$accommodates = $value->accommodates;
			}
			if($value->room_type == '') {
			$room_type='';
			}else{
			$room_type = $value->room_type;
			}
			if($value->lat == '') {
			$lat='';
			}else{
			$lat = $value->lat;
			}
			if($value->lang == '') {
			$lang='';
			}else{
			$lang = $value->lang;
			}
			$productarr[] = array('image'=>$prodimgArr,'title'=>$title,'space'=>$space,'description'=>$description,'other_thingnote'=>$other_thingnote,'house_rules'=>$house_rules,'cancellation_policy'=>$cancellation_policy,'currency'=>$currency,'price'=>$price,'address'=>$address,'perweek'=>$perweek,'permonth'=>$permonth,'currency'=>$currency,'amt_list'=>$list_name,'bed'=>$bed,'bedroom'=>$bedrooms,'bathroom'=>$bathrooms,'guest'=>$accommodates,'list_type'=>$room_type,'home_type'=>$value->home_type,'lat'=>$lat,'long'=>$lang,'status'=>$value->status,'minimum_stay'=>$minimum_stay);
			}
			$json_encode = json_encode(array("product"=>$productarr));
			echo $json_encode;
	
	}
	
	
	/** 
	 * 
	 * Loading Category Json Page
	 */
	 
	 //wish list 
	 
	public function mobile_wishlist() {
	
		$wishcatname = $_POST['title'];
		$product_id =$_POST['product_Id'];
		$email = $_POST['email_Id'];
		
		
		$condition1 = array("email"=>$email);
		$view = $this->mobile_model->get_all_details(USERS,$condition1);
		$euser_id = $view->row()->id;

		$wishuser_id = $euser_id;
		$condition = array('user_id'=>$wishuser_id,'product_id'=>$product_id);
		$check = $this->mobile_model->get_all_details(LISTS_DETAILS,$condition);
		
		if($check->num_rows() == 0) {
			$data = $this->mobile_model->add_wishlist_category ( array (
					'user_id' => $wishuser_id,
					'name' => ucfirst ( $wishcatname ),
					'product_id'=>$product_id

			) );
		$newdata = array('fav'=>1);
		$condition = array('id'=>$product_id);
		$this->mobile_model->update_details ( PRODUCT, $newdata, $condition );
		$res = 'successfully wishlist added';
		$json_encode = json_encode(array("status"=>$res));
		}else{
		$this->db->where('user_id', $wishuser_id);
		$this->db->where('product_id', $product_id);
		$this->db->delete(LISTS_DETAILS);
		$newdata = array('fav'=>0);
		$condition = array('id'=>$product_id);
		$this->mobile_model->update_details ( PRODUCT, $newdata, $condition );		
		$res = 'successfully wishlist removed';
		$json_encode = json_encode(array("status"=>$res));
		}
		echo $json_encode;		
	}
	public function mobile_setcurrency(){
		$currencytype = $_GET['currency_code'];
		$email = $_GET['email'];
		if($_GET['currency_code'] != '' && $_GET['email'] != '') {
		$condition1 = array("email"=>$email);
		$view = $this->mobile_model->get_all_details(USERS,$condition1);
		$euser_id = $view->row()->id;
		
		$newdata = array('user_currency'=>$currencytype);
		$condition = array('id'=>$euser_id);
		$this->mobile_model->update_details ( USERS, $newdata, $condition );
		$res = 'successful';
		$json_encode = json_encode(array("status"=>$res));
		}else{
		$res = 'failed';
		$json_encode = json_encode(array("status"=>$res));
		}
		echo $json_encode;
	}
	public function mobile_wishlistview() {
	
		$email = $_GET['email_Id'];

		$condition1 = array("email"=>$email);
		$view = $this->mobile_model->get_all_details(USERS,$condition1);
		$euser_id = $view->row()->id; 
		$this->db->select('p.product_id,p.name,pp.product_image,pa.city, pa.state');
		$this->db->from(LISTS_DETAILS.' as p');
		$this->db->join(PRODUCT_PHOTOS.' as pp',"pp.product_id=p.product_id","LEFT");
		$this->db->join(PRODUCT_ADDRESS_NEW.' as pa',"pa.productId=p.product_id","LEFT");
		$this->db->where('p.user_id',$euser_id);
		$this->db->order_by('p.id',desc);
		$this->db->group_by('pp.product_id');
		$query = $this->db->get();	
		if($query->num_rows() != 0){
			foreach($query->result() as $view){
				if($view->product_image != ''){
			$p_img = explode('.',$view->product_image);	
			$pro_img = $p_img[0].'.jpg';
			$proImage = $pro_img;
			}else{
				$proImage = 'no_image.jpg';
			}
			$city = str_replace("Country", "", $view->city.$view->state);
			$res[]=array("title"=>$view->name,"image"=>$proImage,'product_Id'=>$view->product_id,'city'=>$city);
			}	
		}
		if($query->num_rows() == 0){
			$res =array();
		}
		$json_encode = json_encode(array("status"=>$res));
		echo $json_encode;
	
	}	
	
	public function mobile_yourtrips() {
		$email = $_GET['email'];
		$condition = array("email"=>$email);
		$view = $this->mobile_model->get_all_details(USERS,$condition);
		$user_id = $view->row()->id;
		//$user_id= 75;
		if($_GET['email'] != ''){
		$this->db->select('pb.*,pa.post_code,pa.address,pa.apt,pp.product_image,
								c.name as country_name,
								s.name as state_name,
								ci.name as city_name,
								p.product_name,p.product_title,p.price,p.currency,
								u.firstname,u.image,
								rq.booking_status,rq.checkin,rq.checkout,rq.dateAdded,rq.user_id as GestId,rq.renter_id,rq.serviceFee,rq.totalAmt,rq.approval as approval,rq.id as cid');
			$this->db->from(PRODUCT_BOOKING.' as pb');
			$this->db->join(PRODUCT_ADDRESS.' as pa' , 'pa.product_id = pb.product_id','left');
			$this->db->join(LOCATIONS.' as c' , 'c.id = pa.country','left');
			$this->db->join(STATE_TAX.' as s' , 's.id = pa.state','left');
			$this->db->join(CITY.' as ci' , 'ci.id = pa.city','left');
			$this->db->join(PRODUCT.' as p' , 'p.id = pb.product_id','left');
			$this->db->join(PRODUCT_PHOTOS.' as pp' , 'p.id = pp.product_id','left');
			$this->db->join(RENTALENQUIRY.' as rq' , 'p.id = rq.prd_id');
			
			$this->db->join(USERS.' as u' , 'u.id = rq.renter_id');
			$this->db->where('rq.user_id = '.$user_id);
			$this->db->where('DATE(rq.checkout) > ', date('"Y-m-d H:i:s"'), FALSE);
			$this->db->where('rq.booking_status != "Enquiry"');
			$this->db->group_by('rq.id');
			$this->db->order_by('rq.dateAdded');
			$tripresult = $this->db->get();
			foreach($tripresult->result() as $trip){
			if($trip->product_image !=''){
			$p_img = explode('.',$trip->product_image);	
			$pro_img = $p_img[0].'.jpg';
			$proImage = $pro_img;
			}else{
				$proImage = 'no_image.jpg';
			}
				$res[] =array("product_title"=>$trip->product_title,"image"=>'no_image.jpg');
			}
			if($tripresult->num_rows()==0) {
				$res=array();
				$json_encode = json_encode(array("status"=>$res));
			}else{
				$json_encode = json_encode(array("status"=>$res));
			}
			}else{
				$res=array();
				$json_encode = json_encode(array("status"=>$res));
			}
			echo $json_encode;		
	}
	public function mobile_psdchange(){
		
		$email = $_POST['email'];
		$current_pass = md5 ( $_POST['currentpsd'] );
		$newpsd = $_POST['newpsd'];
		$condition = array (
					'email' => $email,
					'password' => $current_pass 
			);
			$checkuser = $this->mobile_model->get_all_details ( USERS, $condition );
			if ($checkuser->num_rows () == 1) {
				$newPass = md5 ( $newpsd );
				$newdata = array (
						'password' => $newPass 
				);
				$condition1 = array (
						'email' => $email 
				);
				$this->mobile_model->update_details ( USERS, $newdata, $condition1 );
				 $res = 'Password changed successfully';
			} else {
				$res = 'Current password is wrong';
			}
			$json_encode = json_encode(array("status"=>$res));
			echo $json_encode;
			
	}
	public function mobile_hometype() {
		$condition = array('listspace_id'=>9,'other'=>'Yes');
		$listvalue = $this->mobile_model->get_all_details (LISTSPACE_VALUES, $condition );
		foreach($listvalue->result() as $list){
		$propertytype[] = array('properytype'=>$list->list_value);
		}
		$json_encode = json_encode(array('type'=>$propertytype));
		echo $json_encode;
	}
	public function mobile_listspacetype() {
		$condition = array('listspace_id'=>10,'other'=>'no');
		$listvalue = $this->mobile_model->get_all_details (LISTSPACE_VALUES, $condition );
		foreach($listvalue->result() as $list){
		$propertytype[] = array('properytype'=>$list->list_value);
		}
		$json_encode = json_encode(array('type'=>$propertytype));
		echo $json_encode;
	}
	
	public function mobile_updateprofile() {
		//echo '<pre>';print_r($_POST);die;
		$email = $_GET['Email'];
		$condition = array("email"=>$email);
		$view = $this->mobile_model->get_all_details(USERS,$condition);
		$user_id = $view->row()->id;

		if ($view->num_rows() == 1) {
		
		if($_POST['u_email'] != '') {
		
		$email1 = $_POST['u_email'];
		$condition = array("email"=>$email1);
		$view = $this->mobile_model->get_all_details(USERS,$condition);
			if($view->num_rows() == 0 ) {
			
			$email1 = $_POST['u_email'];
			$newdata = array ('email' => $email1);
			$condition1 = array ('id' => $user_id );
			$this->mobile_model->update_details ( USERS, $newdata, $condition1 );
			
			}else{
			$status = 'email already exits';
			$json_encode = json_encode(array("profileinfo"=>$detail,"status"=>$status));
			
			}

		}

		if($_POST['u_first_name'] != '') {
			
			$firstname = $_POST['u_first_name'];
			$newdata = array ('firstname' => $firstname,'user_name'=>$firstname);
			$condition1 = array ('id' => $user_id );
			$this->mobile_model->update_details ( USERS, $newdata, $condition1 );
		}
		
		if($_POST['u_last_name'] != '') {
			
			$lastname = $_POST['u_last_name'];
			$newdata = array ('lastname' => $lastname);
			$condition1 = array ('id' => $user_id );
			$this->mobile_model->update_details ( USERS, $newdata, $condition1 );
		}
		
		if($_POST['u_gender'] != '') {
		
			$gender = $_POST['u_gender'];
			$newdata = array ('gender' => $gender);
			$condition1 = array ('id' => $user_id );
			$this->mobile_model->update_details ( USERS, $newdata, $condition1 );
			// echo $this->db->last_query();die;
		}

		if($_POST['u_birth'] != '') {
			$dob = explode("/",$_POST['u_birth']);
			
			$newdata = array ('dob_date' => $dob[0],"dob_month"=>$dob[1],"dob_year"=>$dob[2]);
			$condition1 = array ('id' => $user_id );
			$this->mobile_model->update_details ( USERS, $newdata, $condition1 );
		//	echo $this->db->last_query();die;
		}
		if($_POST['u_phone'] != '') {
			
			$phone_no = $_POST['u_phone'];
			$newdata = array ('phone_no' => $phone_no);
			$condition1 = array ('id' => $user_id );
			$this->mobile_model->update_details ( USERS, $newdata, $condition1 );
		}
		if($_FILES['photo1'] != '') {
		
					$uploaddir = "images/users/";
					$data = file_get_contents($_FILES['photo1']['tmp_name']);
					$image = imagecreatefromstring( $data );
					$imgname=time().".jpg";
					imagejpeg($image,$uploaddir.$imgname, 99);


					

			$newdata = array ('image' => $imgname);
			$condition1 = array ('id' => $user_id );
			$this->mobile_model->update_details ( USERS, $newdata, $condition1 );
		}
		
		if($_POST['live'] != '') {
			$live = $_POST['live'];
			$newdata = array ('address' => $live);
			$condition1 = array ('id' => $user_id );
			$this->mobile_model->update_details ( USERS, $newdata, $condition1 );
		}

		if($_POST['u_about'] != '') {
		
			$describe = $_POST['u_about'];
			$newdata = array ('description' => $describe);
			$condition1 = array ('id' => $user_id );
			$this->mobile_model->update_details ( USERS, $newdata, $condition1 );
		}
		
		if($_POST['school'] != '') {
			$school = $_POST['school'];
			$newdata = array ('school' => $school);
			$condition1 = array ('id' => $user_id );
			$this->mobile_model->update_details ( USERS, $newdata, $condition1 );
		}

		if($_POST['work'] != '') {
			$work = $_POST['work'];
			$newdata = array ('work' => $work);
			$condition1 = array ('id' => $user_id );
			$this->mobile_model->update_details ( USERS, $newdata, $condition1 );
		} 

		if($_POST['language'] != '') {
			$language = $_POST['language'];
			$newdata = array ('languages_known' => $language);
			$condition1 = array ('id' => $user_id );
			$this->mobile_model->update_details ( USERS, $newdata, $condition1 );

		}
		
		
		$email = $_GET['Email'];
		$condition = array("email"=>$email);
		$view = $this->mobile_model->get_all_details(USERS,$condition);
		//echo '<pre>';print_r($view->row());die;
		if($view->row()->image != '') {
		$img = $view->row()->image;
		}else{
		$img = 'profile.jpg';
		}
		if($view->row()->gender != 'Unspecified') {
		$gender = $view->row()->gender;
		}else{
		$gender = '';
		}
		if($view->row()->dob_date != 0) {
		$date = $view->row()->dob_date."-";
		}else{
		$date = '';
		}
		if($view->row()->dob_month != 0) {
		$month = $view->row()->dob_month."-";
		}else{
		$month = '';
		}
		if($view->row()->dob_year != 0) {
		$year = $view->row()->dob_year;
		}else{
		$year = '';
		}
		$detail[] = array("firstname"=>$view->row()->firstname,"lastname"=>$view->row()->lastname,"email"=>$view->row()->email,"phone"=>$view->row()->phone_no,"gender"=>$gender,"dob"=>$date.$month.$year,"live"=>$view->row()->address,"describe"=>$view->row()->description,"school"=>$view->row()->school,"work"=>$view->row()->work,"language"=>$view->row()->languages_known,"image"=>$img,"status"=>'Successfully Updated');
		
		$json_encode = json_encode(array("profileinfo"=>$detail,"status"=>'Successfully Updated'));
		}else{
		$json_encode = json_encode(array("status"=>'Failed'));
		}
		echo $json_encode;
	
	}
	public function mobile_chat_new() {
		$user_email = $_POST['Email'];
		$rental_id = $_POST['p_id'];
		$message = $_POST['h_msg'];
		$receiverId = $_POST['h_id'];
		$condition = array('email'=>$user_email);
		$senderDetails = $this->mobile_model->get_all_details ( USERS, $condition );
		$senderId = $senderDetails->row()->id;
		$condition = array('id'=>$receiverId);
		$receiverDetails = $this->mobile_model->get_all_details ( USERS, $condition );
		
	}
	public function mobile_chat() {
		$user_email = $_POST['Email'];
		$rental_id = $_POST['p_id'];
		$message = $_POST['h_msg'];
		$host_id = $_POST['h_id'];
		
		$condition = array('email'=>$user_email);
		$useremail = $this->mobile_model->get_all_details ( USERS, $condition );
		$user_id = $useremail->row()->id;
		
		$condition4 = array('id'=>$rental_id);
		$productdetail = $this->mobile_model->get_all_details ( PRODUCT, $condition4);
		$pro_user_id = $useremail->row()->user_id;
		
		$condition2 = array('rental_id'=>$rental_id,'sender_id'=>$user_id);
		$sender = $this->mobile_model->get_all_details ( DISCUSSION, $condition2 );
		$condition3 = array('rental_id'=>$rental_id,'receiver_id'=>$user_id,'sender_id'=>$host_id);
		$receiver = $this->mobile_model->get_all_details (DISCUSSION, $condition3 );
		if($sender->num_rows() > 0) {
		$condition1 = array('id'=>$host_id);
		$key = $this->mobile_model->get_all_details ( USERS, $condition1 );
		$mobile_key = $key->row()->mobile_key;
		$ios_key = $key->row()->ios_key;
		$conid = $sender->row()->convId;
		$data = array(
					'rental_id'=>$rental_id,
					'sender_id'=>$user_id,
					'receiver_id'=>$host_id,
					'message'=>$message,
					'convId'=>$conid,
					'posted_by'=>$_POST['h_type']
					
					);
		$this->mobile_model->simple_insert(DISCUSSION,$data);

		}elseif($receiver->num_rows() > 0){
		$condition1 = array('id'=>$host_id);
		$key = $this->mobile_model->get_all_details ( USERS, $condition1 );
		$mobile_key = $key->row()->mobile_key;
		$ios_key = $key->row()->ios_key;
		$conid = $receiver->row()->convId;
		$data = array(
					'rental_id'=>$rental_id,
					'sender_id'=>$user_id,
					'receiver_id'=>$host_id,
					'message'=>$message,
					'convId'=>$conid,
					'posted_by'=>$_POST['h_type']
					);
		$this->mobile_model->simple_insert(DISCUSSION,$data);
		
		//echo "test";die;
		}else{
		
		$condition1 = array('id'=>$host_id);
		$key = $this->mobile_model->get_all_details ( USERS, $condition1 );
		$mobile_key = $key->row()->mobile_key;
		$ios_key = $key->row()->ios_key;
		$conid = time();
		$data = array(
					'rental_id'=>$rental_id,
					'sender_id'=>$user_id,
					'receiver_id'=>$host_id,
					'message'=>$message,
					'convId'=>$conid,
					'posted_by'=>$_POST['h_type']
				);
		$this->mobile_model->simple_insert(DISCUSSION,$data);
		
		}
		
		$condition1 = array('id'=>$user_id);
		$key = $this->mobile_model->get_all_details ( USERS, $condition1 );
		if($key->row()->image !=''){
			$userImage = $key->row()->image;
		}else{
			$userImage  = 'profile.jpg';
		}
			
		$userName = $key->row()->user_name;
		$pushStatus = "";

		if($mobile_key != ''){
			if(!empty($_GET["push"])) {	
				$gcmRegID  = $mobile_key;
				$pushMessage = $message;	
				if (isset($gcmRegID) && isset($pushMessage)) {		
					$gcmRegIds = array($gcmRegID);
					$message = array("m" => $pushMessage,"k"=>'msg',"convId"=>$conid,"convName"=>$userName,"convImage"=>$userImage,"convRentalId"=>$rental_id,"convIHostId"=>$host_id);	
					$pushStatus = $this->sendPushNotificationToGCM($gcmRegIds, $message);
				}
			}
		}
		if($ios_key != ''){
			$message = array('message'=>$message);
			$this->push_notification($ios_key,$message);
		}
		$json_encode = json_encode(array('status'=>'successful'));	
		echo $json_encode;
	}
	
	/* Mobile chat start function  */
	
	public function mobile_chatnew(){
		$user_email = $_POST['Email'];
		$rental_id = $_POST['p_id'];
		$message = $_POST['h_msg'];
		$host_id = $_POST['h_id'];
		$Bookingno = $_POST['bookingno'];
		
		$bookingDetails = $this->user_model->get_all_details(RENTALENQUIRY,array('Bookingno'=>$Bookingno));
		
		
		
		$dataArr = array('productId' => $bookingDetails->row()->prd_id, 'bookingNo' => $bookingDetails->row()->Bookingno, 'senderId' => $bookingDetails->row()->user_id, 'receiverId' =>$host_id, 'subject' => 'Booking Request : '.$bookingDetails->row()->Bookingno, 'message' => $message);
		
		$this->user_model->simple_insert(MED_MESSAGE, $dataArr);
		$user_id = $bookingDetails->row()->user_id;
		$condition1 = array('id'=>$user_id);
		$key = $this->mobile_model->get_all_details ( USERS, $condition1 );
		if($key->row()->image !=''){
			$userImage = $key->row()->image;
		}else{
			$userImage  = 'profile.jpg';
		}
		$mobile_key = $key->row()->mobile_key;
		$ios_key = $key->row()->ios_key;
		$userName = $key->row()->user_name;
		$pushStatus = "";

		if($mobile_key != ''){
			if(!empty($_GET["push"])) {	
				$gcmRegID  = $mobile_key;
				$pushMessage = $message;	
				if (isset($gcmRegID) && isset($pushMessage)) {		
					$gcmRegIds = array($gcmRegID);
					$message = array("m" => $pushMessage,"k"=>'msg',"convId"=>$Bookingno,"convName"=>$userName,"convImage"=>$userImage,"convRentalId"=>$rental_id,"convIHostId"=>$host_id);	
					$pushStatus = $this->sendPushNotificationToGCM($gcmRegIds, $message);
				}
			}
		}
		if($ios_key != ''){
			$message = array('message'=>$message);
			$this->push_notification($ios_key,$message);
		}
		$jsonReturn['status'] = 'successful';
		echo json_encode(array('jsonReturn'=>$jsonReturn));
		
	
	}
	
	public function mobile_chat_insert(){
		$user_email = $_POST['Email'];
		$message = $_POST['h_msg'];
		$Bookingno = $_POST['bookingno'];
		
		$bookingDetails = $this->user_model->get_all_details(MED_MESSAGE,array('bookingNo'=>$Bookingno));
		
		$userDetails = $this->user_model->get_all_details(USERS,array('email'=>$user_email));
		if($userDetails->row()->id == $bookingDetails->row()->senderId){
		$receiverId = $bookingDetails->row()->receiverId; 
		}else{
		$receiverId = $bookingDetails->row()->senderId;
		}
		$dataArr = array('productId' => $bookingDetails->row()->productId, 'bookingNo' => $bookingDetails->row()->bookingNo, 'senderId' => $userDetails->row()->id, 'receiverId' =>$receiverId, 'subject' => 'Booking Request : '.$bookingDetails->row()->Bookingno, 'message' => $message);
		
		$this->user_model->simple_insert(MED_MESSAGE, $dataArr);
		$user_id = $userDetails->row()->id;
		$condition1 = array('id'=>$user_id);
		$key = $this->mobile_model->get_all_details ( USERS, $condition1 );
		if($key->row()->image !=''){
			$userImage = $key->row()->image;
		}else{
			$userImage  = 'profile.jpg';
		}
		$mobile_key = $key->row()->mobile_key;
		$ios_key = $key->row()->ios_key;
		$userName = $key->row()->user_name;
		$pushStatus = "";

		if($mobile_key != ''){
			if(!empty($_GET["push"])) {	
				$gcmRegID  = $mobile_key;
				$pushMessage = $message;	
				if (isset($gcmRegID) && isset($pushMessage)) {		
					$gcmRegIds = array($gcmRegID);
					$message = array("m" => $pushMessage,"k"=>'msg',"convId"=>$Bookingno,"convName"=>$userName,"convImage"=>$userImage,"convRentalId"=>$rental_id,"convIHostId"=>$host_id);	
					$pushStatus = $this->sendPushNotificationToGCM($gcmRegIds, $message);
				}
			}
		}
		if($ios_key != ''){
			$message = array('message'=>$message);
			$this->push_notification($ios_key,$message);
		}
		$json_encode = json_encode(array('status'=>'successful'));	
		echo $json_encode;
		
	
	}
	
	
	
	
	/* Mobile chat end function */
	
	function sendPushNotificationToGCM($registatoin_ids, $message) {
		//Google cloud messaging GCM-API url
        $url = 'https://android.googleapis.com/gcm/send';
        $fields = array(
            'registration_ids' => $registatoin_ids,
            'data' => $message,
        );
		// Google Cloud Messaging GCM API Key
		define("GOOGLE_API_KEY", "AIzaSyD0VJs5nLcm0j34eHCIpP7I8iNI-yRycqo"); 		
        $headers = array(
            'Authorization: key=' . GOOGLE_API_KEY,
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);	
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);				
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }
	
	public function mobile_viewchat() {
		
		$user_email = $_GET['email'];
		$condition = array('email'=>$user_email);
		$useremail = $this->mobile_model->get_all_details ( USERS, $condition );
		$user_id = $useremail->row()->id;
		
		$this->db->select('d.message,d.receiver_id,d.sender_id,d.rental_id,d.convId,d.date_created,u.user_name,u.image,p.user_id');
		$this->db->from(DISCUSSION.' as d');
		$this->db->join(USERS.' as u' , 'u.id = d.receiver_id');
		$this->db->join(PRODUCT.' as p' , 'p.id = d.rental_id','LEFT');
		$this->db->where('d.sender_id',$user_id);
		$this->db->or_where('d.receiver_id',$user_id);
		$this->db->group_by('d.convId');
		$chat = $this->db->get();
		if($chat->num_rows() != 0) {
		foreach($chat->result() as $list)
		{
			$newChat[] = $list;
		}
		foreach($newChat as $key => $list)
		{
			$newMessages = $this->mobile_model->get_all_details(DISCUSSION, array('convId'=>$list->convId),array(array('field'=>'id','type'=>'desc')));
			$newChat[$key]->newMsg = $newMessages->row()->message;
			$newChat[$key]->newDate = $newMessages->row()->date_created;
		}
		foreach($newChat as $chatval) {
		if($chatval->image !=''){
		$img = $chatval->image;
		}else{
		$img  = 'profile.jpg';
		}
		if($user_id == $chatval->sender_id){
		$hot = $chatval->receiver_id;
		}else{
		$hot = $chatval->sender_id;
		}
		$CatArr[] = array("msg" =>$chatval->newMsg,"newMsg" =>$chatval->newMsg,"username"=>$chatval->user_name,"date"=>$chatval->newDate,"newDate"=>$chatval->newDate,"image"=>$img,'convId'=>$chatval->convId,'rental_id'=>$chatval->rental_id,'host_id'=>$hot);
		}
		}else{
		$CatArr = array();
		}
		
		$json_encode = json_encode(array("sender" => $CatArr));
		echo $json_encode;

	}
	/* mobile chat view new */
	
	public function mobile_viewchat_new() {
		
		$user_email = $_GET['email'];
		$condition = array('email'=>$user_email);
		$useremails = $this->mobile_model->get_all_details ( USERS, $condition );
		$user_id = $useremails->row()->id;
		
		$this->db->select('d.message,d.receiverId,d.senderId,d.productId,d.bookingNo,d.dateAdded,u.user_name,u.image,p.user_id');
		$this->db->from(MED_MESSAGE.' as d');
		$this->db->join(USERS.' as u' , 'u.id = d.senderId');
		$this->db->join(PRODUCT.' as p' , 'p.id = d.productId','LEFT');
		//$this->db->where('d.senderId',$user_id);
		$this->db->where_in('d.receiverId',$user_id);
		$this->db->group_by('d.bookingNo');
		$chat = $this->db->get();
		//print_r($chat->result());die;
		if($chat->num_rows() != 0) {
		foreach($chat->result() as $list)
		{
			$newChat[] = $list;
		}
		foreach($newChat as $key => $list)
		{
			$newMessages = $this->mobile_model->get_all_details(MED_MESSAGE, array('bookingNo'=>$list->bookingNo),array(array('field'=>'id','type'=>'desc')));
			if($newMessages->row()->senderId == $useremails->row()->id){
			$useremail = $this->mobile_model->get_all_details ( USERS, array('id'=>$newMessages->row()->senderId));
			
			$newChat[$key]->newMsg = $newMessages->row()->message;
			$newChat[$key]->images = $useremail->row()->image;
			$newChat[$key]->username = $useremail->row()->user_name;
			$newChat[$key]->newDate = $newMessages->row()->dateAdded;
			}else{
			$useremail = $this->mobile_model->get_all_details ( USERS, array('id'=>$newMessages->row()->senderId) );
			$newChat[$key]->images = $useremail->row()->image;
			$newChat[$key]->username = $useremail->row()->user_name;
			$newChat[$key]->newMsg = $newMessages->row()->message;
			$newChat[$key]->newDate = $newMessages->row()->dateAdded;
			}
		}
		foreach($newChat as $chatval) {
		if($chatval->images !=''){
		$img = $chatval->images;
		}else{
		$img  = 'profile.jpg';
		}
		if($user_id == $chatval->senderId){
		$hot = $chatval->receiverId;
		}else{
		$hot = $chatval->senderId;
		}
		$CatArr[] = array("msg" =>$chatval->newMsg,"newMsg" =>$chatval->newMsg,"username"=>$chatval->username,"date"=>$chatval->newDate,"newDate"=>$chatval->newDate,"image"=>$img,'convId'=>$chatval->bookingNo,'rental_id'=>$chatval->productId,'host_id'=>$hot);
		}
		}else{
		$CatArr = array();
		}
		
		$json_encode = json_encode(array("sender" => $CatArr));
		echo $json_encode;

	}
	
	/* mobile chat view new */
	
	
	
	
	public function mobile_detailchat() {
		
		$convId = $_GET['convId'];
		$condition = array('email'=>$_GET['email']);
		$useremail = $this->mobile_model->get_all_details ( USERS, $condition );
		$user_id = $useremail->row()->id;
		
		$this->db->select('d.message,d.sender_id,d.receiver_id,d.convId,d.posted_by,d.date_created');
		$this->db->from(DISCUSSION.' as d');
		$this->db->where('convId',$convId);
		$chat = $this->db->get();
		
		if($chat->num_rows() != 0) {
		foreach($chat->result() as $chatval) {
		if($chatval->image !=''){
		$img = $chatval->image;
		}else{
		$img  = 'profile.jpg';
		}
		$sender_id = $chatval->sender_id;
		$receiver_id = $chatval->receiver_id;
		$msgby='';
		if($user_id == $sender_id){
		//$msgby='currentuser';
		//echo $sender_id;
		$detail = $this->mobile_model->get_all_details ( USERS, array ('id' => $sender_id));
		//echo $detail->row()->email;
		}else{
		//$msgby='receiver';
		//echo $receiver_id;
		$detail = $this->mobile_model->get_all_details ( USERS, array ('id' => $sender_id ));
		//echo $detail->row()->email;die;
		}
		
		 
		$CatArr[] = array("msg" =>$chatval->message,"username"=>$detail->row()->user_name,"date"=>$chatval->date_created,"image"=>$img,'convId'=>$chatval->convId,'post_by'=>$chatval->posted_by,'email'=>$detail->row()->email);
		}
		}else{
		$CatArr = array();
		}
		$json_encode = json_encode(array("sender" => $CatArr));
		echo $json_encode;
	
	    
	}
	
	
	
	/* mobile detail chat list */
	public function mobile_detailchat_new() {
		
		$convId = $_GET['convId'];
		$condition = array('email'=>$_GET['email']);
		$useremail = $this->mobile_model->get_all_details ( USERS, $condition );
		$user_id = $useremail->row()->id;
		
		$this->db->select('d.message,d.senderId,d.receiverId,d.bookingNo,d.dateAdded');
		$this->db->from(MED_MESSAGE.' as d');
		$this->db->where('bookingNo',$convId);
		$chat = $this->db->get();
		
		if($chat->num_rows() != 0) {
		foreach($chat->result() as $chatval) {
		 if($chatval->image !=''){
		$img = $chatval->image;
		}else{
		$img  = 'profile.jpg';
		}
		$sender_id = $chatval->senderId;
		$receiver_id = $chatval->receiverId;
		$msgby='';
		if($user_id == $sender_id){
		$detail = $this->mobile_model->get_all_details ( USERS, array ('id' => $sender_id));
		$chat_member_details = "user";
		if($detail->row()->image !=''){
		$img = $detail->row()->image;
		}else{
		$img  = 'profile.jpg';
		}

		}else{
		$detail = $this->mobile_model->get_all_details ( USERS, array ('id' => $sender_id ));
		$chat_member_details = "host";
		
		if($detail->row()->image !=''){
		$img = $detail->row()->image;
		}else{
		$img  = 'profile.jpg';
		}
		
		} 
		//print_r($detail->result());
		$CatArr[] = array("msg" =>$chatval->message,"username"=>$detail->row()->user_name,"date"=>$chatval->dateAdded,"image"=>$img,'convId'=>$chatval->bookingNo,'post_by'=>'host','email'=>$detail->row()->email);
		}
		}else{
		$CatArr = array();
		}
		$json_encode = json_encode(array("sender" => $CatArr));
		echo $json_encode;
	
	    
	}

	/* mobile detail chat list */

	
	
	public function category(){
		$this->db->select('id,cat_name,image,rootID');
		$this->db->from(CATEGORY);
		$this->db->where('status','Active');
		$CategoryVal = $this->db->get();
				
		$CatArr = array();
			
		foreach($CategoryVal->result() as $catVal){
			if($catVal->image!=''){
				$catImage = $catVal->image;
			}else{
				$catImage = 'no_image.jpg';
			}
			$CatArr[] = array("id" => $catVal->id, "categoryName" => $catVal->cat_name,"image" =>'mb/'.$catImage,"catId"=>$catVal->rootID);
		}
		
		$json_encode = json_encode(array("categoryDetails" => $CatArr,"cartCount"=>(string)$this->data["cartCount"]));
		echo $json_encode;
	} 
	
	
	public function product() {
		$catid=intval($_GET['catid']);
		
		$shopname=$_GET['shopname'];
		$shopId=0;
		if($shopname!="")		{
			$shopId=intval($this->mobile_model->get_sellerId($shopname));
		}
		$this->db->select('p.id,p.product_name,p.image,p.price,p.base_price,p.user_id,p.status,s.seller_businessname,s.seller_id,a.pricing');
		$this->db->from(PRODUCT.' as p');
		$this->db->join(SELLER.' as s' , 'p.user_id = s.seller_id');
		$this->db->join(SUBPRODUCT.' as a','p.id=a.product_id','left');
		if($catid>0){
			$run = "FIND_IN_SET('".$catid."', p.category_id)";
			$this->db->where($run);
		}
		$this->db->where('p.status','Publish');
		$this->db->where('p.pay_status','Paid');
		if($shopId>0){
			$this->db->where('p.user_id',$shopId);
		}
		$this->db->group_by('p.id');
		$productList = $this->db->get();
		$ProdArr = array();
			//$ProdArr[] = array('itemCount'=>$productList->num_rows());
			$i=1;
		foreach($productList->result() as $ProdList) {
			if($i<=20){
			$img=explode(',',$ProdList->image);
			#$price= $ProdList->base_price;
			$price= number_format($this->data["currencyValue"]*$ProdList->base_price,2);
			
			
			$favStatus = $this->mobile_model->get_all_details(FAVORITE,array('p_id'=>$ProdList->id,'user_id'=>$this->data["commonId"],'favorite'=>'Yes'))->num_rows();
			if($favStatus>0){$favStatus=1;}else{$favStatus=0;}
			
			$ProdArr[] = array("productId" => $ProdList->id,
										"productName" => character_limiter($ProdList->product_name,15),
										"Image" => 'mb/'.$img[0],
										"Price" => $price,
										"currencySymbol" =>$this->data["currencySymbol"],
										"currencyCode" =>$this->data["currencyCode"],
										"favStatus" =>(string)$favStatus,
										"storeName" => $ProdList->seller_businessname,
										"itemCount"=> (string)$productList->num_rows(),
										"pagePos"=>'1');
			$i++;
			}else{
				break;
			}
		}
		$json_encode = json_encode(array("productDetails" => $ProdArr,"cartCount"=>(string)$this->data["cartCount"]));
		echo $json_encode;
	}
	public function mobile_pay_cal(){

		$date1 = $_GET['start'];
		$date2 = $_GET['end'];

		$diff = abs(strtotime($date2) - strtotime($date1));
		$years = floor($diff / (365*60*60*24));
		$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
		$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
	}
	
	public function GetDays($sStartDate, $sEndDate){  
		
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime("-1 day", strtotime($sEndDate)));  
		
		$aDays[] = $sStartDate;  
      
		$sCurrentDate = $sStartDate;
		
		while($sCurrentDate < $sEndDate){  
        $sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
      
        $aDays[] = $sCurrentDate; 
		}  
		return $aDays;  
		
    }
	
	public function mobile_price_calculation(){
		
		$productId = $_GET['P_Id'];
		$startDate = $_GET['s_date'];
		$endDate = $_GET['e_date'];
		$CalendarDateArr = $this->GetDays($startDate, $endDate);
		
		$this->data['productPriceDetails'] = $this->mobile_model->get_all_details(PRODUCT,array('id'=>$productId));
		
		$price = $this->data['productPriceDetails']->row()->price;
		
		$currency = 1;
		
		$currencySymbol = '$';
		
		if($_GET['code'] && $_GET['code'] != '')
		{
		$this->data['currencyDetails'] = $this->mobile_model->get_all_details(CURRENCY,array('currency_type'=>$_GET['code']));
		if($this->data['currencyDetails']->num_rows() == 1){
		$currency = $this->data['currencyDetails']->row()->currency_rate;
		
		$currencySymbol = $this->data['currencyDetails']->row()->currency_symbols;
		}
		}
		$price = $price*$currency;
		$jsonReturn['perNight'] = $currencySymbol.$price;
		
		foreach($CalendarDateArr as $CalendarDateRow){
			$CalendarTimeDateArr = explode(' GMT',$CalendarDateRow);
			$sadfsd=trim($CalendarTimeDateArr[0]);
			$startDate = strtotime($sadfsd);    
			$result[] =  date("Y-m-d",$startDate);
		}
		$jsonReturn['totalNights'] = count($result) > 1 ? count($result).' nights' : count($result).' night';
		$DateCalCul=0;
		$this->data['ScheduleDatePrice'] = $this->mobile_model->get_all_details(SCHEDULE,array('id'=>$productId));
		if($this->data['ScheduleDatePrice']->row()->data !=''){
			$dateArr=json_decode($this->data['ScheduleDatePrice']->row()->data);
			$finaldateArr=(array)$dateArr;
			foreach($result as $Rows){
				if (array_key_exists($Rows, $finaldateArr)) {
					$DateCalCul = $DateCalCul+$finaldateArr[$Rows]->price;
				}else{
					$DateCalCul = $DateCalCul+$price;
				};
			}
		}else{ 
			$DateCalCul = (count($result) * $price);
		}
		$jsonReturn['total'] = $currencySymbol.$DateCalCul;
		$service_tax_query='SELECT * FROM '.COMMISSION.' WHERE commission_type="Guest Booking" AND status="Active"';
		$service_tax = $this->mobile_model->ExecuteQuery($service_tax_query);
		
		if($service_tax->num_rows()==0){
			$tax = 0;
		}
		else if($service_tax->row()->promotion_type=='flat'){
			$tax = $service_tax->row()->commission_percentage*$currency;
		}
		else{
			$tax = number_format((($DateCalCul * $service_tax->row()->commission_percentage)/100)*$currency, 2);
		}
		$jsonReturn['tax'] = $currencySymbol.$tax;
		$jsonReturn['grandTotal'] = $currencySymbol.($DateCalCul+$tax);
		echo json_encode($jsonReturn);
	}
	
	public function mobile_host_request(){
		$productId = $_GET['P_Id'];
		$startDate = $_GET['s_date'];
		$endDate = $_GET['e_date'];
		$guestEmail = $_GET['email'];
		
		$this->data['guestDetails'] = $this->mobile_model->get_all_details(USERS,array('email'=>$guestEmail));
		
		$userId = $this->data['guestDetails']->row()->id;
		$phone_no = $this->data['guestDetails']->row()->phone_no;
		$guests = $_GET['guests'];
		
		$CalendarDateArr = $this->GetDays($startDate, $endDate);
		
		$this->data['productPriceDetails'] = $this->mobile_model->get_all_details(PRODUCT,array('id'=>$productId));
		
		$price = $this->data['productPriceDetails']->row()->price;
		
		$sellerId = $sellerId = $this->data['productPriceDetails']->row()->user_id;
		
		$currency = 1;
		
		$currencySymbol = '';
		
		$checkIn = $checkIn = date('Y-m-d H:i:s', strtotime($startDate));
			
		$checkOut = $checkOut = date('Y-m-d H:i:s', strtotime($endDate));
		
		/* if($_GET['code'] && $_GET['code'] != '')
		{
		$this->data['currencyDetails'] = $this->mobile_model->get_all_details(CURRENCY,array('currency_type'=>$_GET['code']));
		if($this->data['currencyDetails']->num_rows() == 1){
		$currency = $this->data['currencyDetails']->row()->currency_rate;
		
		$currencySymbol = $this->data['currencyDetails']->row()->currency_symbols;
		}
		} */
		
		$price = $price*$currency;
		
		foreach($CalendarDateArr as $CalendarDateRow){
			$CalendarTimeDateArr = explode(' GMT',$CalendarDateRow);
			$sadfsd=trim($CalendarTimeDateArr[0]);
			$startDate = strtotime($sadfsd);    
			$result[] =  date("Y-m-d",$startDate);
		}
		$totalNights = count($result) > 1 ? count($result).' nights' : count($result).' night';
		$DateCalCul=0;
		$this->data['ScheduleDatePrice'] = $this->mobile_model->get_all_details(SCHEDULE,array('id'=>$productId));
		if($this->data['ScheduleDatePrice']->row()->data !=''){
			$dateArr=json_decode($this->data['ScheduleDatePrice']->row()->data);
			$finaldateArr=(array)$dateArr;
			foreach($result as $Rows){
				if (array_key_exists($Rows, $finaldateArr)) {
					$DateCalCul = $DateCalCul+$finaldateArr[$Rows]->price;
				}else{
					$DateCalCul = $DateCalCul+$price;
				};
			}
		}else{ 
			$DateCalCul = (count($result) * $price);
		}
		$service_tax_query='SELECT * FROM '.COMMISSION.' WHERE commission_type="Guest Booking" AND status="Active"';
		$service_tax = $this->mobile_model->ExecuteQuery($service_tax_query);
		
		if($service_tax->num_rows()==0){
			$tax = 0;
		}
		else if($service_tax->row()->promotion_type=='flat'){
			$tax = $service_tax->row()->commission_percentage*$currency;
		}
		else{
			$tax = number_format((($DateCalCul * $service_tax->row()->commission_percentage)/100)*$currency, 2);
		}
		$grandTotal = $DateCalCul+$tax;
		
		$dataArr = array(
			'user_id' => $userId ,
			'prd_id' => $productId ,
			'checkin' => $checkIn ,
			'checkout' => $checkOut ,
			'caltophone' => $phone_no ,
			'NoofGuest' => $guests ,
			'renter_id' => $sellerId ,
			'numofdates' => $totalNights ,
			'serviceFee' => $tax ,
			'totalAmt' => $grandTotal ,
			'phone_no' => $phone_no ,
			'booking_status' => 'Pending' ,
			'approval' => 'Pending'
		);
		//echo '<pre>';print_r($dataArr);die;
		$this->db->insert(RENTALENQUIRY, $dataArr); 
		
		$insertid = $this->db->insert_id ();
		
		$val = 1500000+$insertid;
		$bookingno ="EN".$val;
		
		$newdata = array ('Bookingno' => $bookingno);
		$condition = array ('id' => $insertid);
		$jsonReturn['bookingno'] = $bookingno;
		$this->mobile_model->update_details (RENTALENQUIRY,$newdata,$condition);
		if($insertid > 0) $jsonReturn['status'] = 'Success';
		else $jsonReturn['status'] = 'Failed';
		
		$hostDetails = $this->mobile_model->get_all_details(USERS,array('id'=>$sellerId));
		
		$gcmRegID  = $hostDetails->row()->mobile_key;
		$pushMessage = 'Your property '.$this->data['productPriceDetails']->row()->product_title.' was booked by '.$this->data['guestDetails']->row()->user_name;	
		if (isset($gcmRegID) && isset($pushMessage)) {		
			$gcmRegIds = array($gcmRegID);
			$message = array("m" => $pushMessage, "k"=>'Your Reservation');	
			$pushStatus = $this->sendPushNotificationToGCM($gcmRegIds, $message);
		}
		
		echo json_encode($jsonReturn);
		
	}
	
	public function mobile_your_trips(){
		$guestEmail = $_GET['email']; 
		$this->data['guestDetails'] = $this->mobile_model->get_all_details(USERS,array('email'=>$guestEmail));
		
		$userId = $this->data['guestDetails']->row()->id;
		
		$this->db->select('R.id as enqury_id, R.*, U.user_name, U.phone_no as user_phone, P.product_title, PP.product_image, PA.address');
		$this->db->from(RENTALENQUIRY . ' as R');
		$this->db->where('R.user_id',$userId);
		$this->db->join ( USERS . ' as U', 'U.id = R.renter_id', 'left' );
		$this->db->join ( PRODUCT_PHOTOS . ' as PP', 'PP.product_id = R.prd_id', 'left' );
		$this->db->join ( PRODUCT . ' as P', 'P.id = R.prd_id', 'left' );
		$this->db->join ( PRODUCT_ADDRESS_NEW . ' as PA', 'PA.productId = P.id', 'left' );
		$this->db->where('R.booking_status <>', 'Enquiry');
		$this->db->order_by('P.id',desc);
		$this->db->group_by('R.Bookingno');
		$this->data['tripDetails'] = $this->db->get();
		$i = 0;
		$tripDetails['yourTrips'] = array();
		foreach($this->data['tripDetails']->result_array() as $row)
		{
			$tripDetails['yourTrips'][] = $row;
			if($tripDetails['yourTrips'][$i]['product_image']=='')
			$tripDetails['yourTrips'][$i]['mproduct_image'] = 'no_image.jpg';
			else $tripDetails['yourTrips'][$i]['mproduct_image'] = $tripDetails['yourTrips'][$i]['product_image'];
			$tripDetails['yourTrips'][$i]['serviceFee'] = '$'.$tripDetails['yourTrips'][$i]['serviceFee'];
			$tripDetails['yourTrips'][$i]['totalAmt'] = '$'.$tripDetails['yourTrips'][$i]['totalAmt'];
			$today =  date('Y-m-d',strtotime(date('Y-m-d', strtotime("-2 days"))));
			$tripDetails['yourTrips'][$i]['Expired'] = 'No';
			if($today > $tripDetails['yourTrips'][$i]['dateAdded'])
			$tripDetails['yourTrips'][$i]['Expired'] = 'Yes';
			$i++;
		} 
		if(empty($tripDetails))echo json_encode($tripDetails['yourTrips']);
		else echo json_encode($tripDetails);
	}
	
	public function mobile_your_reservation(){
		$guestEmail = $_GET['email'];
		$this->data['guestDetails'] = $this->mobile_model->get_all_details(USERS,array('email'=>$guestEmail));
		
		$userId = $this->data['guestDetails']->row()->id;
		
		$this->db->select('R.*, U.user_name, U.phone_no as user_phone, P.product_title, PP.product_image, PA.address');
		$this->db->from(RENTALENQUIRY . ' as R');
		$this->db->where('R.renter_id',$userId);
		$this->db->join ( USERS . ' as U', 'U.id = R.user_id', 'left' );
		$this->db->join ( PRODUCT_PHOTOS . ' as PP', 'PP.product_id = R.prd_id', 'left' );
		$this->db->join ( PRODUCT . ' as P', 'P.id = R.prd_id', 'left' );
		$this->db->join ( PRODUCT_ADDRESS_NEW . ' as PA', 'PA.productId = P.id', 'left' );
		$this->db->order_by('P.id',desc);
		$this->db->group_by('R.Bookingno');
		$this->data['tripDetails'] = $this->db->get();
		
		$i = 0;
		$tripDetails['yourTrips'] = array();
		foreach($this->data['tripDetails']->result_array() as $row)
		{
			$tripDetails['yourTrips'][] = $row;
			if($tripDetails['yourTrips'][$i]['product_image']=='')
			$tripDetails['yourTrips'][$i]['mproduct_image'] = 'no_image.jpg';
			else $tripDetails['yourTrips'][$i]['mproduct_image'] = $tripDetails['yourTrips'][$i]['product_image'];
			$i++;
		}
		if(empty($tripDetails))echo json_encode($tripDetails['yourTrips']);
		else echo json_encode($tripDetails);
	}
	
	
	public function host_approval()
	{
		$enquiryId = $_GET['enqId'];
		
		$newdata = array ('approval' => 'Accept');
		$condition = array ('id' => $enquiryId);
		$this->mobile_model->update_details (RENTALENQUIRY,$newdata,$condition);
		
		$enqDetails = $this->mobile_model->get_all_details(RENTALENQUIRY,array('id'=>$enquiryId));
		
		$product_Id = $enqDetails->row()->prd_id;
		
		$rentalDetails = $this->mobile_model->get_all_details(PRODUCT,array('id'=>$product_Id));
		
		$product_title = $rentalDetails->row()->product_title;
		
		$productDetails = $this->mobile_model->get_all_details(RENTALENQUIRY,array('id'=>$enquiryId));
		
		$userId = $productDetails->row()->user_id;
		
		$userDetails = $this->mobile_model->get_all_details(USERS,array('id'=>$userId));
		
		$gcmRegID  = $userDetails->row()->mobile_key;
		$pushMessage = 'The booked property '.$rentalDetails->row()->product_title.' was approved';	
		if (isset($gcmRegID) && isset($pushMessage)) {		
			$gcmRegIds = array($gcmRegID);
			$message = array("m" => $pushMessage, "k"=>'Your Trips');	
			$pushStatus = $this->sendPushNotificationToGCM($gcmRegIds, $message);
		}
		
		
		
		if($enquiryId > 0) $jsonReturn['status'] = 'successfully accepted';
		else $jsonReturn['status'] = 'Failed';
		echo json_encode($jsonReturn);
	}
	
	public function host_decline()
	{
		$enquiryId = $_GET['enqId'];
		
		$newdata = array ('approval' => 'Decline');
		$condition = array ('id' => $enquiryId);
		$this->mobile_model->update_details (RENTALENQUIRY,$newdata,$condition);
		
		$enqDetails = $this->mobile_model->get_all_details(RENTALENQUIRY,array('id'=>$enquiryId));
		
		$product_Id = $enqDetails->row()->prd_id;
		
		$rentalDetails = $this->mobile_model->get_all_details(PRODUCT,array('id'=>$product_Id));
		
		$product_title = $rentalDetails->row()->product_title;
		
		$productDetails = $this->mobile_model->get_all_details(RENTALENQUIRY,array('id'=>$enquiryId));
		
		$userId = $productDetails->row()->user_id;
		
		$userDetails = $this->mobile_model->get_all_details(USERS,array('id'=>$userId));
		
		$gcmRegID  = $userDetails->row()->mobile_key;
		$pushMessage = 'The booked property '.$rentalDetails->row()->product_title.' was declined';
		if (isset($gcmRegID) && isset($pushMessage)) {		
			$gcmRegIds = array($gcmRegID);
			$message = array("m" => $pushMessage, "k"=>'Your Trips');	
			$pushStatus = $this->sendPushNotificationToGCM($gcmRegIds, $message);
		}
		
		
		if($enquiryId > 0) $jsonReturn['status'] = 'successfully declined';
		else $jsonReturn['status'] = 'Failed';
		echo json_encode($jsonReturn);
	}
	
	public function host_delete_list()
	{
		$email = $_POST['email'];
		$productId = $_POST['pId'];
		$condition = array('email'=>$email);
		$userDetails = $this->mobile_model->get_all_details ( USERS, $condition );
		$userId = $userDetails->row()->id;
		$this->db->where('user_id', $userId);
		$this->db->where('id', $productId);
		$this->db->delete(PRODUCT);
		$condition = array('user_id' => $userId, 'id' => $productId);
		$productDetails = $this->mobile_model->get_all_details ( PRODUCT, $condition );
		if($productDetails->num_rows() == 0)
		{
			$this->db->where('product_id', $productId);
			$this->db->delete(PRODUCT_ADDRESS); 
			$this->db->where('product_id', $productId);
			$this->db->delete(PRODUCT_PHOTOS); 
			$this->db->where('id', $productId);
			$this->db->delete(SCHEDULE); 
			$this->db->where('PropId', $productId);
			$this->db->delete(CALENDARBOOKING);
			$jsonReturn['status'] = 'Success';
		}
		echo json_encode($jsonReturn);
	}
	
	public function host_unlist_list()
	{
		$email = $_GET['email'];
		$productId = $_GET['pId'];
		$action = $_GET['action'];
		$condition = array('email'=>$email);
		$userDetails = $this->mobile_model->get_all_details ( USERS, $condition );
		$userId = $userDetails->row()->id;
		$data = array(
			'user_status'=>$action
			);
		$condition = array(
			'id'=>$productId,
			'user_id'=>$userId
			);
		$this->mobile_model->update_details(PRODUCT ,$data ,$condition);
		$jsonReturn['status'] = 'Success';
		echo json_encode($jsonReturn);
	}
	
	public function save_cancel_policy()
	{
		$tType = $_POST['tType'];
		$productId = $_POST['pId'];
		$data = array(
			'cancellation_policy'=>$tType
			);
		$condition = array(
			'id'=>$productId
			);
		$this->mobile_model->update_details(PRODUCT ,$data ,$condition);
		$jsonReturn['status'] = 'successfully added';
		echo json_encode($jsonReturn);
	}
	
	public function save_minimum_stay()
	{
		$minimum_stay = $_POST['minimum_stay'];
		$productId = $_POST['pId'];
		$data = array(
			'minimum_stay'=>$minimum_stay
			);
		$condition = array(
			'id'=>$productId
			);
		$this->mobile_model->update_details(PRODUCT ,$data ,$condition);
		$jsonReturn['status'] = 'successfully added';
		echo json_encode($jsonReturn);
	}
	
	public function save_product_currency()
	{
		$currency = $_POST['currency'];
		$productId = $_POST['pId'];
		$data = array(
			'currency'=>$currency
			);
		$condition = array(
			'id'=>$productId
			);
		$this->mobile_model->update_details(PRODUCT ,$data ,$condition);
		$jsonReturn['status'] = 'successfully added';
		echo json_encode($jsonReturn);
	}
	
	
	
	public function proceedPayment(){
		//if ($_POST['enqId']!=''){ 
		$enqId = 1;
		if ($enqId != ''){ 
			$this->db->select('R.*');
			$this->db->from(RENTALENQUIRY . ' as R');
			$this->db->where('R.id',$enqId);
			$result = $this->db->get();
			$userId=$result->row()->renter_id;
			$sellerId=$result->row()->user_id;
			$productId=$result->row()->prd_id;
			$payment=$result->row()->totalAmt;
			$payArr = array('userId'	=>	$userId,
							'sellerId'		=>	$sellerId,
							'productId'		=>	$productId,
							'payment'	=>	$payment,	
						);
			$this->mobile_model->simple_insert(MOBILE_PAYMENT,$payArr);
			$mobileId = $this->db->insert_id();
			$json_encode = json_encode(array("status"=>'Success', "mobileId"=>$mobileId));
			echo $json_encode;
			//echo "Success|".$mobileId;
		}else{
			$json_encode = json_encode(array("status"=>'Failed'));
			echo $json_encode;
			//echo "Failure";
		}
	}
	
	public function send_iphone_message()
	{
		$this->push_notification();
	}
	/** 
	 * 
	 * Load CMS pages for mobile view
	 */
	public function mobilePages() {	
		$seourl = $this->uri->segment(2);
		$pageDetails = $this->mobile_model->get_all_details(CMS,array('seourl'=>$seourl,'status'=>'Publish'));
		if ($pageDetails->num_rows() == 0){
    		show_404();
    	}else {
    		$this->mobdata['pageDetails'] = $pageDetails;			
			$this->load->view('mobile/cms.php',$this->mobdata);
		}
	}
	
	public function iospn(){
		#413944117c13a80d85ef678f6557d854e36fe378da63376bdf24bdbe6e86381a
		$this->push_notification();
	}
	
	public function transaction_history(){
		$email = $_GET['email'];
		$transactionsArr = $this->mobile_model->get_all_details(COMMISSION_TRACKING,array('host_email'=>$email));
		$resonse_json = $transactionsArr->result();
		echo json_encode(array('transaction_history'=>$resonse_json));
	}
	 
	public function add_to_favourite(){
		$email = $_POST['email'];
		$productId = $_POST['p_Id'];
		$userDetails = $this->mobile_model->get_all_details(USERS,array('email'=>$email));
		$userId = $userDetails->row()->id;
		$listDetails = $this->mobile_model->get_all_details(LISTS_DETAILS,array('user_id'=>$userId, 'product_id'=>$productId));
		if($listDetails->num_rows() == 0)
		{
			$productDetails = $this->mobile_model->get_all_details(PRODUCT,array('id'=>$productId));
			$productName = $productDetails->row()->product_title;
			$dataArr = array('user_id'=>$userId, 'name'=>$productName, 'product_id'=>$productId, 'whocansee'=>'Everyone');
			$this->mobile_model->simple_insert(LISTS_DETAILS,$dataArr);
			$listDetails = $this->mobile_model->get_all_details(LISTS_DETAILS,array('user_id'=>$userId, 'product_id'=>$productId));
		}
		$jsonReturn['status'] = 'successfully added';
		echo json_encode(array('jsonReturn'=>$jsonReturn));
	}
	
	public function remove_from_favourite(){
		$email = $_POST['email'];
		$productId = $_POST['p_Id'];
		$userDetails = $this->mobile_model->get_all_details(USERS,array('email'=>$email));
		$userId = $userDetails->row()->id;
		$productDetails = $this->mobile_model->get_all_details(PRODUCT,array('id'=>$productId));
		$productName = $productDetails->row()->product_title;
		$this->db->where('user_id', $userId);
		$this->db->where('product_id', $productId);
		$this->db->delete(LISTS_DETAILS); 
		//echo $this->db->last_query();die;
		$jsonReturn['status'] = 'successfully removed';
		echo json_encode(array('jsonReturn'=>$jsonReturn));
	}
	public function language_list() {
		
		
		$select_qry = "select language_code,language_name from fc_languages_known";
		$list_language = $this->mobile_model->ExecuteQuery($select_qry);
		$listvalueArr = array();
		foreach($list_language->result() as $list_value) {
		$listvalueArr[] = array("language_code" =>$list_value->language_code,"language_name"=>$list_value->language_name);
		}
		$json_encode = json_encode(array("list_language"=>$listvalueArr));
		echo $json_encode;
	}
	
	public function get_payout(){
		$email = $_GET['email'];
		$this->db->select('accname, accno, bankname, swiftcode');
		$this->db->from(USERS);
		$this->db->where('email',$email);
		$result = $this->db->get();
		$payoutDetails['payoutDetails'] = array();
		foreach($result->result_array() as $row)
		{
			$payoutDetails['payoutDetails'][] = $row;
		}
		if(empty($payoutDetails))echo json_encode($payoutDetails['yourTrips']);
		else echo json_encode($payoutDetails);
	}
	
	public function save_payout(){
		$email = $_POST['email'];
		$accname = $_POST['accname'];
		$accno = $_POST['accno'];
		$bankname = $_POST['bankname'];
		$swiftcode = $_POST['swiftcode'];
		$condition = array (
				'email' => $email
		);
		$dataArr = array (
			'accname' => $accname,
			'accno' => $accno,
			'bankname' => $bankname,
			'swiftcode' => $swiftcode
		);
		$this->mobile_model->update_details(USERS, $dataArr, $condition);
		$jsonReturn['status'] = 'successfully added';
		echo json_encode(array('jsonReturn'=>$jsonReturn));
	}
	
	public function verify_my_id(){
		$email = $_POST['email'];
		$condition = array (
				'email' => $email
		);
		$userDetails = $this->mobile_model->get_all_details(USERS, $condition);
		if($userDetails->num_rows() == 0)
		{
			$jsonReturn['status'] = 'sending failed';
			echo json_encode(array('jsonReturn'=>$jsonReturn));die;
		}
		$uid = $userDetails->row ()->id;
		$username = $userDetails->row ()->user_name;
		$email = $userDetails->row ()->email;
		
		$newsid = '18';
		$template_values = $this->mobile_model->get_newsletter_template_details( $newsid );
		
		$user=$userDetails->row ()->firstname.' '.$userDetails->row ()->lastname;
		
		$subject = 'From: ' . $this->config->item ( 'email_title' ) . ' - ' . $template_values ['news_subject'];
		$adminnewstemplateArr = array (
				'email_title' => $this->config->item ( 'email_title' ),
				'logo' => $this->data ['logo'],
				'username'=>$username
		);
		extract ( $adminnewstemplateArr );
		$header .= "Content-Type: text/plain; charset=ISO-8859-1\r\n";
		
		$message .= '<!DOCTYPE HTML>
			<html>
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
			<meta name="viewport" content="width=device-width"/><body>';
		include ('./newsletter/registeration' . $newsid . '.php');
		
		$message .= '</body>
			</html>';
		
		if ($template_values ['sender_name'] == '' && $template_values ['sender_email'] == '') {
			$sender_email = $this->data ['siteContactMail'];
			$sender_name = $this->data ['siteTitle'];
		} else {
			$sender_name = $template_values ['sender_name'];
			$sender_email = $template_values ['sender_email'];
		}
		$email_values = array (
				'mail_type' => 'html',
				'from_mail_id' => $sender_email,
				'mail_name' => $sender_name,
				'to_mail_id' => $email,
				'cc_mail_id' => $sender_email,
				'subject_message' => $template_values ['news_subject'],
				'body_messages' => $message 
				
		);
		//print_r(stripslashes($message));die;
		
		$email_send_to_common = $this->mobile_model->common_email_send ( $email_values );
	
		$jsonReturn['status'] = 'successfully sent';
		echo json_encode(array('jsonReturn'=>$jsonReturn));
	}
	
	public function resize_all_cities()
	{
		$dir    = FCPATH.'images/city';
		$files = scandir($dir);
		
		foreach($files as $file)
		{
			$uploaddir = $dir.'/mobile/';
			$source = $dir.'/'.$file;
			$renameArr = explode('.', $file);
			$newName = $renameArr[0].'.jpg';
			echo $target = $dir.'/mobile/'.$newName;
			echo '<br>';
			if (!copy($source, $target)) {
				if(is_file($target))
				{
					$option=$this->getImageShape(500,350,$target);
					$renameArr = explode('.', $target);
					$newName = $renameArr[0].'.jpg';
					$resizeObj = new Resizeimage($target);	
					$resizeObj -> resizeImage(500, 350, $option);
					$resizeObj -> saveImage($uploaddir.$newName, 100);
					$this->ImageCompress($uploaddir.$newName);
					@copy($uploaddir.$newName, $uploaddir.$newName);
				}
			}
		}
	}
	
	public function resize_all_products()
	{
		$dir    = FCPATH.'server/php/rental';
		$files = scandir($dir);
		
		foreach($files as $file)
		{
			$uploaddir = $dir.'/mobile/';
			$source = $dir.'/'.$file;
			$renameArr = explode('.', $file);
			$newName = $renameArr[0].'.jpg';
			echo $target = $dir.'/mobile/'.$newName;
			echo '<br>';
			if (!copy($source, $target)) {
				if(is_file($target))
				{
					$option=$this->getImageShape(500,350,$target);
					$renameArr = explode('.', $target);
					$newName = $renameArr[0].'.jpg';
					$resizeObj = new Resizeimage($target);	
					$resizeObj -> resizeImage(500, 350, $option);
					$resizeObj -> saveImage($uploaddir.$newName, 100);
					$this->ImageCompress($uploaddir.$newName);
					@copy($uploaddir.$newName, $uploaddir.$newName);
				}
			}
		}
	}
	
}

/* End of file mobile.php no_image.jpg */
/* Location: ./application/controllers/site/mobile.php */
?>